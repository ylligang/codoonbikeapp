package com.inwhoop.codoon.view;

import com.inwhoop.codoon.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Space;

/**
 * @Project: SeekBarPressure
 * @Title: MyView.java
 * @Package com.example.seekbarpressure
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-5-10 下午1:29:32
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class LineView extends View {

	private int mThumbWidth;

	private Drawable mThumbLow;

	private Resources resources = null;

	private int toleft = 0;

	private int number = 0;

	public LineView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		Resources resources = getResources();
		mThumbLow = resources.getDrawable(R.drawable.seekbarpressure_thumb);
		mThumbWidth = mThumbLow.getIntrinsicWidth();
		toleft = (int) resources.getDimension(R.dimen.line_to_left);
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		Paint paint = new Paint();
		paint.setColor(Color.WHITE);
		paint.setStrokeWidth(3);
		int space = (getWidth() - mThumbWidth - mThumbWidth / 2) / number;
		int start = mThumbWidth / 5;
		for (int i = 0; i < number + 1; i++) {
			canvas.drawLine(i * space + start + toleft, 0, i * space + start
					+ toleft, 30, paint);
		}
	}

	public void setMaxnum(int number) {
		this.number = number;
	}
	
	public int getNumber(){
		return number;
	}

	public int getLinewidth() {
		return getWidth();
	}

}
