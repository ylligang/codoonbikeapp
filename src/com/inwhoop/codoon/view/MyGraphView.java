package com.inwhoop.codoon.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

public class MyGraphView extends View {
	private float xPoint = 0;// 原点X坐标
	private float yPoint = 0;// 原点Y坐标
	private float xLengh = 1000;// X轴长度
	private float yLengh = 320;// Y轴长度
	private float xScale = 5;// X轴一个刻度长度
	private float yScale = 5;// Y轴一个刻度长度
	private Paint speedPaint;
	private int HEIGHT = 600;
	int speed[] = { 120, 130, 140, 130, 150, 155, 168, 176, 150, 132, 149, 120,
			130, 140, 130, 150, 155, 168, 176, 150, 132, 149 };

	public MyGraphView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		speedPaint = new Paint();

	}

	public MyGraphView(Context context, AttributeSet attrs) {
		super(context, attrs);
		speedPaint = new Paint();
	}

	public MyGraphView(Context context) {
		super(context);
		speedPaint = new Paint();
		init();
	}

	private void init() {
		xPoint = 20;
		yPoint = 500;
		xScale = getScale(speed.length - 1, xLengh);
		yScale = getScale(speed.length - 1, yLengh);

	}

	/**
	 * 得到每一等分的长度
	 * 
	 * @param num
	 *            所要分成的等份
	 * @param length
	 *            要分割的总长度
	 * @return
	 */
	private float getScale(int num, float length) {
		if (num > 0 && length > 0) {
			length -= 10;// 为了美观，缩进
			length = length - (length % num);
			return length / num;
		} else {
			return 0;
		}

	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		speedPaint.setAntiAlias(true); // 设置画笔为无锯齿
		speedPaint.setColor(0xff4b8646); // 设置画笔颜色
		// canvas.drawColor(Color.WHITE); // 白色背景
		speedPaint.setStrokeWidth((float) 3.0); // 线宽
		speedPaint.setStyle(Style.FILL);
		Path path = new Path(); // Path对象
		int m = 0;
		for (int i = 0; i < speed.length; i++) {
			if (i == 0) {
				path.moveTo(xPoint, speed[i]); // 起始点
			} else {
				m = i;
				path.lineTo(i * 100, speed[i] - 200); // 连线到下一点
			}
			if (i == speed.length - 1) {
				path.lineTo(i * 100, speed[1]); // 连线到下一点
				path.close();
			}
		}
		// path.moveTo(0, 600); // 起始点
		// path.lineTo(0, 250); // 连线到下一点
		// path.lineTo(100, 200); // 连线到下一点
		// path.lineTo(200, 250); // 连线到下一点
		// path.lineTo(200, 200); // 连线到下一点
		// path.lineTo(300, 250); // 连线到下一点
		// path.lineTo(400, 200); // 连线到下一点
		// path.lineTo(500, 250); // 连线到下一点
		// path.lineTo(600, 200); // 连线到下一点
		// path.lineTo(700, 250); // 连线到下一点
		// path.lineTo(800, 200); // 连线到下一点
		// path.lineTo(900, 250); // 连线到下一点
		// path.lineTo(1000, 200); // 连线到下一点
		// path.lineTo(1100, 250); // 连线到下一点
		// path.lineTo(1200, 200); // 连线到下一点
		// path.lineTo(1300, 250); // 连线到下一点
		// path.lineTo(1400, 200); // 连线到下一点
		// path.lineTo(1500, 250); // 连线到下一点
		// path.lineTo(1800, 200); // 连线到下一点
		// path.lineTo(3000, 250); // 连线到下一点
		// // path.lineTo(300, 300); // 连线到下一点
		// // path.lineTo(450, 50); // 连线到下一点
		// // path.lineTo(200, 200); // 连线到下一点
		// path.lineTo(3000, 600);
		canvas.drawPath(path, speedPaint); // 绘制任意多边形
	}

}
