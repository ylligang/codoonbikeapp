package com.inwhoop.codoon.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;

public class CommonDialog {

	private static AlertDialog cameraDialog;

	private static ProgressDialog mProgressDialog;

	public static final int DIALOG_PROCESS = 0, DIALOG_OK = 1;
	public static String MESSAGE = "msg", TITLE = "title";

	/**
      * 
      */
	public static void closeCameraDialog() {
		if (cameraDialog != null) {

			cameraDialog.dismiss();
		}
	}

	/**
	 * 
	 * @param context
	 */
	private static boolean isStartSoftKey = true;

	public static void popupSoftKeyboard(Context context) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(0, InputMethodManager.SHOW_IMPLICIT);
		isStartSoftKey = true;
	}

	/**
	 * 
	 * @param context
	 */
	public static void hideSoftKeyboard(Context context) {
		InputMethodManager imm = (InputMethodManager) (context
				.getSystemService(Context.INPUT_METHOD_SERVICE));
		if (isStartSoftKey) {
			imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,
					InputMethodManager.HIDE_NOT_ALWAYS);
			isStartSoftKey = false;
		}

	}

	/**
	 * 
	 * @param context
	 */
	public static void openProcessDialog(Activity currentActivity,
			String content) {
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
		mProgressDialog = new ProgressDialog(currentActivity);
		mProgressDialog.setMessage(content);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(true);
		mProgressDialog.show();
	}

	/**
     * 
     */
	public static void closeProcessDialog() {
		if (mProgressDialog != null)
			mProgressDialog.dismiss();
	}

	/**
     * 
     */
	public static void closeProcessDialogWait() {
		if (mProgressDialog != null) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			mProgressDialog.dismiss();
		}
	}

	/**
	 * 
	 * @param currentActivity
	 */
	public static void openOkDialog(Activity currentActivity, String msg,
			String title) {
		AlertDialog.Builder builder = new AlertDialog.Builder(currentActivity);
		builder.setTitle(TextUtils.isEmpty(title) ? "Message" : title);
		builder.setNegativeButton("OK", null);
		builder.setMessage(msg);
		builder.create().show();
	}

	/**
	 * 图片预锟斤拷
	 * 
	 * @param context
	 * @param bitmap
	 * @param bitName
	 */
	public static void openReviewPhoto(Activity activity, Bitmap bitmap) {
		String path = "/sdcard/" + "temp" + ".png";
		saveMyBitmap("temp", bitmap);
		File file = new File(path);

		Intent intent = new Intent();
		intent.setAction(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file), "image/*");
		activity.startActivity(intent);

	}

	private static void saveMyBitmap(String bitName, Bitmap mBitmap) {
		File f = new File("/sdcard/" + bitName + ".png");
		try {
			f.createNewFile();
		} catch (IOException e) {
		}
		FileOutputStream fOut = null;
		try {
			fOut = new FileOutputStream(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		mBitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
		try {
			fOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			fOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void sendHandler(int what, Handler handler, String content,
			String title) {
		Message message = new Message();
		message.what = what;
		Bundle bundle = new Bundle();
		if (message != null) {
			bundle.putString(MESSAGE, content);
		}
		if (title != null) {
			bundle.putString(TITLE, title);
		}
		message.setData(bundle);
		handler.sendMessage(message);
	}
}