package com.inwhoop.codoon.service;

import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.utils.Utils;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

public class CountService extends Service {
	private int count = 0;
	private boolean isRun = true;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		System.out.println("=========intentintentintentintent===============");
		count = 0;
		addCount();
		return super.onStartCommand(intent, flags, startId);
	}

	public void addCount() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isRun) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					handler.sendEmptyMessage(0);

				}

			}
		}).start();

	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			int speed = Utils.getIntpreference(getApplicationContext(),
					MyApplication.SPEED, 0);
			int taPing = Utils.getIntpreference(getApplicationContext(),
					MyApplication.CARDEN, 0);
			if (speed > 0 || taPing > 0) {
				count = 0;
			}
			++count;
			if (count == (3*60)) {
				Intent intent = new Intent();// 创建Intent对象
				intent.setAction(MyApplication.ACTION_COUNT);
				sendBroadcast(intent);// 发送广播
			}
		}
	};

	public void onDestroy() {
		isRun = false;

	};
}
