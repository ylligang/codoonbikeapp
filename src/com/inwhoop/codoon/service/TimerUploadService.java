package com.inwhoop.codoon.service;

import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.utils.JsonUtils;
import com.inwhoop.codoon.utils.Utils;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

/**
 * @Project: codoonbikeapp
 * @Title: UploadService.java
 * @Package com.inwhoop.codoon.service
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-5-13 上午8:59:05
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class TimerUploadService extends Service {

	private int count = 0;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		timerToupload();
		return super.onStartCommand(intent, flags, startId);
	}

	private void timerToupload() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						int min = Utils.getIntpreferenceset(
								getApplicationContext(), MyApplication.DATA, 3);
						count++;
						if (count == min * 60) {
							handler.sendEmptyMessage(0);
							count = 0;
						} else if (count > min * 60) {
							count = 0;
						}
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
				}
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			int datacount = Utils.getIntpreferenceData(getApplicationContext(),
					"datacount", 0);
			if (datacount > 0) {
				for (int i = 0; i < datacount; i++) {
					upload(Utils.getpreferenceData(getApplicationContext(),
							"data" + i));
					System.out.println("========Utils=========="+Utils.getpreferenceData(getApplicationContext(),
							"data" + i));
				}
				Utils.clear(getApplicationContext(), MyApplication.SP1);
				int datacount1 = Utils.getIntpreferenceData(
						getApplicationContext(), "datacount", 0);
			}
		};
	};

	private void upload(final String json) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					String result = JsonUtils.uploadData(json, Utils
							.getpreference(getApplicationContext(),
									MyApplication.Token));
					uploadhandler.sendEmptyMessage(0);
				} catch (Exception e) {

				}
			}
		}).start();
	}

	private Handler uploadhandler = new Handler() {
		public void handleMessage(android.os.Message msg) {

		};
	};

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}
