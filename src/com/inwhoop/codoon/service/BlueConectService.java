package com.inwhoop.codoon.service;

import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.utils.BlueToothUtils;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcelable;

public class BlueConectService extends Service {

	public BlueToothUtils utl = null;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		scanDevice();
		return super.onStartCommand(intent, flags, startId);
	}

	private void scanDevice() {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
			return;
		}
		utl = new BlueToothUtils(handler);
		utl.scanDevice(getApplicationContext());
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (msg.what == BlueToothUtils.GET_NEW_DEVICE) {
				Intent intent = new Intent();// 创建Intent对象
				intent.setAction(MyApplication.ACTION_BLURTHOOTDEVICE);
				intent.putExtra("device", (Parcelable) msg.obj);
				sendBroadcast(intent);// 发送广播
				stopSelf();
				BlueToothUtils.mDisconveryManager.stopSearch();
			}

		};
	};

	public void onDestroy() {
		utl.setNull();
		stopSelf();
		if (null != BlueToothUtils.mDisconveryManager) {
			BlueToothUtils.mDisconveryManager.stopSearch();
		}

	};
}
