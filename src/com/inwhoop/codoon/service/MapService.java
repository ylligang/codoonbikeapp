package com.inwhoop.codoon.service;

import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.utils.Utils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class MapService extends Service {
	private boolean isSend = true;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		send();
		return super.onStartCommand(intent, flags, startId);
	}

	private void send() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isSend) {
					try {
						Intent intent = new Intent();// 创建Intent对象
						intent.setAction(MyApplication.ACTION_MAP);
						sendBroadcast(intent);// 发送广播
						Thread.sleep(30 * 1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		}).start();
	}

	@Override
	public void onDestroy() {
		isSend = false;
		super.onDestroy();
	}
}
