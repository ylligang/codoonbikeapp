package com.inwhoop.codoon.service;

import java.util.ArrayList;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.utils.Utils;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.format.Time;

/**
 * 语音播报服务
 * 
 * @Project: CodoonBikeApp
 * @Title: PlayService.java
 * @Package com.inwhoop.codoon.service
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-5-11 下午2:35:50
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class PlayService extends Service implements OnCompletionListener {

	/* 定于一个多媒体对象 */
	public static MediaPlayer mMediaPlayer = null;
	// 是否单曲循环
	private static boolean isLoop = false;
	// 用户操作
	private int MSG;

	/* 定义要播放的文件夹路径 */
	private final String MP3_PATH = "android.resource://com.inwhoop.codoon/";
	private ArrayList<ArrayList<Integer>> playPaths = new ArrayList<ArrayList<Integer>>();
	private int listIndex = 0;
	private int playIndex = 0;
	private boolean isPlay = true;
	private Time t;
	private MyThread myThread;
	private int count=0;
	private int time=0;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		if (mMediaPlayer != null) {
			mMediaPlayer.reset();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		mMediaPlayer = new MediaPlayer();
		/* 监听播放是否完成 */
		mMediaPlayer.setOnCompletionListener(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mMediaPlayer != null) {
			mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		isPlay = false;
		myThread.interrupt();
		myThread=null;
		
		System.out.println("intentPlay===========onDestroy");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		System.out.println("intentPlay===========onStartCommand");
		t = new Time("GMT+8");
		playMusic(listIndex, playIndex);
		return super.onStartCommand(intent, flags, startId);
	}

	public void playMusic(final int listindex, final int index) {
		myThread=new MyThread();
		myThread.start();
//		new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				while (isPlay) {
//					int time = Utils.getIntpreferenceset(
//							getApplicationContext(), MyApplication.YUYIN, 0);
//					if (time != 0) {
//						try {
//							Thread.sleep(time * 60 * 1000);
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//						listIndex = 0;
//						playIndex = 0;
//						Message msg = handler.obtainMessage();
//						msg.arg1 = 0;
//						msg.arg2 = 0;
//						handler.sendMessage(msg);
//					}
//				}
//
//			}
//		}).start();

	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
//			System.out.println("intentPlay===message");
			try {
				if (MyApplication.mState == 1) {
					play(msg.arg1, msg.arg2);
				}
			} catch (Exception e) {
			}

		};
	};

	private void play(int listindex, int index) {
		try {
			t.setToNow(); // 取得系统时间。
			int hour = t.hour + 8; // 0-23
			int minute = t.minute;
			playPaths.clear();
			playPaths.add(Utils.getMediaPath(
					Utils.getIntpreference(getApplicationContext(),
							MyApplication.SPEED, 0) + "", R.raw.sudu,
					R.raw.gonglimeishi));
			playPaths.add(Utils.getMediaPath(
					Utils.getIntpreference(getApplicationContext(),
							MyApplication.CARDEN, 0) + "", R.raw.tapin,
					R.raw.zhuanmeifen));
			playPaths.add(Utils.getMediaPath(
					Utils.getIntpreference(getApplicationContext(),
							MyApplication.DISTANCE, 0) + "", R.raw.juli,
					R.raw.gongli));
			playPaths.add(Utils.getMediaPath(hour + "", R.raw.shijian,
					R.raw.hour));
			playPaths.add(Utils.getMediaPath(minute + "", -1, R.raw.fen));
			if (MyApplication.mState == 1) {
				if (listindex < playPaths.size()
						&& index < playPaths.get(listindex).size()) {
					/* 重置多媒体 */
					mMediaPlayer.reset();
					/* 读取mp3文件 */
					Uri uri = Uri.parse(MP3_PATH
							+ playPaths.get(listindex).get(index));

					mMediaPlayer.setDataSource(getApplicationContext(), uri);
					/* 准备播放 */
					mMediaPlayer.prepare();
					/* 开始播放 */
					mMediaPlayer.start();
					/* 是否单曲循环 */
					mMediaPlayer.setLooping(isLoop);
					// new Thread(this).start();

				}
			}
		} catch (Exception e) {
		}

	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		++playIndex;
		if (listIndex < playPaths.size()) {
			if (playIndex < playPaths.get(listIndex).size()) {
				play(listIndex, playIndex);
			} else {
				if (listIndex < playPaths.size()) {
					++listIndex;
					playIndex = 0;
					play(listIndex, playIndex);
				}

			}
		}

	}

	class MyThread extends Thread {
		@Override
		public void run() {
			while (isPlay) {
				time = Utils.getIntpreferenceset(
						getApplicationContext(), MyApplication.YUYIN, 0);
				if (time != 0) {
					count++;
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("intentPlay===========time"+time);
					System.out.println("intentPlay=====11======time"+count);
					if(count%(time*60)==0){
						listIndex = 0;
						playIndex = 0;
						Message msg = handler.obtainMessage();
						msg.arg1 = 0;
						msg.arg2 = 0;
						handler.sendMessage(msg);
					}
				}
			}
		}
	}

}
