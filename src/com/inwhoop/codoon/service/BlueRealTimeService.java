package com.inwhoop.codoon.service;

import com.communication.ble.BleSyncManager;
import com.communication.data.CODMBCommandData;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.fragment.ConfirmBindFragment;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class BlueRealTimeService extends Service {
	private boolean isbool = true;
	public static BleSyncManager mSyncDeviceDataManager;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		getData();
		return super.onStartCommand(intent, flags, startId);
	}

	private void getData() {
		new Thread(new Runnable() {
			// Intent intent = new Intent();// 创建Intent对象
			@Override
			public void run() {
				while (isbool) {
					try {
						if (null != MyApplication.mSyncDeviceDataManager) {
							MyApplication.mSyncDeviceDataManager
									.SendDataToDevice(CODMBCommandData
											.getPostRealTimeData());
						}
						// intent.setAction(MyApplication.ACTION_BLURTHOOT_DATA);
						// sendBroadcast(intent);// 发送广播

						Thread.sleep(2000);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}
		}).start();
	}

	@Override
	public void onDestroy() {
		isbool = false;
		stopSelf();
		super.onDestroy();
	}
}
