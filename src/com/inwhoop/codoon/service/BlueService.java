package com.inwhoop.codoon.service;

import com.communication.data.CODMBCommandData;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.utils.Utils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class BlueService extends Service {
	
	private int nowCir = 0;
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// int[] data = CODMBCommandData.getPostRealTimeData();
		send();
		return super.onStartCommand(intent, flags, startId);
	}

	private void send() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						int speed = Utils
								.getIntpreference(getApplicationContext(),
										MyApplication.SPEED, 0);
						int taPing = Utils.getIntpreference(
								getApplicationContext(), MyApplication.CARDEN,
								0);
						int circleNum = Utils.getIntpreference(
								getApplicationContext(), MyApplication.CIRCLE,
								0);
						int[] data = new int[3];
						data[0] = speed;
						data[1] = taPing;
						data[2] = Math.abs(circleNum-nowCir);
						if(speed>0){
							Intent intent = new Intent();// 创建Intent对象
							intent.setAction(MyApplication.ACTION_BLURTHOOT);
							intent.putExtra("data", data);
							sendBroadcast(intent);// 发送广播
						}
						nowCir = circleNum;
						Thread.sleep(1000);
						// Thread.sleep(Utils.getIntpreference(
						// getApplicationContext(), MyApplication.YUYIN, 3) * 60
						// * 1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		}).start();

	}
}
