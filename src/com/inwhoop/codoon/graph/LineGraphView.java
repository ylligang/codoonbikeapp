/**
 * This file is part of GraphView.
 *
 * GraphView is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphView is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GraphView.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
 *
 * Copyright Jonas Gehring
 */

package com.inwhoop.codoon.graph;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.codoon.entity.CodoonTime;
import com.inwhoop.codoon.graph.GraphViewSeries.GraphViewSeriesStyle;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Line Graph View. This draws a line chart.
 */
public class LineGraphView extends GraphView {

	private Paint paintBackground;
	private boolean drawBackground;

	private double lastx = 1;

	double lastEndY = 0;
	double lastEndX = -1;

	public LineGraphView(Context context, AttributeSet attrs) {
		super(context, attrs);

		paintBackground = new Paint();
		// paintBackground.setColor(Color.rgb(20, 40, 60));
		// paintBackground.setStrokeWidth(4);
	}

	public LineGraphView(Context context, String title) {
		super(context, title);
		paintBackground = new Paint();
		// paintBackground.setColor(Color.rgb(20, 40, 60));
		// paintBackground.setStrokeWidth(4);
	}

	@Override
	public void drawSeries(Canvas canvas, GraphViewDataInterface[] values,
			float graphwidth, float graphheight, float border, double minX,
			double minY, double diffX, double diffY, float horstart,
			double nowx, GraphViewSeriesStyle style) {
		if (drawBackground) {
			paintBackground.setStrokeWidth(style.thickness);
			paintBackground.setColor(style.color);
			float startY = graphheight + border;
			for (int i = 0; i < values.length; i++) {
				double valY = values[i].getY() - minY;
				double ratY = valY / diffY;
				double y = graphheight * ratY;

				double valX = values[i].getX() - minX;
				double ratX = valX / diffX;
				double x = graphwidth * ratX;

				float endX = (float) x + (horstart + 1);
				float endY = (float) (border - y) + graphheight + 2;

				// if (i > 0) {
				// fill space between last and current point
				// double numSpace = ((endX - lastEndX)/3f) +1;
				double numSpace = ((endX - lastEndX)) + 1;
				if (lastEndX != -1) {
					for (int xi = 0; xi < numSpace; xi++) {
						float spaceX = (float) (lastEndX + ((endX - lastEndX)
								* xi / (numSpace - 1)));
						float spaceY = (float) (lastEndY + ((endY - lastEndY)
								* xi / (numSpace - 1)));

						// start => bottom edge
						float startX = spaceX;
						// do not draw over the left edge
						// System.out.println("startX - horstart= "+(startX -
						// horstart));
						// if (startX - horstart > 1) {
						canvas.drawLine(startX, startY, spaceX, spaceY,
								paintBackground);
						// }
					}
				}
				// }

				lastEndY = endY;
				lastEndX = endX;
			}
		}
		// draw data
		paint.setStrokeWidth(style.thickness);
		paint.setColor(style.color);

		lastEndY = 0;
		lastEndX = 0;
		for (int i = 0; i < values.length; i++) {
			double valY = values[i].getY() - minY;
			double ratY = valY / diffY;
			double y = graphheight * ratY;

			double valX = values[i].getX() - minX;
			double ratX = valX / diffX;
			double x = graphwidth * ratX;

			if (i > 0) {
				float startX = (float) lastEndX + (horstart + 1);
				float startY = (float) (border - lastEndY) + graphheight;
				float endX = (float) x + (horstart + 1);
				float endY = (float) (border - y) + graphheight;
				canvas.drawLine(startX, startY, endX, endY, paint);
			}
			lastEndY = y;
			lastEndX = x;
		}
	}

	public int getBackgroundColor() {
		return paintBackground.getColor();
	}

	public boolean getDrawBackground() {
		return drawBackground;
	}

	@Override
	public void setBackgroundColor(int color) {
		paintBackground.setColor(color);
	}

	/**
	 * @param drawBackground
	 *            true for a light blue background under the graph line
	 */
	public void setDrawBackground(boolean drawBackground) {
		this.drawBackground = drawBackground;
	}
}
