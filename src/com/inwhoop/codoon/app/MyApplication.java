package com.inwhoop.codoon.app;

import java.util.ArrayList;
import java.util.List;

import com.communication.ble.BleSyncManager;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.v4.app.Fragment;

public class MyApplication extends Application {

	public static  Context context;
	// public static int spacetime = 1; //数据同步时间
	public static final String APP_VERSION = "1.0"; // app版本

	public final static int READ_SUCCESS = 200; // 读取网络数据成功
	public final static int READ_FAIL = 500; // 读取网络失败
	public static List<Fragment> fragments = new ArrayList<Fragment>();
	public static int screenWidth;
	public static int screenHeight;
//	public static final String CONECT_TAG = "conect";// 连接key
	public static final int CONECT = 1;// 连接
	public static final int NO_CONECT = 0;// 未连接
	public static final String USER_SP = "user";// 存储个人注册信息
	public static final String SET_SP = "set";// 存储个人注册信息
	public static final String START_TIME = "start_time";// 存储骑行开始
	public static final String STOP_TIME = "stop_time";// 存储骑行结束
	public static final String PLAY = "play";// 存储骑行语音
	public static final String AUTO_LOGIN = "auto_login";// 存储个人注册信息
	public static final String GPS = "gps";// 存储个人注册信息
	// public static final String USER_LOGIN = "user_login";//存储个人注册信息
	public static final String USER_INFO = "userinfo";// 存储个人信息
	public static final String SPORT_INFO = "sport";// 存储运动信息
	public static final String SP = "codoon_device";// 存储设备信息
	public static final String SP1 = "codoon_device_data";// 存储骑行数据
	public static final String SP2 = "codoon_device_set";// SP设置
	public static final String D_NAME = "d_name";// 存储设备名称
	public static final String VERSION = "Version";// 存储设备信息
	public static final String DEV_ID = "DeviceID";// 存储设备信息
	public static final String BATTERY = "battery";// 存储设备信息
	public static final String DEV_TIME = "DeviceTime";// 存储设备信息
	public static final String PROGRESS = "SyncDataProgress";// 存储设备信息
	public static final String SPEED = "speed";// 存储设备信息
	public static final String CARDEN = "carden";// 存储设备信息
	public static final String DISTANCE = "distance";// 存储设备信息
	public static final String CIRCLE = "circle";// 存储设备信息
	public static final String PERIMETER = "perimeter";// 存储设备信息
	public static final String WEIGHT = "weight";// 存储设备信息
	public static final String YUYIN = "yuyin";// 存储语音播报时间
	public static final String DATA = "data";// 存储数据同步时间
	public static final String Token = "mAccessToken";
	public static final String USERID = "userId";
	public static final String END_TIME = "end_time";
	public static final String EXPIRE = "expire_in";
//	public static final String TIME = "time";
	public static final String HOUR = "hour";
	public static final String MIN = "min";
	public static final String ALL_DISTANCE = "all_distance";//总里程
	public static final String ALL_XIAOHAO = "all_xiaohao";//总消耗
	
	public static final String WHEEL_SIZE = "wheel_size";
	public static final String WHEEL_WIDTH = "wheel_width";
	public static final String WHEEL_SIZE_TEXT = "wheel_size_text";
	public static final String WHEEL_WIDTH_TEXT = "wheel_width_text";
	public static final String WHEEL_WIDTH_NUMBER = "wheel_width_number";
	public static final String WHEEL_GRITH = "wheel_girth";
	public static String REGISTER = "http://api.codoon.com/user/codoonmobileregist";// 注册接口
	public static final String BIND = "bind";
	public static final String SUCESS="1";//1绑定成功，2绑定失败
	public static final String FAIL="2";//1绑定成功，2绑定失败
	public static  float Kc ;// 本次消耗
	public static  float c_distance ;// 本次里程
	public static  float max_sp ;// 本次最高速度
	public static  float max_tp ;// 本次最高踏频
	public static  float a_sp ;// 本次平均速度
	public static  float a_tp ;// 本次平均踏频
	public static  String COUNT="count" ;// 计数器
	public static int mState = -1;// 连接
	
	public static int  speed;
	public static int tp ;
	public static int circleNum;
	
	public static boolean flag = true;   //标识跳转到指南针界面
	

	public static final String AUTHORIZATION_KEY = "Authorization";
	public static final String HTTP_HOST = "http://api.codoon.com";// "http://api.xiaogd.com";//
	public static final String HTTP_CLIENT_KEY = "99b0f06142c411e39360842b2b8ab7ec";// "099cce28c05f6c39ad5e04e51ed60704";
	public static final String HTTP_CLIENT_SECRET = "83a6a7ba42c411e38403f04da2ec070e";// "c39d3fbea1e85becee41c1997acf0e36";
	public static final String HTTP_TOKEN_URL = HTTP_HOST + "/token";
	public static final String HTTP_API_PUBLIC = HTTP_HOST + "/api";
	public static final String HTTP_OTHER_LOGIN = HTTP_HOST + "/external_token";
	public static final String HTTP_USER_URL = HTTP_API_PUBLIC
			+ "/verify_credentials";
	public static final String HTTP_REGIST_URL = HTTP_HOST
			+ "/user/codoonmobileregist";
	public static final String HTTP_USER_DATA = HTTP_HOST
			+ "/api/get_tracker_summary";
	public static final String HTTP_SUBMIT_DATA = HTTP_HOST
			+ "/api/post_tracker_data";
	public static final String HTTP_RIDEBLE_DATAUPLOAD = HTTP_HOST
			+ "/api/rideble_dataupload";
	public static final String HTTP_RIDEBLE_HISTORY = HTTP_HOST
			+ "/api/rideble_gethistory";
	public static final String HTTP_RIDEBLE_MOREPERIOD = HTTP_HOST
			+ "/api/rideble_getmoreperiod";
	public static final String HTTP_RIDEBLE_BIKESETTINGUPLOAD = HTTP_HOST
			+ "/api/rideble_bikesettingupload";
	public static final String HTTP_RIDEBLE_BIKESETTING = HTTP_HOST
			+ "/api/rideble_getbikesetting";
	
	public static BleSyncManager mSyncDeviceDataManager;

	// 登录用户发送的广播
	public final static String ACTION_LOGIN = "com.inwhoop.login";
	public final static String ACTION_BLURTHOOT = "com.inwhoop.bluethoot";
	public final static String ACTION_BLURTHOOT_DATA = "com.inwhoop.bluethootdata";
	public final static String ACTION_UPLOAD = "com.inwhoop.upload";
	public final static String ACTION_MAP = "com.inwhoop.map";
	public final static String ACTION_STOP = "com.inwhoop.stop";
	public final static String ACTION_COUNT = "com.inwhoop.count";
	public final static String ACTION_SPEED = "com.inwhoop.speed";
	public final static String ACTION_BLURTHOOTDEVICE = "com.inwhoop.bluethootdevice";
	public static List<Activity> activities=new ArrayList<Activity>();
}
