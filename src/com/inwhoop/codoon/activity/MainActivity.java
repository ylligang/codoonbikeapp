package com.inwhoop.codoon.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.service.BlueConectService;
import com.inwhoop.codoon.utils.JsonUtils;
import com.inwhoop.codoon.utils.Utils;

/**
 * 启动界面
 * 
 * @Project: CodoonBikeApp
 * @Title: MainActivity.java
 * @Package com.inwhoop.codoon
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-4-14 下午5:57:37
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class MainActivity extends Activity {
	private ImageView indexView = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int datacount = Utils.getIntpreferenceData(this,
				"datacount", 0);
		setContentView(R.layout.activity_main);
		indexView = (ImageView) findViewById(R.id.image_icon);
		MyApplication.screenHeight = getWindowManager().getDefaultDisplay()
				.getHeight();
		MyApplication.screenWidth = getWindowManager().getDefaultDisplay()
				.getWidth();
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.CENTER_HORIZONTAL);
		params.setMargins(0, MyApplication.screenHeight / 3, 0, 0);
		indexView.setLayoutParams(params);
		handler.sendEmptyMessageDelayed(0, 2000);

		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = handler.obtainMessage();
				msg.obj = JsonUtils.getData();
				msg.what = 404;
				handler.sendMessage(msg);

			}
		}).start();

	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 0:
				start();
				break;

			case 404:
				TextView tv = null;
				if (msg.obj.equals("error")) {
					tv.setText((String) msg.obj);
				}
				break;
			}
		}
	};

	private void start() {
		Intent intent = null;
		if (!Utils.getBooleanpreference(MainActivity.this,
				MyApplication.AUTO_LOGIN)) {
			intent = new Intent(MainActivity.this, LoginActivity.class);
		} else {
			String tokenString = Utils.getpreference(MainActivity.this,
					MyApplication.Token);
			if (null != tokenString && !tokenString.equals("")) {
				long time = Long.parseLong(Utils.getpreference(
						MainActivity.this, MyApplication.END_TIME).trim());
				if (time < System.currentTimeMillis()) {
					intent = new Intent(MainActivity.this, LoginActivity.class);
					Utils.clear(MainActivity.this, MyApplication.SP);
					Utils.clear(MainActivity.this, MyApplication.USER_INFO);
					Utils.clear(MainActivity.this, MyApplication.USER_SP);
					Utils.clear(MainActivity.this, MyApplication.AUTO_LOGIN);
					Utils.clear(MainActivity.this, MyApplication.GPS);
				} else {
					intent = new Intent(MainActivity.this,
							SlidingActivity.class);
					intent.putExtra("first", false);
					// String isBind = Utils.getpreference(
					// getApplicationContext(), MyApplication.BIND);
					// if (isBind.equals(MyApplication.SUCESS)) {
					// Intent serIntent = new Intent(MainActivity.this,
					// BlueConectService.class);
					// startService(serIntent);
					// }
				}
			} else {
				intent = new Intent(MainActivity.this, LoginActivity.class);
				intent.putExtra("first", true);
			}
		}
		startActivity(intent);
		MainActivity.this.finish();

	}
}
