package com.inwhoop.codoon.activity;

import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.widget.Toast;

import cn.sharesdk.framework.ShareSDK;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.fragment.MainFragment;
import com.inwhoop.codoon.fragment.MenuFragment;
import com.inwhoop.codoon.fragment.StartBindFragment;
import com.inwhoop.codoon.slidmenu.SlidingFragmentActivity;
import com.inwhoop.codoon.slidmenu.SlidingMenu;
import com.inwhoop.codoon.utils.Utils;

public class SlidingActivity extends SlidingFragmentActivity {
	public SlidingMenu menu;
//	private StartBindFragment startBindFragment;
//	private MainFragment mainFragment;
	public MenuFragment menuFragment;
	public FragmentManager mFm = null;
	public String token = "";
	private static Boolean isExit = false;
	private static Boolean hasTask = false;
	private Timer tExit;
	private TimerTask task;
	// private boolean isFrist;

	public Intent timerIntent = null;
	public Intent serIntent = null;
	public Intent countIntent = null;
	public Intent speedIntent = null;
	public Intent intentPlay;
	public Intent intentData;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainlayout);
		setBehindContentView(R.layout.menu_main);
		MyApplication.activities.add(this);
		token = Utils.getpreference(this, MyApplication.Token);
		MyApplication.screenHeight = getWindowManager().getDefaultDisplay()
				.getHeight();
		MyApplication.screenWidth = getWindowManager().getDefaultDisplay()
				.getWidth();
		menu = getSlidingMenu();
		menu.setMode(SlidingMenu.LEFT);
		menu.setBehindOffset(MyApplication.screenWidth / 5);
		menu.setFadeEnabled(false);
		menu.setFadeDegree(0.4f);
		menu.setBehindScrollScale(0);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		mFm = getSupportFragmentManager();
		initView();
		ShareSDK.initSDK(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	private void initView() {

		menuFragment = new MenuFragment();
		String isBind = Utils.getpreference(getApplicationContext(),
				MyApplication.BIND);
		FragmentTransaction ft = this.getSupportFragmentManager()
				.beginTransaction();
		if (isBind.equals(MyApplication.SUCESS)) {
			Bundle bundle = new Bundle();
			bundle.putString("fag", "auto");
			menuFragment.mainFragment = new MainFragment();
			menuFragment.mainFragment.setArguments(bundle);
			ft.add(R.id.content_frame, menuFragment.mainFragment);
		} else {
			menuFragment.startBindFragment = new StartBindFragment();
			ft.add(R.id.content_frame, menuFragment.startBindFragment);
		}
		ft.replace(R.id.menu_frame, menuFragment, "menuFragment");
		ft.addToBackStack("menuFragment");
		ft.commitAllowingStateLoss();
		tExit = new Timer();
		task = new TimerTask() {

			@Override
			public void run() {
				isExit = false;
				hasTask = true;
			}
		};
	}

	public void switchContent(final Fragment fragment) {
		menu.showContent();
		menu.setMode(SlidingMenu.LEFT);
		menu.clearIgnoredViews();
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				msg.obj = fragment;
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			FragmentTransaction ft = SlidingActivity.this
					.getSupportFragmentManager().beginTransaction();
			menuFragment.startBindFragment = (StartBindFragment) msg.obj;
			ft.replace(R.id.content_frame, menuFragment.startBindFragment,
					"startBindFragment");
			ft.addToBackStack("startBindFragment");
			ft.commitAllowingStateLoss();
		};
	};


	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (isExit == false) {
				isExit = true;
				Toast.makeText(this, "再按一次退出", Toast.LENGTH_SHORT).show();
				if (!hasTask) {
					tExit.schedule(task, 2000);
				}
			} else {
				BluetoothAdapter blueAdapter = BluetoothAdapter
						.getDefaultAdapter();
				blueAdapter.disable();
				blueAdapter = null;
				for (int i = 0; i < MyApplication.activities.size(); i++) {
					MyApplication.activities.get(i).finish();
				}
				Utils.saveIntPreference(SlidingActivity.this,
						MyApplication.SPEED, 0);
				Utils.saveIntPreference(SlidingActivity.this,
						MyApplication.CARDEN, 0);
				Utils.saveIntPreference(SlidingActivity.this,
						MyApplication.CIRCLE, 0);
				Utils.saveBooleanStartPreference(SlidingActivity.this,
						MyApplication.START_TIME, false);
				Utils.saveBooleanStopPreference(SlidingActivity.this,
						MyApplication.STOP_TIME, false);
				Utils.saveBooleanStopPreference(SlidingActivity.this,
						MyApplication.PLAY, false);

				if (null != timerIntent) {
					stopService(timerIntent);
				}
				if (null != serIntent) {
					stopService(serIntent);
				}
				if (null != countIntent) {
					stopService(countIntent);
				}
				if (null != speedIntent) {
					stopService(speedIntent);
				}
				if (null != intentPlay) {
					stopService(intentPlay);
				}
				if (null != intentData) {
					stopService(intentData);
				}
				System.exit(0);
			}
		}
		return false;
	}

}
