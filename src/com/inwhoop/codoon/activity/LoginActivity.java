package com.inwhoop.codoon.activity;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.framework.utils.UIHandler;
//import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.tencent.weibo.TencentWeibo;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.entity.BikeSettingBean;
import com.inwhoop.codoon.entity.BikeSettingEntity;
import com.inwhoop.codoon.entity.EquipmentInfoBean;
import com.inwhoop.codoon.entity.RidingDataBean;
import com.inwhoop.codoon.entity.RidingEntity;
import com.inwhoop.codoon.entity.ThirdLogin;
import com.inwhoop.codoon.entity.User;
import com.inwhoop.codoon.entity.UserToken;
import com.inwhoop.codoon.utils.JsonUtils;
import com.inwhoop.codoon.utils.Parameter;
import com.inwhoop.codoon.utils.SyncHttp;
import com.inwhoop.codoon.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Handler.Callback;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * 登录界面
 * 
 * @Project: CodoonBikeApp
 * @Title: LoginActivity.java
 * @Package com.inwhoop.codoon
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-4-16 下午3:30:55
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class LoginActivity extends BaseActivity implements OnClickListener,
		Callback, PlatformActionListener {
	private static final int MSG_USERID_FOUND = 1;
	private static final int MSG_LOGIN = 2;
	private static final int MSG_AUTH_CANCEL = 3;
	private static final int MSG_AUTH_ERROR = 4;
	private static final int MSG_AUTH_COMPLETE = 5;
	private EditText userEditText = null;
	private EditText pwdEditText = null;
	private Button loginButton = null;
	private Button registerButton = null;
	private Button sinaButton = null;
	private Button weiboButton = null;
	private Button qqButton = null;
//	private Button otherboButton = null;
	private Button useButton = null;
	private MyApplication application = null;
	private String userName = "";
	private String passWord = "";
	private UserToken token;
	private String platformName = "";
	private User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		ShareSDK.initSDK(this);
		init();
	}

	/**
	 * 组件初始化
	 * 
	 * @Title: initView
	 * @Description: TODO void
	 */
	private void init() {
		application = (MyApplication) getApplicationContext();
		application.screenWidth = getWindowManager().getDefaultDisplay()
				.getWidth();
		application.screenHeight = getWindowManager().getDefaultDisplay()
				.getHeight();
		userEditText = (EditText) findViewById(R.id.username);
		pwdEditText = (EditText) findViewById(R.id.pwd);
		loginButton = (Button) findViewById(R.id.login);
		registerButton = (Button) findViewById(R.id.register);
		sinaButton = (Button) findViewById(R.id.sina_bt);
		weiboButton = (Button) findViewById(R.id.weibo_bt);
		qqButton = (Button) findViewById(R.id.qq_bt);
//		otherboButton = (Button) findViewById(R.id.other_bt);
		useButton = (Button) findViewById(R.id.use);
		int w = (int) (application.screenWidth - (getResources().getDimension(
				R.dimen.left_margin) * 4));
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w / 5,
				w / 5);
		LinearLayout.LayoutParams sinaParams = new LinearLayout.LayoutParams(
				w / 5, w / 5);
		params.leftMargin = (int) getResources().getDimension(
				R.dimen.left_margin);

		sinaButton.setLayoutParams(sinaParams);
		weiboButton.setLayoutParams(params);
		qqButton.setLayoutParams(params);
//		otherboButton.setLayoutParams(params);
		loginButton.setOnClickListener(this);
		registerButton.setOnClickListener(this);
		sinaButton.setOnClickListener(this);
		weiboButton.setOnClickListener(this);
		qqButton.setOnClickListener(this);
//		otherboButton.setOnClickListener(this);
		useButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent = null;
		switch (v.getId()) {
		case R.id.login:

			String msg = check();
			if (null != msg) {
				Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT)
						.show();
			} else {
				showProgressDialog("正在登录...");
				new Thread(new Runnable() {
					@Override
					public void run() {
						token = JsonUtils.parseLogin(userName, passWord);
						Message message = handler.obtainMessage();
						if (null == token) {
							message.what = MyApplication.READ_FAIL;
						} else {
							message.what = 520;
						}
						handler.sendMessage(message);
					}
				}).start();
			}
			break;

		case R.id.register:
			intent = new Intent(LoginActivity.this, RegisterActivity.class);
			startActivity(intent);

			break;
		case R.id.sina_bt:
			platformName = "sina";
			authorize(new SinaWeibo(this));
			break;

		case R.id.weibo_bt:
			platformName = "qq2";
			authorize(new TencentWeibo(this));
			break;
		case R.id.qq_bt:
			platformName = "qq";
			authorize(new QZone(this));
			break;

//		case R.id.other_bt:
//			break;
		case R.id.use:
			intent = new Intent(LoginActivity.this, SlidingActivity.class);
			startActivity(intent);
			Utils.clear(LoginActivity.this, MyApplication.SP);
			break;

		}

	}

	private void authorize(Platform plat) {
		if (plat == null) {
			return;
		}
		showProgressDialog("授权中...");
//		if (plat.isValid()) {
//			String userId = plat.getDb().getUserId();
//			if (userId != null) {
//				UIHandler.sendEmptyMessage(MSG_USERID_FOUND, this);
//				login(plat.getName(), "", null);
//				dismissProgressDialog();
//				return;
//			}
//		}
		plat.removeAccount();
		plat.setPlatformActionListener(this);
		plat.SSOSetting(true);
		plat.showUser(null);
		dismissProgressDialog();
	}

	private void login(String plat, String userId,
			HashMap<String, Object> userInfo) {
		Message msg = new Message();
		msg.what = MSG_LOGIN;
		msg.obj = plat;
		UIHandler.sendMessage(msg, this);
	}

	/**
	 * 登录
	 * 
	 * @Title: loginSelf
	 * @Description: TODO void
	 */
	private String loginSelf(String username, String password) {
		SyncHttp syncHttp = new SyncHttp();
		String result = "";
		// /** -------------------登录---------------------- */
		// try {
		// String path = "http://api.codoon.com/token?email="
		// + URLEncoder.encode(username, "utf-8") + "&password="
		// + URLEncoder.encode(password, "utf-8")
		// + "&grant_type=password"
		// + "&client_id=99b0f06142c411e39360842b2b8ab7ec"
		// + "&scope=user";
		// result = syncHttp.httpGet(path, "", "");
		// } catch (Exception e) {
		// System.out.println("Exception== " + e.toString());
		// e.printStackTrace();
		// }
	
		/** -------------------获取头像信息---------------------- */
		// try {
		// String token = Utils.getpreference(mContext, MyApplication.Token);
		// String path = "http://api.codoon.com/api/verify_credentials";
		// result = syncHttp.httpGet(path, "", token);
		// System.out.println("ll==" + result);
		// } catch (Exception e) {
		// System.out.println("Exception== " + e.toString());
		// e.printStackTrace();
		// }

		/** -------------------上传---------------------- */
		// EquipmentInfoBean info = new EquipmentInfoBean("1002", "4.3", "6.3");
		// RidingDataBean bean = new RidingDataBean("2014-05-12 12:00:00",
		// "2014-05-12 12:33:00", 25, 200, 30, 27, 40, 30);
		// RidingDataBean bean1 = new RidingDataBean("2014-05-12 15:00:00",
		// "2014-05-12 15:33:00", 25, 200, 30, 27, 40, 30);
		// List<RidingDataBean> list = new ArrayList<RidingDataBean>();
		// list.add(bean);
		// list.add(bean1);
		// RidingEntity entity = new RidingEntity();
		// entity.setInfo(info);
		// entity.setData(list);
		// result=syncHttp.post(MyApplication.HTTP_RIDEBLE_DATAUPLOAD,
		// JsonUtils.entityToJson(entity), "e6eaaca7d20e36cec5956c5cf2851c74");
		// http://api.codoon.com/api/rideble_gethistory?user_id=c077efdb-de28-421e-9bf7-bc222c28a466
		// http://api.codoon.com/api/rideble_gethistory?user_id=c077efdb-de28-421e-9bf7-bc222c28a466
		/** -------------------获取“历史”页面数据---------------------- */
		// try {
		// result = syncHttp.httpGet(MyApplication.HTTP_RIDEBLE_HISTORY,
		// "user_id=" + "c077efdb-de28-421e-9bf7-bc222c28a466",
		// "e6eaaca7d20e36cec5956c5cf2851c74");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		/** -------------------获取 更多 骑行流水数据---------------------- */
		// http://api.codoon.com/api/rideble_gethistory?count=5&start_time=2014-05-11
		// http://api.codoon.com/api/rideble_getmoreperiod?start_time=2014-05-11&count=5
		// try {
		// result = syncHttp.httpGet(MyApplication.HTTP_RIDEBLE_MOREPERIOD,
		// "start_time=" + "2014-05-11" + "&count=" + 5,
		// "966f6222a6cd0fd946fec7fc20915504");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// /** ------------------- 自行车设置上传---------------------- */
		// BikeSettingEntity entity = new BikeSettingEntity();
		// entity.setInfo(new EquipmentInfoBean("1002", "4.3", "6.3"));
		// BikeSettingBean bean = new BikeSettingBean();
		// bean.setPerimeter(2);
		// entity.setData(bean);
		// result = syncHttp.post(MyApplication.HTTP_RIDEBLE_BIKESETTINGUPLOAD,
		// JsonUtils.entityToJson(entity),
		// "e6eaaca7d20e36cec5956c5cf2851c74");

		/** ------------------- 获取“轮胎”参数---------------------- */

		// try {
		// result = syncHttp.httpGet(MyApplication.HTTP_RIDEBLE_BIKESETTING,
		// "user_id=" + "c077efdb-de28-421e-9bf7-bc222c28a466",
		// "e6eaaca7d20e36cec5956c5cf2851c74");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// System.out.println("result== " + result);
		return result;

	}

	private Handler handler = new Handler() {

		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			Intent intent = null;
//			boolean isFrist = getIntent().getExtras().getBoolean("first");
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				Toast.makeText(mContext, "登录失败", Toast.LENGTH_SHORT).show();
				break;

			case MyApplication.READ_SUCCESS:
				Utils.savePreference(mContext, MyApplication.Token,
						token.getAccess_token());
				Utils.savePreference(mContext, MyApplication.USERID,
						token.getUser_id());
				Utils.savePreference(mContext, MyApplication.EXPIRE,
						token.getExpire_in() + "");
				Utils.savePreference(mContext, MyApplication.END_TIME,
						(System.currentTimeMillis() + token.getExpire_in())
								+ "");
				Utils.saveObject(mContext, MyApplication.USER_SP, user);
				intent = new Intent(LoginActivity.this, SlidingActivity.class);
//				intent.putExtra("first", isFrist);
				startActivity(intent);
				LoginActivity.this.finish();

				break;
			case 101:
				if (null != msg.obj) {
					Utils.saveObject(LoginActivity.this, MyApplication.USER_SP,
							(User) msg.obj);
					intent = new Intent(LoginActivity.this,
							SlidingActivity.class);
//					intent.putExtra("first", isFrist);
					Utils.savePreference(mContext, MyApplication.Token,
							token.getAccess_token());
					Utils.savePreference(mContext, MyApplication.USERID,
							token.getUser_id());
					Utils.savePreference(mContext, MyApplication.EXPIRE,
							token.getExpire_in() + "");
					Utils.savePreference(mContext, MyApplication.END_TIME,
							(System.currentTimeMillis() + token.getExpire_in())
									+ "");
					startActivity(intent);
					LoginActivity.this.finish();
				}
				break;
			case 520:
				getUser();
				break;
			}
		};
	};

	private void getUser() {
		showProgressDialog("正在获取用户信息...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message message = handler.obtainMessage();
				try {
					SyncHttp syncHttp = new SyncHttp();
					String result = "";
					// String token = Utils.getpreference(mContext,
					// MyApplication.Token);
					String path = "http://api.codoon.com/api/verify_credentials";
					result = syncHttp
							.httpGet(path, "", token.getAccess_token());
					User user = JsonUtils.parseUser(result);
					message.what = 101;
					message.obj = user;

				} catch (Exception e) {
					message.what = MyApplication.READ_FAIL;
					System.out.println("ee==" + e.toString());
				}
				handler.sendMessage(message);
			}
		}).start();

	}

	/**
	 * 输入信息检查
	 * 
	 * @Title: check
	 * @Description: TODO
	 * @return String
	 */
	private String check() {
		String msg = null;
		userName = userEditText.getText().toString();
		passWord = pwdEditText.getText().toString();
		if (TextUtils.isEmpty(userName)) {
			msg = getResources().getString(R.string.input_name);
		} else if (TextUtils.isEmpty(passWord)) {
			msg = getResources().getString(R.string.input_pwd);
		}
		return msg;
	}

	@Override
	public void onCancel(Platform arg0, int action) {
		if (action == Platform.ACTION_USER_INFOR) {
			UIHandler.sendEmptyMessage(MSG_AUTH_CANCEL, this);
		}

	}

	@Override
	public void onComplete(final Platform platform, int action,
			HashMap<String, Object> res) {
		if (action == Platform.ACTION_USER_INFOR) {
			LoginActivity.this.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					showProgressDialog("账号授权中...");
					
				}
			});
			UIHandler.sendEmptyMessage(MSG_AUTH_COMPLETE, this);
			// login(platform.getName(), platform.getDb().getUserId(), res);
			final ThirdLogin thirdLogin = new ThirdLogin();
			thirdLogin.setToken(platform.getDb().getToken());
			thirdLogin.setSecret(platform.getDb().getTokenSecret());
			thirdLogin.setCatalog("codoon_oauth2.0");
			thirdLogin.setDevice_token("");
			thirdLogin.setSource(platformName);
			thirdLogin.setExpire_in(platform.getDb().getExpiresTime());
			thirdLogin.setExternal_user_id(platform.getDb().getUserId());
			user = new User();
			user.get_icon_middle = platform.getDb().getUserIcon();
			user.nick = platform.getDb().getUserName();
			thirdLogin.setClient_id(MyApplication.HTTP_CLIENT_KEY);

			new Thread(new Runnable() {

				@Override
				public void run() {
					token = JsonUtils.parseThridLogin(thirdLogin, platform
							.getDb().getToken());
					Message message = handler.obtainMessage();
					if (null == token) {
						message.what = MyApplication.READ_FAIL;
					} else {
						message.what = MyApplication.READ_SUCCESS;
					}
					handler.sendMessage(message);
				}
			}).start();

		}

	}

	@Override
	public void onError(Platform arg0, int action, Throwable t) {
//		System.out.println("onError= "+t.toString());
		if (action == Platform.ACTION_USER_INFOR) {
			UIHandler.sendEmptyMessage(MSG_AUTH_ERROR, this);
		}
		t.printStackTrace();
	}

	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what) {
		case MSG_USERID_FOUND: {
			Toast.makeText(this, R.string.userid_found, Toast.LENGTH_SHORT)
					.show();
		}
			break;
		case MSG_LOGIN: {
			String text = getString(R.string.logining, msg.obj);
			Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
			// Builder builder = new Builder(this);
			// builder.setTitle(R.string.if_register_needed);
			// builder.setMessage(R.string.after_auth);
			// builder.setPositiveButton(R.string.ok, null);
			// builder.create().show();
		}
			break;
		case MSG_AUTH_CANCEL: {
			Toast.makeText(this, R.string.auth_cancel, Toast.LENGTH_SHORT)
					.show();
		}
			break;
		case MSG_AUTH_ERROR: {
			Toast.makeText(this, R.string.auth_error, Toast.LENGTH_SHORT)
					.show();
		}
			break;
		case MSG_AUTH_COMPLETE: {
			Toast.makeText(this, R.string.auth_complete, Toast.LENGTH_SHORT)
					.show();
		}
			break;
		}
		return false;
	}
	
}
