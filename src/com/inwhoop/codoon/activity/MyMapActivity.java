package com.inwhoop.codoon.activity;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.Circle;
import com.amap.api.maps2d.model.CircleOptions;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.amap.api.maps2d.model.MyLocationStyle;
import com.amap.api.maps2d.model.PolylineOptions;
import com.inwhoop.codoon.R;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.entity.BikeData;
import com.inwhoop.codoon.utils.Utils;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyMapActivity extends Activity implements OnClickListener,
		AMapLocationListener {
	private AMap aMap;
	private MapView mapView;
	private LocationManagerProxy mAMapLocationManager;
	private TextView kmTextView;// 总里程
	private TextView qixingView;// 骑行
	private TextView kcView;// 总消耗
	private TextView cKmView;// 本次里程
	private TextView xHView;// 本次消耗
	private TextView aSpView;// 平均速度
	private TextView aTpView;// 平均踏频
	private TextView maxSpView;// 平均速度
	private TextView maxTpView;// 平均踏频
	private float allKc = 0;
	private float alldistance = 0;
	private ImageView mylocationImageView, shareImageView;
	private ArrayList<LatLng> latlngList = new ArrayList<LatLng>();
	private LatLng marker1;
	private LinearLayout mapLayout = null;
	private Marker marker = null;// 当前轨迹点图案
	private Marker markericon = null;// 当前轨迹点图案
	private Circle circle = null;
	private BikeData bikeData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		mapView = (MapView) findViewById(R.id.map);
		mapView.onCreate(savedInstanceState);
		kmTextView = (TextView) findViewById(R.id.km);
		qixingView = (TextView) findViewById(R.id.qixing);
		kcView = (TextView) findViewById(R.id.kc);
		cKmView = (TextView) findViewById(R.id.c_km);
		xHView = (TextView) findViewById(R.id.xiaohao);
		aSpView = (TextView) findViewById(R.id.a_speed);
		aTpView = (TextView) findViewById(R.id.a_tp);
		maxSpView = (TextView) findViewById(R.id.max_sp);
		maxTpView = (TextView) findViewById(R.id.max_tp);
		mylocationImageView = (ImageView) findViewById(R.id.locationimg);
		mylocationImageView.setOnClickListener(this);
		shareImageView = (ImageView) findViewById(R.id.shareimg);
		mapLayout = (LinearLayout) findViewById(R.id.map_down);
		shareImageView.setOnClickListener(this);
		mapLayout.setOnClickListener(this);
		bikeData = (BikeData) getIntent().getExtras().getSerializable("bike");
		allKc = getIntent().getFloatExtra("allbike", 0);
		alldistance = getIntent().getFloatExtra("alldistance", 0);
		init();
		location();
		setTextInfo();
	}

	/**
	 * 初始化AMap对象
	 */
	private void init() {
		if (aMap == null) {
			aMap = mapView.getMap();
			setUpMap();
		}
	}

	/**
	 * 设置一些amap的属性
	 */
	private void setUpMap() {
		// 自定义系统定位小蓝点
		// MyLocationStyle myLocationStyle = new MyLocationStyle();
		// myLocationStyle.myLocationIcon(BitmapDescriptorFactory
		// .fromResource(R.drawable.location_marker));// 设置小蓝点的图标
		// myLocationStyle.strokeColor(Color.BLACK);// 设置圆形的边框颜色
		// myLocationStyle.radiusFillColor(Color.argb(100, 0, 0, 180));//
		// 设置圆形的填充颜色
		// // myLocationStyle.anchor(int,int)//设置小蓝点的锚点
		// myLocationStyle.strokeWidth(1.0f);// 设置圆形的边框粗细
		// aMap.setMyLocationStyle(myLocationStyle);
		// aMap.setLocationSource(this);// 设置定位监听
		aMap.getUiSettings().setMyLocationButtonEnabled(false);// 设置默认定位按钮是否显示
		aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
		// getRote();
	}

	private void location() {
		if (null == mAMapLocationManager) {
			mAMapLocationManager = LocationManagerProxy.getInstance(this);
		}
		/*
		 * mAMapLocManager.setGpsEnable(false);
		 * 1.0.2版本新增方法，设置true表示混合定位中包含gps定位，false表示纯网络定位，默认是true Location
		 * API定位采用GPS和网络混合定位方式
		 * ，第一个参数是定位provider，第二个参数时间最短是2000毫秒，第三个参数距离间隔单位是米，第四个参数是定位监听者
		 */
		mAMapLocationManager.requestLocationUpdates(
				LocationProviderProxy.AMapNetwork, 2000, 10, this);

	}

	private void setTextInfo() {
		DecimalFormat fnum = new DecimalFormat("##0.00");
		kmTextView.setText(fnum.format(alldistance) + "\n总里程（公里）");
		kcView.setText(fnum.format(allKc) + "\n总消耗（千卡）");
		maxSpView.setText(fnum.format(bikeData.max_speed) + "km/h");
		maxTpView.setText(fnum.format(bikeData.max_pedal_rate) + "转/分");
		cKmView.setText(fnum.format(bikeData.distance) + "km");
		xHView.setText(fnum.format(bikeData.calories) + "kcal");
		aSpView.setText(fnum.format(bikeData.avg_speed) + "km/h");
		aTpView.setText(fnum.format(bikeData.avg_pedal_rate) + "rmp");

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mapView.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mapView.onPause();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		mapView.onSaveInstanceState(outState);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		try {
			mapView.onDestroy();
			mapView = null;
			marker.destroy();
			marker = null;
			aMap.clear();
			aMap = null;
			mAMapLocationManager.destory();
			mAMapLocationManager = null;
		} catch (Exception e) {

		}
	}

	private void drawLine(ArrayList<LatLng> list) {
		if (aMap != null) {
			aMap.clear();
		}
		if (list.size() > 0) {
			LatLng replayGeoPoint = list.get(list.size() - 1);
			if (marker != null) {
				marker.destroy();
			}
			// 添加汽车位置
			MarkerOptions markerOptions = new MarkerOptions();
			markerOptions
					.position(replayGeoPoint)
					.title("起点")
					.snippet(" ")
					.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
							.decodeResource(getResources(), R.drawable.car)))
					.anchor(0.5f, 0.5f);
			marker = aMap.addMarker(markerOptions);
			// 增加起点开始
			aMap.addMarker(new MarkerOptions()
					.position(list.get(0))
					.title("起点")
					.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
							.decodeResource(getResources(),
									R.drawable.nav_route_result_start_point))));
			// 增加起点结束
			if (list.size() > 1) {
				PolylineOptions polylineOptions = (new PolylineOptions())
						.addAll(list).color(Color.rgb(9, 129, 240)).width(6.0f);
				aMap.addPolyline(polylineOptions);

			}
			// for (int i = 1; i < list.size() - 1; i++) {
			// aMap.addMarker(new MarkerOptions()
			// .position(list.get(i))
			// .title("中间")
			// .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
			// .decodeResource(getResources(),
			// R.drawable.icon_marka))));
			// }
			aMap.addMarker(new MarkerOptions()
					.position(list.get(list.size() - 1))
					.title("终点")
					.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
							.decodeResource(getResources(),
									R.drawable.nav_route_result_end_point))));
		}
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationChanged(AMapLocation aLocation) {
		if (null != aLocation) {
			LatLng latLng = new LatLng(aLocation.getLatitude(),
					aLocation.getLongitude());
			aMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

			marker1 = new LatLng(aLocation.getLatitude(),
					aLocation.getLongitude());
			latlngList.add(marker1);
			if (Utils.getBooleanpreference(MyMapActivity.this,
					MyApplication.GPS)) {
				drawLine(latlngList);
			}
			if (null != markericon) {
				markericon.destroy();
			}
			if (null != circle) {
				circle.remove();
			}
			marker = aMap.addMarker(new MarkerOptions().position(latLng)
					.anchor(0.5f, 0.5f)
					// 锚点设置为中心
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.location_marker)));
			// 自定义定位成功后绘制圆形
			circle = aMap.addCircle(new CircleOptions().center(latLng)
					.radius(50).fillColor(Color.argb(100, 0, 0, 180))
					.strokeColor(Color.BLACK).strokeWidth(1.0f));

		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.locationimg:
			location();
			break;
		case R.id.shareimg:
			try {
				Utils.GetandSaveCurrentImage(MyMapActivity.this);
				String Content = "";
				if (null != bikeData) {
					Content = "#咕咚智能码表#告诉我，我在"
							+ Utils.getYear(bikeData.start_time)
							+ " "
							+ Utils.getHour(bikeData.start_time,
									bikeData.end_time) + "骑行了"
							+ bikeData.distance + "公里，燃烧了" + bikeData.calories
							+ "大卡";
				}
				Utils.showShare(MyMapActivity.this, Content,
						Utils.getSDCardPath()
								+ "/codoon/ScreenImage/Screen_1.png");
			} catch (Exception e) {
				// TODO: handle exception
			}
			break;
		case R.id.map_down:
			MyMapActivity.this.finish();
			break;

		}

	}

}
