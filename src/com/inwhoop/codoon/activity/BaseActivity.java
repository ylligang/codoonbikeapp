package com.inwhoop.codoon.activity;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.app.MyApplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.TextView;

/**
 * activity基类
 * 
 * @Project: CodoonBikeApp
 * @Title: BaseActivity.java
 * @Package com.inwhoop.codoon.activity
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-4-22 下午5:49:25
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class BaseActivity extends Activity {
	private Button leftButton = null;
	private Button rightButton = null;
	private TextView titleView = null;
	public ProgressDialog progressDialog;// 加载进度框
	public Context mContext = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mContext = this;
		MyApplication.activities.add(this);
		MyApplication.context=mContext;
	}

	/**
	 * 视图初始化
	 * 
	 * @Title: initView
	 * @Description: TODO void
	 */
	public void initView() {
		leftButton = (Button) findViewById(R.id.left);
		rightButton = (Button) findViewById(R.id.right);
		titleView = (TextView) findViewById(R.id.title);
	}

	/**
	 * 设置标题
	 * 
	 * @Title: setTitleName
	 * @Description: TODO
	 * @param textId
	 *            标题的资源id void
	 */
	public void setTitleName(int textId) {
		titleView.setText(textId);
	}

	/**
	 * 设置左边按钮文字
	 * 
	 * @Title: setLeftButtonText
	 * @Description: TODO
	 * @param textId
	 *            文字资源id void
	 */
	public void setLeftButtonText(int textId) {
		leftButton.setVisibility(View.VISIBLE);
		leftButton.setText(textId);
	}

	/**
	 * 设置左边按钮背景
	 * 
	 * @Title: setLeftButtonImg
	 * @Description: TODO
	 * @param resid
	 *            背景资源id void
	 */
	public void setLeftButtonImg(int resid) {
		leftButton.setVisibility(View.VISIBLE);
		leftButton.setBackgroundResource(resid);
	}

	/**
	 * 设置右边按钮文字
	 * 
	 * @Title: setRightButtonText
	 * @Description: TODO
	 * @param textId
	 *            void
	 */
	public void setRightButtonText(int textId) {
		rightButton.setVisibility(View.VISIBLE);
		rightButton.setText(textId);
	}

	/**
	 * 设置右边按钮背景
	 * 
	 * @Title: setRightButtonImg
	 * @Description: TODO
	 * @param resid
	 *            void
	 */
	public void setRightButtonImg(int resid) {
		rightButton.setVisibility(View.VISIBLE);
		rightButton.setBackgroundResource(resid);
	}

	/**
	 * 显示进度框
	 * 
	 * @Title: showProgressDialog
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showProgressDialog(String msg) {
		if (null == progressDialog) {
			progressDialog = new ProgressDialog(mContext);
		}
		progressDialog.setMessage(msg);
		progressDialog.setCancelable(true);
		progressDialog.show();
	}

	/**
	 * 关闭进度框
	 * 
	 * @Title: dismissProgressDialog
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	protected void dismissProgressDialog() {
		if (null != progressDialog && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	public Button getLeftButton() {
		return leftButton;
	}

}
