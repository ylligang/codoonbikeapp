package com.inwhoop.codoon.activity;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicHeader;
import org.json.JSONObject;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.entity.User;
import com.inwhoop.codoon.entity.UserToken;
import com.inwhoop.codoon.utils.JsonUtils;
import com.inwhoop.codoon.utils.Parameter;
import com.inwhoop.codoon.utils.SyncHttp;
import com.inwhoop.codoon.utils.Utils;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 注册界面
 * 
 * @Project: CodoonBikeApp
 * @Title: RegisterActivity.java
 * @Package com.inwhoop.codoon.activity
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-4-22 下午5:54:19
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class RegisterActivity extends BaseActivity {
	private final int REGISTER = 20;
	private final int LOGIN = 21;
	private User user;
	private EditText nameText;
	private EditText nickText;
	private EditText pwdText;
	private EditText repwdText;
	private Button regButton;
	private String email;
	private String nick;
	private String password;
	private String rePassword;
	private UserToken token;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		initView();
	}

	@Override
	public void initView() {
		super.initView();
		setTitleName(R.string.register);
		setLeftButtonText(R.string.login);
		nameText = (EditText) findViewById(R.id.login_user);
		nickText = (EditText) findViewById(R.id.login_name);
		pwdText = (EditText) findViewById(R.id.login_pwd);
		repwdText = (EditText) findViewById(R.id.re_pwd);
		regButton = (Button) findViewById(R.id.register_bt);
		regButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showProgressDialog("正在注册...");
				email = nameText.getText().toString();
				nick = nickText.getText().toString();
				password = pwdText.getText().toString();
				rePassword = repwdText.getText().toString();
				String msg = check();
				if (null != msg) {
					dismissProgressDialog();
					Toast.makeText(RegisterActivity.this, msg,
							Toast.LENGTH_SHORT).show();
				} else {
					new Thread(new Runnable() {

						@Override
						public void run() {
							Message message = handler.obtainMessage();
							message.what = REGISTER;
							message.obj = register(nick, email, password);
							handler.sendMessage(message);
						}
					}).start();
				}

			}
		});

		getLeftButton().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				RegisterActivity.this.finish();
			}
		});
	}

	private String register(String nick, String email, String password) {
		SyncHttp syncHttp = new SyncHttp();
		String params = "nick=" + nick + "&email=" + email + "&password="
				+ password;
		String result = "注册失败";
		try {
			result = syncHttp.httpGet(MyApplication.REGISTER, params, "");

			JSONObject jsonObject = new JSONObject(result);
			boolean rs = jsonObject.getBoolean("rs");
			if (rs) {
				user = JsonUtils.parseUser(jsonObject.getString("info"));
				if (null != user) {
					result = "注册成功";
				}
			} else {
				result = "昵称已经存在，注册失败";
			}
		} catch (Exception e) {
			System.out.println("Exception== " + e.toString());
			e.printStackTrace();
		}
		return result;

	}

	private String check() {
		String msg = null;
		if (Utils.isNull(email)) {
			msg = "账号不能为空";
		} else if (!Utils.isEmail(email)) {
			msg = "请输入正确的邮箱";
		} else if (Utils.isNull(nick)) {
			msg = "昵称不能为空";
		} else if (Utils.isNull(password)) {
			msg = "密码不能为空";
		} else if (Utils.isNull(rePassword)) {
			msg = "请再次输入密码";
		} else if (!password.equals(rePassword)) {
			msg = "两次输入密码不一致";
		}
		return msg;
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			// Toast.makeText(RegisterActivity.this, (CharSequence) msg.obj,
			// Toast.LENGTH_SHORT).show();

			switch (msg.what) {
			case REGISTER:
				Toast.makeText(RegisterActivity.this, (CharSequence) msg.obj,
						Toast.LENGTH_SHORT).show();
				if (((String) msg.obj).equals("注册成功")) {
					Utils.saveObject(mContext, MyApplication.USER_SP, user);
					login();
				}
				break;
			case MyApplication.READ_SUCCESS:
				getUser();
				break;
			case MyApplication.READ_FAIL:
				break;
			case LOGIN:
				if (null != msg.obj) {
					Utils.saveObject(RegisterActivity.this,
							MyApplication.USER_SP, (User) msg.obj);
					Intent intent = new Intent(RegisterActivity.this,
							SlidingActivity.class);
					intent.putExtra("first", true);
					Utils.savePreference(mContext, MyApplication.Token,
							token.getAccess_token());
					Utils.savePreference(mContext, MyApplication.USERID,
							token.getUser_id());
					Utils.savePreference(mContext, MyApplication.EXPIRE,
							token.getExpire_in() + "");
					Utils.savePreference(mContext, MyApplication.END_TIME,
							(System.currentTimeMillis() + token.getExpire_in())
									+ "");
					startActivity(intent);
					RegisterActivity.this.finish();
				}

				break;
			}
		}
	};

	private void login() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				token = JsonUtils.parseLogin(email, password);
				Message message = handler.obtainMessage();
				if (null == token) {
					message.what = MyApplication.READ_FAIL;
				} else {
					message.what = MyApplication.READ_SUCCESS;
				}
				handler.sendMessage(message);
			}
		}).start();
	}

	private void getUser() {
		showProgressDialog("正在获取用户信息...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message message = handler.obtainMessage();
				try {
					SyncHttp syncHttp = new SyncHttp();
					String result = "";
					// String token = Utils.getpreference(mContext,
					// MyApplication.Token);
					String path = "http://api.codoon.com/api/verify_credentials";
					result = syncHttp
							.httpGet(path, "", token.getAccess_token());
					User user = JsonUtils.parseUser(result);
					message.what = LOGIN;
					message.obj = user;

				} catch (Exception e) {
					message.what = MyApplication.READ_FAIL;
					System.out.println("ee==" + e.toString());
				}
				handler.sendMessage(message);
			}
		}).start();

	}
}
