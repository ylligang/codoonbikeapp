package com.inwhoop.codoon.entity;

import java.io.Serializable;

public class BikeSettingBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int perimeter;

	public int getPerimeter() {
		return perimeter;
	}

	public void setPerimeter(int perimeter) {
		this.perimeter = perimeter;
	}

}
