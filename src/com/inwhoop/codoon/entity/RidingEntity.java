package com.inwhoop.codoon.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 骑行流水数据上传实体类
 * 
 * @Project: CodoonBikeApp
 * @Title: RidingEntity.java
 * @Package com.inwhoop.codoon.entity
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-5-12 下午4:59:39
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class RidingEntity implements Serializable {

	private EquipmentInfoBean info;
	private List<RidingDataBean> data;

	public RidingEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RidingEntity(EquipmentInfoBean info, List<RidingDataBean> data) {
		super();
		this.info = info;
		this.data = data;
	}

	public EquipmentInfoBean getInfo() {
		return info;
	}

	public void setInfo(EquipmentInfoBean info) {
		this.info = info;
	}

	public List<RidingDataBean> getData() {
		return data;
	}

	public void setData(List<RidingDataBean> data) {
		this.data = data;
	}

}
