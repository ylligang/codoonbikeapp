package com.inwhoop.codoon.entity;

import java.io.Serializable;

public class User implements Serializable {
	public String id;
	public String certificatename;
	public String get_icon_large;
	public int height;
	public int _auto_id;
	// public boolean is_newuser;
	public String location;
	public int followers;
	public String installed_apps;
	public String realname;
	// public boolean emailverified;
	public int stridelength;
	public String group_ids;
	public String hobby;
	public String get_icon_xlarge;
	public int fighting_level;
	public String get_icon_small;
	public String gender;
	public String descroption;
	public String[] mobile_portraits;
	public int followings;
	public String get_icon_tiny;
	public String mobilenumber;
	public String domain;
	public int weight;
	public int week_goal_value;
	public RegistDate regist_date;
	public String portrait;
	public String nick;
	public String get_icon_middle;
	public int last_login;
	public String email;
	public String certificateinfo;
	public String tmp_portrait;
	public String verify_code;
	public Birthday birthday;
	public String address;
	public String certificateid;
	public String[] mobile_portraits_l;
	public int age;
	public String week_goal_type;
	public String[] mobile_portraits_x;
	public int runstridelength;

	// public boolean mobileverified;

	public class RegistDate implements Serializable {
		public int year;
		public int month;
		public int day;
	}

	public class Birthday implements Serializable {
		public int y;
		public int m;
		public int d;
	}
}
