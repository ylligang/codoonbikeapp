package com.inwhoop.codoon.entity;

import java.io.Serializable;

/**
 * 
 * 刻度表
 * 
 * @Project: codoonbikeapp
 * @Title: ScaleInfo.java
 * @Package com.inwhoop.codoon.entity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-5-10 下午2:26:24
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ScaleInfo implements Serializable {

	public int number; // 所在刻度所对应的刻度份数
	public int position; // 所在刻度

}
