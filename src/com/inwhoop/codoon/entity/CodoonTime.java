package com.inwhoop.codoon.entity;

public class CodoonTime {

	public String year;
	public String time;
	public String second;
	public boolean isitem;

	public CodoonTime(String year, String time, String second) {
		this.year = year;
		this.time = time;
		this.second = second;
	}

}
