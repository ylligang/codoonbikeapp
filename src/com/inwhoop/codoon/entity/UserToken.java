package com.inwhoop.codoon.entity;

/**
 * 用户登录数据实体
 * 
 * @Project: CodoonBikeApp
 * @Title: UserToken.java
 * @Package com.inwhoop.codoon.entity
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-5-15 上午10:56:42
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class UserToken {

	private String user_id;
	private String access_token;
	private String token_ty;
	private String scope;
	private long expire_in;
	private String refresh_token;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getToken_ty() {
		return token_ty;
	}

	public void setToken_ty(String token_ty) {
		this.token_ty = token_ty;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public long getExpire_in() {
		return expire_in;
	}

	public void setExpire_in(long expire_in) {
		this.expire_in = expire_in;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

}
