package com.inwhoop.codoon.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BikeAllData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public List<BikeData> periods = new ArrayList<BikeData>();
	public BikeScore statistic;
}
