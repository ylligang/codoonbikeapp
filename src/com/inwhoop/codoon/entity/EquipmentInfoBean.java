package com.inwhoop.codoon.entity;

import java.io.Serializable;

/**
 * 设备信息
 * 
 * @Project: CodoonBikeApp
 * @Title: EquipmentInfoBean.java
 * @Package com.inwhoop.codoon.entity
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-5-12 下午4:58:32
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class EquipmentInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String product_id;
	private String hard_version;
	private String soft_version;

	public EquipmentInfoBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EquipmentInfoBean(String product_id, String hard_version,
			String soft_version) {
		super();
		this.product_id = product_id;
		this.hard_version = hard_version;
		this.soft_version = soft_version;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getHard_version() {
		return hard_version;
	}

	public void setHard_version(String hard_version) {
		this.hard_version = hard_version;
	}

	public String getSoft_version() {
		return soft_version;
	}

	public void setSoft_version(String soft_version) {
		this.soft_version = soft_version;
	}

}
