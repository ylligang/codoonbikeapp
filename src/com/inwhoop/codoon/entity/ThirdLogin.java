package com.inwhoop.codoon.entity;

public class ThirdLogin {
	// @"token": 第三方的token,
	// @"secret": 第三方的refresh token,
	// @"external_user_id": 第三方的userId,
	// @"source": ‘qq/sina/qq2/renren’,
	// @"expire_in": 第三方的过期时间,
	// @"catalog": ‘写死 codoon_oauth2.0’,
	// @"client_id": '咕咚的clientID',
	// @"device_token": ‘留空’

	private String token;
	private String secret;
	private String external_user_id;
	private String source;
	private long expire_in;
	private String catalog;
	private String client_id;
	private String device_token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getExternal_user_id() {
		return external_user_id;
	}

	public void setExternal_user_id(String external_user_id) {
		this.external_user_id = external_user_id;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public long getExpire_in() {
		return expire_in;
	}

	public void setExpire_in(long expire_in) {
		this.expire_in = expire_in;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getDevice_token() {
		return device_token;
	}

	public void setDevice_token(String device_token) {
		this.device_token = device_token;
	}

}
