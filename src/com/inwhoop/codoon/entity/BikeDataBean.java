package com.inwhoop.codoon.entity;

public class BikeDataBean {

	private String start_time;// datatime ，获取时间段的第一个时间，
	private int count;// int，需要获取的个数

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
