package com.inwhoop.codoon.entity;

import java.io.Serializable;

public class BikeData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String start_time;// 一段骑行开始时间,
	public String end_time; // 一段骑行结束时间 ,
	public float distance; // 一段骑行的距离,
	public float calories;// 一段骑行消耗的卡路里,
	public float avg_speed;// 一段骑行的平均速度 ,
	public float avg_pedal_rate;// 一段骑行的平均踏频 ,
	public float max_speed;// 一段骑行的最快速度 ,
	public float max_pedal_rate;// 一段骑行的最快踏频

	public BikeData() {

	}

	public BikeData(String start_time, String end_time, int distance,
			int calories, int avg_speed, int avg_pedal_rate, int max_speed,
			int max_pedal_rate) {
		super();
		this.start_time = start_time;
		this.end_time = end_time;
		this.distance = distance;
		this.calories = calories;
		this.avg_speed = avg_speed;
		this.avg_pedal_rate = avg_pedal_rate;
		this.max_speed = max_speed;
		this.max_pedal_rate = max_pedal_rate;
	}

}
