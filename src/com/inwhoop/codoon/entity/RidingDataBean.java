package com.inwhoop.codoon.entity;

import java.io.Serializable;

/**
 * 骑行流水数据
 * 
 * @Project: CodoonBikeApp
 * @Title: RidingDataBean.java
 * @Package com.inwhoop.codoon.entity
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-5-12 下午4:58:55
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class RidingDataBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 一段骑行开始时间 string 2013-09-21 18:02:58 */
	private String start_time;
	/** 一段骑行结束时间 */
	private String end_time;
	/** 一段骑行的距离 */
	private float distance;
	/** 一段骑行消耗的卡路里 */
	private float calories;
	/** 一段骑行的平均速度 */
	private float avg_speed;
	/** 一段骑行的平均踏频 */
	private float avg_pedal_rate;
	/** 一段骑行的最快速度 */
	private float max_speed;
	/** 一段骑行的最快踏频 */
	private float max_pedal_rate;

	public RidingDataBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RidingDataBean(String start_time, String end_time, float distance,
			float calories, float avg_speed, float avg_pedal_rate,
			float max_speed, float max_pedal_rate) {
		super();
		this.start_time = start_time;
		this.end_time = end_time;
		this.distance = distance;
		this.calories = calories;
		this.avg_speed = avg_speed;
		this.avg_pedal_rate = avg_pedal_rate;
		this.max_speed = max_speed;
		this.max_pedal_rate = max_pedal_rate;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public float getCalories() {
		return calories;
	}

	public void setCalories(float calories) {
		this.calories = calories;
	}

	public float getAvg_speed() {
		return avg_speed;
	}

	public void setAvg_speed(float avg_speed) {
		this.avg_speed = avg_speed;
	}

	public float getAvg_pedal_rate() {
		return avg_pedal_rate;
	}

	public void setAvg_pedal_rate(float avg_pedal_rate) {
		this.avg_pedal_rate = avg_pedal_rate;
	}

	public float getMax_speed() {
		return max_speed;
	}

	public void setMax_speed(float max_speed) {
		this.max_speed = max_speed;
	}

	public float getMax_pedal_rate() {
		return max_pedal_rate;
	}

	public void setMax_pedal_rate(float max_pedal_rate) {
		this.max_pedal_rate = max_pedal_rate;
	}

}
