package com.inwhoop.codoon.entity;

import java.io.Serializable;

public class BikeSettingEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private EquipmentInfoBean info;
	private BikeSettingBean data;

	public EquipmentInfoBean getInfo() {
		return info;
	}

	public void setInfo(EquipmentInfoBean info) {
		this.info = info;
	}

	public BikeSettingBean getData() {
		return data;
	}

	public void setData(BikeSettingBean data) {
		this.data = data;
	}

}
