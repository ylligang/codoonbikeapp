package com.inwhoop.codoon.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.activity.MainActivity;
import com.inwhoop.codoon.activity.SlidingActivity;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.utils.Utils;

/**
 * 
 * 程序设置
 * 
 * @Project: codoonbikeapp
 * @Title: OrderSetFragment.java
 * @Package com.inwhoop.codoon.fragment
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-5-10 下午5:31:10
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class OrderSetFragment extends BaseFragment implements OnClickListener {

	private SeekBar secondBar, miniBar;

	private TextView secondTextView, miniTextView;

	private ImageView gpsImageView, loginImageView;

	private int second = 0; // 表示语音播报的间隔

	private int min = 1; // 表示数据同步的间隔

	private boolean isgps = true; // 表示是否开启gps同步

	private boolean islogin = true; // 表示是否开启自动登录

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.order_info_layout, null);
		init(view);
		return view;
	}

	@Override
	public void init(View view) {
		super.init(view);
		setTitleText(R.string.orderset);
		setLeftBtBack(R.drawable.ic_top_menu, true);
		secondBar = (SeekBar) view.findViewById(R.id.secondbar);
		secondBar.setMax(60);
		secondBar.setProgress(Utils.getIntpreference(getActivity(),
				MyApplication.YUYIN, 0));
		miniBar = (SeekBar) view.findViewById(R.id.minibar);
		miniBar.setMax(29);
		miniBar.setProgress(Utils.getIntpreference(getActivity(),
				MyApplication.DATA, 3));
		((SlidingActivity) getActivity()).menu.addIgnoredView(secondBar);
		((SlidingActivity) getActivity()).menu.addIgnoredView(miniBar);
		secondTextView = (TextView) view.findViewById(R.id.secondtext);
		miniTextView = (TextView) view.findViewById(R.id.minitext);
		setSecondText(Utils.getIntpreferenceset(getActivity(),
				MyApplication.YUYIN, 0));
		setminText(Utils.getIntpreferenceset(getActivity(), MyApplication.DATA, 3));
		gpsImageView = (ImageView) view.findViewById(R.id.gpsimg);
		gpsImageView.setOnClickListener(this);
		loginImageView = (ImageView) view.findViewById(R.id.loginimg);
		loginImageView.setOnClickListener(this);
		if (!Utils.getBooleanpreference(getActivity(), MyApplication.GPS)) {
			gpsImageView.setBackgroundResource(R.drawable.ico_switch_off);
		} else {
			gpsImageView.setBackgroundResource(R.drawable.btn_switch_on);
		}

		if (!Utils
				.getBooleanpreference(getActivity(), MyApplication.AUTO_LOGIN)) {
			loginImageView.setBackgroundResource(R.drawable.ico_switch_off);
		} else {
			loginImageView.setBackgroundResource(R.drawable.btn_switch_on);
		}
		secondBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				setSecondText(progress);
				Utils.saveIntPreferenceset(getActivity(), MyApplication.YUYIN,
						progress);

			}
		});
		miniBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				setminText(progress + 1);
				Utils.saveIntPreferenceset(getActivity(), MyApplication.DATA,
						progress + 1);
				// MyApplication.spacetime = progress + 1;
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.gpsimg:
			if (!Utils.getBooleanpreference(getActivity(), MyApplication.GPS)) {
				gpsImageView.setBackgroundResource(R.drawable.btn_switch_on);
				isgps = true;
			} else {
				gpsImageView.setBackgroundResource(R.drawable.ico_switch_off);
				isgps = false;
			}
			Utils.saveBooleanPreference(getActivity(), MyApplication.GPS, isgps);
			break;

		case R.id.loginimg:
			if (!Utils.getBooleanpreference(getActivity(),
					MyApplication.AUTO_LOGIN)) {
				loginImageView.setBackgroundResource(R.drawable.btn_switch_on);
				islogin = true;
			} else {
				loginImageView.setBackgroundResource(R.drawable.ico_switch_off);
				islogin = false;
				Utils.clear(getActivity(), MyApplication.USER_INFO);
				Utils.clear(getActivity(), MyApplication.USER_SP);
			}
			Utils.saveBooleanPreference(getActivity(),
					MyApplication.AUTO_LOGIN, islogin);
			break;

		default:
			break;
		}
	}

	/**
	 * 设置语音播报
	 * 
	 * @Title: setSecondText
	 * @Description: TODO
	 * @param @param minute
	 * @return void
	 */
	private void setSecondText(int minute) {
		secondTextView.setText(minute + "分钟");
	}

	/**
	 * 设置数据同步
	 * 
	 * @Title: setminText
	 * @Description: TODO
	 * @param @param minute
	 * @return void
	 */
	private void setminText(int minute) {
		miniTextView.setText(minute + "分钟");
	}

}
