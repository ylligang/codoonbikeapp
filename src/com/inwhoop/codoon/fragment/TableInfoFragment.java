package com.inwhoop.codoon.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.utils.Utils;

/**
 * @Project: codoonbikeapp
 * @Title: TableInfoFragment.java
 * @Package com.inwhoop.codoon.fragment
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-5-10 下午6:05:33
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class TableInfoFragment extends BaseFragment {

	private TextView versionTextView, idTextView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.table_info_layout, null);
		init(view);
		return view;
	}

	@Override
	public void init(View view) {
		super.init(view);
		setTitleText(R.string.tableinfo);
		setLeftBtBack(R.drawable.ic_top_menu, true);
		versionTextView = (TextView) view.findViewById(R.id.version);
		idTextView = (TextView) view.findViewById(R.id.pid);
		if (null != getActivity()) {
			versionTextView.setText(Utils.getpreference(getActivity(),
					MyApplication.VERSION));
			idTextView.setText(Utils.getpreference(getActivity(),
					MyApplication.DEV_ID));
		}
		// versionTextView.setText(Utils.getpreference(getActivity(),
		// MyApplication.VERSION));
		// idTextView.setText(Utils.getpreference(getActivity(),
		// MyApplication.DEV_ID));
	}

}
