package com.inwhoop.codoon.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.activity.LoginActivity;
import com.inwhoop.codoon.activity.MainActivity;
import com.inwhoop.codoon.activity.SlidingActivity;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.entity.User;
import com.inwhoop.codoon.service.BlueConectService;
import com.inwhoop.codoon.service.BlueService;
import com.inwhoop.codoon.utils.BitmapManager;
import com.inwhoop.codoon.utils.Utils;
import com.inwhoop.codoon.view.CircularImage;
import com.inwhoop.codoon.view.CustomImageView;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class MenuFragment extends Fragment implements OnClickListener {

	private FragmentManager mFm = null;
	public TextView loginView = null;
	public CircularImage customImageView;
	public TextView nickView;
	public TextView bindText;

	public MainFragment mainFragment = null;

	public StartBindFragment startBindFragment = null;

	public SetFragment setFragment = null;

	private OrderSetFragment oderFragment = null;

	private TableInfoFragment tableFragment = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mFm = getFragmentManager();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.left_menu, null);
		init(view);
		return view;
	}

	private void init(View view) {
		int img[] = { R.drawable.ic_set_logo, R.drawable.ic_set_add,
				R.drawable.ic_set_mbsz, R.drawable.ic_set_cxsz,
				R.drawable.ic_set_info };
		ListView listView = (ListView) view.findViewById(R.id.menu_list);
		loginView = (TextView) view.findViewById(R.id.login_text);
		customImageView = (CircularImage) view.findViewById(R.id.userImage);
		nickView = (TextView) view.findViewById(R.id.username_tv);
		bindText = (TextView) view.findViewById(R.id.username_role);
		String text[] = getResources().getStringArray(R.array.menu);
		List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < text.length; i++) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("text", text[i]);
			if (i < img.length) {
				map.put("img", img[i]);
			}
			data.add(map);
		}
		SimpleAdapter adapter = new SimpleAdapter(getActivity(), data,
				R.layout.menu_item, new String[] { "text", "img" }, new int[] {
						R.id.text, R.id.img });
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				switchPosition(arg2);
			}
		});
		loginView.setOnClickListener(this);
	}

	private void switchPosition(int position) {
		FragmentTransaction ft = mFm.beginTransaction();
		String isBind = Utils.getpreference(getActivity(), MyApplication.BIND);
		if (position == 1 && isBind.equals(MyApplication.SUCESS)) {
			if (null != getActivity()) {
				dialog();
			}
		} else {
			if (null != mainFragment) {
				ft.hide(mainFragment);
			}
			if (null != startBindFragment) {
				// ft.hide(startBindFragment);
				ft.remove(startBindFragment);
				startBindFragment = null;
			}
			if (null != setFragment) {
				ft.remove(setFragment);
				setFragment = null;
			}
			if (null != oderFragment) {
				ft.hide(oderFragment);
			}
			if (null != tableFragment) {
				ft.hide(tableFragment);
			}
			switch (position) {
			case 0:
				MyApplication.flag = true;
				if (null != mainFragment) {
					try {
						boolean isRun = Utils.isServiceWorked(getActivity(),
								"com.inwhoop.codoon.service.BlueService");
						if (!isRun) {
							mainFragment.cFragment.intent = new Intent(
									getActivity(), BlueService.class);
							getActivity().startService(
									mainFragment.cFragment.intent);
						}
					} catch (Exception e) {

					}
					ft.show(mainFragment);
				} else {
					mainFragment = new MainFragment();
					Bundle bundle = new Bundle();
					bundle.putString("fag", "");
					mainFragment.setArguments(bundle);
					ft.add(R.id.content_frame, mainFragment);
				}
				break;
			case 1:
				// if (null != startBindFragment) {
				// ft.show(startBindFragment);
				// } else {
				MyApplication.flag = false;
				startBindFragment = new StartBindFragment();
				ft.add(R.id.content_frame, startBindFragment);
				// }
				break;
			case 2:
				// if (null != setFragment) {
				// ft.show(setFragment);
				// } else {
				setFragment = new SetFragment();
				ft.add(R.id.content_frame, setFragment);
				// }
				break;
			case 3:
				if (null != oderFragment) {
					ft.show(oderFragment);
				} else {
					oderFragment = new OrderSetFragment();
					ft.add(R.id.content_frame, oderFragment);

				}
				break;
			case 4:
				if (null != tableFragment) {
					ft.show(tableFragment);
				} else {
					tableFragment = new TableInfoFragment();
					ft.add(R.id.content_frame, tableFragment);
				}
				break;
			}
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.commitAllowingStateLoss();
			((SlidingActivity) getActivity()).menu.showContent();

		}

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		String token = Utils.getpreference(getActivity(), MyApplication.Token);
		if (!token.equals("")) {
			// if (!Utils.getBooleanpreference(getActivity(),
			// MyApplication.AUTO_LOGIN)) {
			// loginView.setText("登录");
			// } else {

			User user = (User) Utils.readObject(getActivity(),
					MyApplication.USER_SP);
			if (null != user) {
				nickView.setText(user.nick);
				BitmapManager.INSTANCE.loadBitmap(user.get_icon_middle,
						customImageView, R.drawable.user_img, true);
				loginView.setText("注销");
			}
			// }
		} else {
			loginView.setText("登录");
			nickView.setText("咕咚用户昵称");
			customImageView.setImageResource(R.drawable.user_img);
		}

		String isBind = Utils.getpreference(getActivity(), MyApplication.BIND);
		if (isBind.equals(MyApplication.SUCESS)) {
			bindText.setText(R.string.binded);
		}
	}
	@Override
	public void onClick(View v) {
		if (loginView.getText().toString().contains("登录")) {
			Intent intent = new Intent(getActivity(), LoginActivity.class);
			intent.putExtra("first", false);
			startActivity(intent);
			getActivity().finish();
		} else if (loginView.getText().toString().contains("注销")) {
			Utils.clear(getActivity(), MyApplication.SP);
			Utils.clear(getActivity(), MyApplication.USER_INFO);
			Utils.clear(getActivity(), MyApplication.USER_SP);
			Utils.clear(getActivity(), MyApplication.AUTO_LOGIN);
			Utils.clear(getActivity(), MyApplication.GPS);
			loginView.setText("登录");
			nickView.setText("咕咚用户昵称");
			customImageView.setImageResource(R.drawable.user_img);
		}

	}

	protected void dialog() {
		AlertDialog.Builder builder = new Builder(getActivity());
		builder.setMessage("已经绑定咕咚码表，继续绑定将解除已有的码表，是否继续？");
		builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Utils.savePreference(getActivity(), MyApplication.BIND,
						MyApplication.FAIL);
				Utils.saveIntPreference(getActivity(), MyApplication.SPEED, 0);
				Utils.saveIntPreference(getActivity(), MyApplication.CARDEN, 0);
				Utils.saveIntPreference(getActivity(), MyApplication.CIRCLE, 0);
				bindText.setText(R.string.no_bind);
				if (null != ((SlidingActivity) getActivity()).serIntent) {
					// ((SlidingActivity) getActivity()).serIntent = new Intent(
					// getActivity(), BlueConectService.class);
					getActivity().stopService(
							((SlidingActivity) getActivity()).serIntent);
				}
				if (null != ((SlidingActivity) getActivity()).intentPlay) {
					getActivity().stopService(
							((SlidingActivity) getActivity()).intentPlay);
				}
				if (null != ((SlidingActivity) getActivity()).intentData) {
					getActivity().stopService(
							((SlidingActivity) getActivity()).intentData);
				}
				mainFragment.cFragment.setElectri();
				// MyApplication.flag = false;
				// MainFragment mainFragment = new MainFragment();
				// FragmentTransaction ft = getActivity()
				// .getSupportFragmentManager().beginTransaction();
				// Bundle bundle = new Bundle();
				// bundle.putString("fag", "");
				// mainFragment.setArguments(bundle);
				// ft.replace(R.id.content_frame, mainFragment, "mainFragment");
				// ft.commitAllowingStateLoss();
				switchPosition(0);
				BluetoothAdapter blueAdapter = BluetoothAdapter
						.getDefaultAdapter();
				blueAdapter.disable();
				blueAdapter = null;
				MyApplication.mSyncDeviceDataManager = null;
			}
		});

		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();

			}
		});
		builder.create().show();
	}

}
