package com.inwhoop.codoon.fragment;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.activity.MyMapActivity;
import com.inwhoop.codoon.activity.SlidingActivity;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.entity.BikeAllData;
import com.inwhoop.codoon.entity.BikeData;
import com.inwhoop.codoon.entity.CodoonTime;
import com.inwhoop.codoon.entity.User;
import com.inwhoop.codoon.entity.UserToken;
import com.inwhoop.codoon.graph.GraphView;
import com.inwhoop.codoon.graph.GraphView.GraphViewData;
import com.inwhoop.codoon.graph.GraphViewSeries;
import com.inwhoop.codoon.graph.GraphViewSeries.GraphViewSeriesStyle;
import com.inwhoop.codoon.graph.LineGraphView;
import com.inwhoop.codoon.utils.JsonUtils;
import com.inwhoop.codoon.utils.Utils;
import com.inwhoop.codoon.view.PullListView;
import com.inwhoop.codoon.view.PullListView.OnRefreshListener;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class GraphFragment extends Fragment implements OnClickListener,
		OnRefreshListener {

	private HorizontalScrollView hScrollView;
	private LinearLayout layout;
	// private MyGraphView myGraphView;
	private int num = 0;
	private GraphViewData[] data;
	private GraphViewData[] dataSpeed;
	private GraphView graphView;
	private double v = 0;
	private ListView listView = null;
	private MyAdapter myAdapter = null;
	private List<CodoonTime> codoonTimes = new ArrayList<CodoonTime>();
	private BikeAllData bikeDatas;
	
	private BikeAllData bikeDataspre;

	private LinearLayout nowlayout = null;

	private TextView year, month, hour;

	private ImageView mapView = null;
	private int map_index = 0;

	private List<BikeData> bikelist = new ArrayList<BikeData>();

	private List<BikeData> list = new ArrayList<BikeData>();
	
	private boolean isOver = true;
	
	private boolean isReading = true;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.graph, null);
		layout = (LinearLayout) view.findViewById(R.id.graph_layout);
		listView = (ListView) view.findViewById(R.id.list_layout);
//		listView.setRefreshListener(this);
		year = (TextView) view.findViewById(R.id.year_text);
		month = (TextView) view.findViewById(R.id.time_text);
		hour = (TextView) view.findViewById(R.id.second_text);
		nowlayout = (LinearLayout) view.findViewById(R.id.nowlayout);
		mapView = (ImageView) view.findViewById(R.id.h_map);
		// MyGraphView myGraphView=new MyGraphView(getActivity());
		// layout.addView(myGraphView);
		mapView.setOnClickListener(this);
		return view;
	}

	private void getHistoryData() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				// for (int i = 0; i < 20; i++) {
				// BikeData bikeData = new BikeData("2014-12-1 12:09:07",
				// "2014-12-1 12:12:07", 100, 210, 22+i, 32+i, 113, 133);
				// bikeDatas.add(bikeData);
				// }
				// UserToken token = (UserToken) Utils.readObject(getActivity(),
				// MyApplication.USER_LOGIN);
				bikeDatas = JsonUtils.parseHistoryData(Utils.getpreference(
						getActivity(), MyApplication.USERID), Utils
						.getpreference(getActivity(), MyApplication.Token));
				Message message = handler.obtainMessage();
				if (null == bikeDatas) {
					message.what = MyApplication.READ_FAIL;
				} else {
					message.what = MyApplication.READ_SUCCESS;
				}
				handler.sendMessage(message);
			}
		}).start();

	}

	// private String getYear(String date) {
	// String year = "";
	// int end = date.lastIndexOf("-");
	// year = date.substring(0, end + 3);
	// return year;
	// }
	//
	// private String getHour(String startTime, String endTime) {
	// int start1 = startTime.lastIndexOf("-");
	// int end1 = startTime.lastIndexOf(":");
	// String mon1 = "";
	// mon1 = startTime.substring(start1 + 2, end1).trim();
	//
	// int start = endTime.lastIndexOf("-");
	// int end = endTime.lastIndexOf(":");
	// String mon2 = "";
	// mon2 = endTime.substring(start + 2, end).trim();
	// return mon1 + "-" + mon2;
	// }
	//
	// private String getTime(int min) {
	// String time = "";
	// if (min < 10) {
	// time = "00h0" + min + "'";
	// } else if (min < 60) {
	// time = "00h" + min + "'";
	// }
	//
	// return time;
	//
	// }

	private void drawCurve(List<BikeData> list) {
		List<CodoonTime> codoonTimeslist = new ArrayList<CodoonTime>();
		int prepos = bikelist.size();
//		System.out.println("=======BikeData============" + list.size());
//		System.out.println("=======BikeData============" + prepos);
		layout.removeAllViews();
		for (int i = 0; i < list.size(); i++) {
			bikelist.add(list.get(i));
		}
		float speed[] = new float[bikelist.size()];
		float tapin[] = new float[bikelist.size()];
		// int speed[] = { 20, 30, 40, 30, 50, 55, 68, 76, 50, 32, 49, 20, 30,
		// 40,
		// 30, 50, 55, 68, 76, 50, 32, 49 };
		// int tapin[] = { 25, 37, 40, 36, 40, 65, 60, 56, 70, 82, 69, 25, 37,
		// 40,
		// 36, 40, 65, 60, 56, 70, 82, 69 };
		// for (int i = 0; i < bikeDatas.size(); i++) {
		// speed[i] = bikeDatas.get(i).avg_speed;
		// tapin[i] = bikeDatas.get(i).avg_pedal_rate;
		// }
		// CodoonTime cTime = new CodoonTime("2014-1-1", "12:00-13:00",
		// "01h0"
		// + i + "'");
		for (int i = 0; i < list.size(); i++) {
			CodoonTime cTime = new CodoonTime(
					Utils.getYear(list.get(i).start_time), Utils.getHour(
							list.get(i).start_time, list.get(i).end_time),
					Utils.getTime(list.get(i).start_time, list.get(i).end_time));
			cTime.isitem = true;
			codoonTimeslist.add(cTime);
		}
		for (int i = 0; i < bikelist.size(); i++) {
			speed[i] = Math.round((bikelist.get(i).avg_speed * 100) / 100);
			tapin[i] = Math.round((bikelist.get(i).avg_pedal_rate * 100) / 100);
		}
		
		if (list.size() < 15) {
//			listView.isRefreshable = false;
			isOver = false;
			for (int i = 0; i < 5; i++) {
				CodoonTime cTime = new CodoonTime("", "", "");
				cTime.isitem = false;
				codoonTimeslist.add(cTime);
			}
		}
		num = speed.length;

		data = new GraphViewData[num];
		dataSpeed = new GraphViewData[num];
		v = 0;
		for (int i = 0; i < num; i++) {
			v += 0.2;
			data[i] = new GraphViewData(i, tapin[i]);
			dataSpeed[i] = new GraphViewData(i, speed[i]);
		}
		myAdapter.addList(codoonTimeslist);
		myAdapter.notifyDataSetChanged();

		// 踏频25a7aa
		graphView = new LineGraphView(getActivity(), "");
		graphView.setListView(listView);
		graphView.setCodoonTimes(myAdapter.getAll());
		graphView.setYear(year);
		graphView.setMonth(month);
		graphView.setHour(hour);
		((SlidingActivity) getActivity()).menu.addIgnoredView(layout);
//		System.out.println("=========BikeData======11===="+myAdapter.getAll().size());
		// ((LineGraphView) graphView).setBackgroundResource(R.drawable.graph);
		((LineGraphView) graphView).setDrawBackground(true);
		// add data
		GraphViewSeries seriesTa = new GraphViewSeries("tapin",
				new GraphViewSeriesStyle(0xff528d4d, 3), data);
		graphView.addSeries(seriesTa);
		GraphViewSeries seriesSpeed = new GraphViewSeries("speed",
				new GraphViewSeriesStyle(0x8f25a7aa, 3), dataSpeed);
		graphView.addSeries(seriesSpeed);
		// set view port, start=2, size=10
		graphView.setViewPort(2, 3);
		graphView.setViewPort(-7, 10);
		graphView.setScalable(true);
		// set manual Y axis bounds
		graphView.setManualYAxisBounds(130, 0);

		layout.addView(graphView);
		
//		graphView.setMinX(prepos);
//		graphView.graphViewContentView.invalidate();

		listView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				map_index = firstVisibleItem;
				if(isReading&&isOver&&((firstVisibleItem+visibleItemCount)==totalItemCount)){
					readMore();
				}
				if (graphView.isLeave) {
					if (firstVisibleItem < myAdapter.getAll().size()) {
						if (!myAdapter.getAll().get(firstVisibleItem).isitem) {
							listView.setSelection(myAdapter.getAll().size() - 6);
						} else {
							nowlayout.setVisibility(View.VISIBLE);
							year.setText(""
									+ myAdapter.getAll().get(firstVisibleItem).year);
							month.setText(""
									+ myAdapter.getAll().get(firstVisibleItem).time);
							hour.setText(""
									+ myAdapter.getAll().get(firstVisibleItem).second);
							// codoonTimes.get(firstVisibleItem).isf = true;
							myAdapter.notifyDataSetChanged();
//							if(isOver){
//								graphView.setMinX(firstVisibleItem);
//							}else{
								graphView.setMinX(firstVisibleItem - 7);
//							}
							graphView.graphViewContentView.invalidate();
						}
					}
				}
			}
		});
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
		if (!hidden) {
			// myAdapter = new MyAdapter();
			// listView.setAdapter(myAdapter);
			getHistoryData();
			// myAdapter.getAll().clear();
			// myAdapter.clear();
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		myAdapter = new MyAdapter(codoonTimes);
		listView.setAdapter(myAdapter);
	}

	private class MyAdapter extends BaseAdapter {
		
		private List<CodoonTime> codlist;

		public MyAdapter(List<CodoonTime> codlist){
			this.codlist = codlist;
		}
		
		public void addList(List<CodoonTime> addlist){
			for (int i = 0; i < addlist.size(); i++) {
				codlist.add(addlist.get(i));
			}
		}
		
		public List<CodoonTime> getAll(){
			return codlist;
		}
		
		public void clear(){
			codlist.clear();
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return codlist.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return codlist.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			position = position + 1;
			if (null == convertView) {
				holder = new ViewHolder();
				convertView = LayoutInflater.from(getActivity()).inflate(
						R.layout.item_layout, null);
				holder.yearText = (TextView) convertView
						.findViewById(R.id.year_text);
				holder.timeText = (TextView) convertView
						.findViewById(R.id.time_text);
				holder.secondText = (TextView) convertView
						.findViewById(R.id.second_text);
				holder.layout = (LinearLayout) convertView
						.findViewById(R.id.layout);
				holder.itemlayout = (LinearLayout) convertView
						.findViewById(R.id.itemlayout);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			if (position < codlist.size()) {
				if (codlist.get(position).isitem) {
					holder.layout.setBackgroundDrawable(null);
					holder.itemlayout.setBackgroundResource(R.drawable.item_bg);
				} else {
					holder.layout.setBackgroundDrawable(null);
					holder.itemlayout.setBackgroundDrawable(null);
				}
				holder.yearText.setText(codlist.get(position).year);
				holder.timeText.setText(codlist.get(position).time);
				holder.secondText.setText(codlist.get(position).second);
				holder.secondText.setText(codlist.get(position).second);
			}
			return convertView;
		}

	}

	class ViewHolder {
		TextView yearText;
		TextView timeText;
		TextView secondText;
		LinearLayout layout, itemlayout;
	}

	// private void getDate(final String start_time, final int count) {
	// new Thread(new Runnable() {
	//
	// @Override
	// public void run() {
	// bikeDatas = JsonUtils.getBikeData(start_time, count,
	// MyApplication.Token);
	// Message msg = handler.obtainMessage();
	// if (bikeDatas.size() == 0) {
	// msg.what = MyApplication.READ_FAIL;
	// } else {
	// msg.what = MyApplication.READ_SUCCESS;
	// }
	// handler.sendMessage(msg);
	// }
	// }).start();
	//
	// }

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case MyApplication.READ_SUCCESS:
				if (null != bikeDatas.periods&&bikeDatas.periods.size()>0) {
					codoonTimes.clear();
					bikelist.clear();
					myAdapter = new MyAdapter(codoonTimes);
					listView.setAdapter(myAdapter);
					isReading = true;
					isOver = true;
					bikeDataspre = bikeDatas;
					drawCurve(bikeDatas.periods);
				}
				break;

			case MyApplication.READ_FAIL:
				break;
			}

		};
	};

	@Override
	public void onClick(View v) {
		if (null != bikeDataspre) {
			Intent it = new Intent(getActivity(), MyMapActivity.class);
			if (map_index < bikeDataspre.periods.size()) {
//				System.out.println("map_index------------");
//				System.out.println("map_index------------"
//						+ bikeDatas.statistic.total_calories);
				it.putExtra("bike", bikelist.get(map_index));
				it.putExtra("allbike", bikeDataspre.statistic.total_calories);
				it.putExtra("alldistance", bikeDataspre.statistic.total_distance);
			} else {
//				System.out.println("map_index==============");
				it.putExtra("bike", new BikeData());
				it.putExtra("allbike", 0);
				it.putExtra("alldistance", 0);
			}
			getActivity().startActivity(it);
		}
	}

	@Override
	public void onRefresh() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getBikeData(
							bikelist.get(bikelist.size() - 1).start_time, 15,
							Utils.getpreference(getActivity(),
									MyApplication.Token));
					msg.what = MyApplication.READ_SUCCESS;
//					System.out.println("list= "+list.get(0).avg_speed);
				}catch(Exception e){
					System.out.println("=========resultresult==============="+e.toString());
					msg.what = MyApplication.READ_FAIL;
				}

				addhandler.sendMessage(msg);
			}
		}).start();
	}
	
	private void readMore(){
		isReading = false;
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getBikeData(
							bikelist.get(bikelist.size() - 1).start_time, 15,
							Utils.getpreference(getActivity(),
									MyApplication.Token));
					msg.what = MyApplication.READ_SUCCESS;
//					System.out.println("list= "+list.get(0).avg_speed);
				}catch(Exception e){
					System.out.println("=========resultresult==============="+e.toString());
					msg.what = MyApplication.READ_FAIL;
				}

				addhandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler addhandler = new Handler() {
		public void handleMessage(Message msg) {
			isReading = true;
//			listView.onRefreshComplete();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;

			case MyApplication.READ_SUCCESS:
				drawCurve(list);
				break;

			default:
				break;
			}

		};
	};

}
