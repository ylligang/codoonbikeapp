package com.inwhoop.codoon.fragment;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.activity.SlidingActivity;
import com.inwhoop.codoon.app.MyApplication;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * 绑定成功
 * 
 * @Project: CodoonBikeApp
 * @Title: BindSucessFragment.java
 * @Package com.inwhoop.codoon.fragment
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-5-11 下午9:09:06
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class BindSucessFragment extends BaseFragment implements OnClickListener {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.bind_sucess, null);
		init(view);
		setLeftBtBack(R.drawable.close, true);
		setTitleText(R.string.bind_sucess);
		Button setButton = (Button) view.findViewById(R.id.set);
		setButton.setOnClickListener(this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getLeftButton().setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		FragmentTransaction ft = getActivity().getSupportFragmentManager()
				.beginTransaction();
		switch (v.getId()) {
		case R.id.left:
			// MainFragment mainFragment = new MainFragment();
			// Bundle bundle=new Bundle();
			// bundle.putString("fag", "");
			// mainFragment.setArguments(bundle);
			// // bundle.putInt(MyApplication.CONECT_TAG, MyApplication.CONECT);
			// // mainFragment.setArguments(bundle);
			// ft.replace(R.id.content_frame, mainFragment, "mainFragment");
			//
			// ft.addToBackStack("mainFragment");
			// ft.commitAllowingStateLoss();

			if (null != getActivity()) {
				// weightEditText.setText("");
				MyApplication.flag = true;
				ft.remove(BindSucessFragment.this);
				if (null != (((SlidingActivity) getActivity()).menuFragment.mainFragment)) {
					// ft.hide((((SlidingActivity)getActivity()).menuFragment.mainFragment));
					// (((SlidingActivity)getActivity()).menuFragment.mainFragment).setArguments(bundle);
					ft.show((((SlidingActivity) getActivity()).menuFragment.mainFragment));
				} else {
					Bundle bundle = new Bundle();
					bundle.putString("fag", "");
					(((SlidingActivity) getActivity()).menuFragment.mainFragment) = new MainFragment();
					(((SlidingActivity) getActivity()).menuFragment.mainFragment)
							.setArguments(bundle);
					ft.add(R.id.content_frame,
							(((SlidingActivity) getActivity()).menuFragment.mainFragment));
				}
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commitAllowingStateLoss();
				// Bundle bundle = new Bundle();
				// bundle.putInt(MyApplication.CONECT_TAG,
				// MyApplication.CONECT);
				// mainFragment.setArguments(bundle);
			}

			break;

		case R.id.set:
			// SetFragment setFragment = new SetFragment();
			// ft.replace(R.id.content_frame, setFragment, "setFragment");
			// ft.addToBackStack("setFragment");
			// ft.commitAllowingStateLoss();
			if (null != getActivity()) {
				// weightEditText.setText("");
				ft.remove(BindSucessFragment.this);
				if (null != (((SlidingActivity) getActivity()).menuFragment.setFragment)) {
					// ft.hide((((SlidingActivity)getActivity()).menuFragment.mainFragment));
					// (((SlidingActivity)getActivity()).menuFragment.mainFragment).setArguments(bundle);
					ft.remove((((SlidingActivity) getActivity()).menuFragment.setFragment));
					(((SlidingActivity) getActivity()).menuFragment.setFragment) = null;
				}
				(((SlidingActivity) getActivity()).menuFragment.setFragment) = new SetFragment();
				ft.add(R.id.content_frame,
						(((SlidingActivity) getActivity()).menuFragment.setFragment));
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commitAllowingStateLoss();
				// Bundle bundle = new Bundle();
				// bundle.putInt(MyApplication.CONECT_TAG,
				// MyApplication.CONECT);
				// mainFragment.setArguments(bundle);
			}
			break;
		}

	}
}
