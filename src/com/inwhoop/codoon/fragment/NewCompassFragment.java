package com.inwhoop.codoon.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.activity.SlidingActivity;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.entity.EquipmentInfoBean;
import com.inwhoop.codoon.entity.RidingDataBean;
import com.inwhoop.codoon.entity.RidingEntity;
import com.inwhoop.codoon.service.BlueService;
import com.inwhoop.codoon.service.CountService;
import com.inwhoop.codoon.service.PlayService;
import com.inwhoop.codoon.service.SpeedService;
import com.inwhoop.codoon.service.TimerUploadService;
import com.inwhoop.codoon.utils.JsonUtils;
import com.inwhoop.codoon.utils.Utils;
import com.inwhoop.codoon.view.CompassView;
import com.inwhoop.codoon.view.RotateAnimation;
import com.inwhoop.codoon.view.RotateAnimation.InterpolatedTimeListener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 指南针、地图、历史
 * 
 * @Project: CodoonBikeApp
 * @Title: CompassFragment.java
 * @Package com.inwhoop.codoon.fragment
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-5-6 下午6:35:15
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class NewCompassFragment extends BaseFragment implements
		InterpolatedTimeListener, OnClickListener {
	private final float MAX_ROATE_DEGREE = 1.0f;
	private SensorManager mSensorManager;
	private Sensor mOrientationSensor;
	private LocationManager mLocationManager;
	private float mDirection;
	private float mTargetDirection;
	private AccelerateInterpolator mInterpolator;
	protected final Handler mHandler = new Handler();
	private boolean mStopDrawing;
	private CompassView mPointer;
	private RelativeLayout speed_layout = null;
	private static TextView speedView;
	private static TextView kmView;
	private static TextView changeView;
	private static TextView speed_text = null;
	private static TextView timeTextView;
	private TextView electriView;
	private ImageView electriImageView;
	private LinearLayout tabLayout = null;
	private RelativeLayout titleLayout = null;
	private boolean isbool = true;
	private int speed;
	private int taPing;
	private float allSpeed;
	private float allTaPing;
	private int circleNum;
	private int maxSpeed = 0;
	private int maxTaPin = 0;
	private int battery_num;
	/** 启动点时间 **/
	private long startTime = 0;
	private long stoptTime = 0;
	/** 时间偏差值 **/
	private long differTime = 0;
	/** 目前我的状态 **/
	private int mState = 0;
	/** 重置归零状态 **/
	private final int TIME_RESET = 0;
	/** 启动状态 **/
	private final int TIME_START = 1;
	/** 暂停状态 **/
	private final int TIME_STOP = 3;
	public boolean isTouch = false;
	private boolean isTime = false;
	private boolean isStopCount = false;
	private List<RidingDataBean> list = new ArrayList<RidingDataBean>();
	private RidingEntity entity;
	public Intent intent;
	private Intent intentIpload;
	private MediaPlayer mMediaPlayer;
	/* 定义要播放的文件夹路径 */
	private final String MP3_PATH = "android.resource://com.inwhoop.codoon/";
	private int s = 0;
	private int m = 0;
	private int h = 0;
	private float avg_speed;
	private float avg_pedal_rate;
	private int speed_count = 0;
	private int tp_count = 0;
	private int count = 0;
	private long startLong = 0;
	private long endLong = 0;
	private float c_distance;
	// private List<Float> spList = new ArrayList<Float>();
	// private List<Float> tpList = new ArrayList<Float>();
	private UPTimeShowTask upShowTask;
	private String isBind;
	protected Runnable mCompassViewUpdater = new Runnable() {
		@Override
		public void run() {
			if (mPointer != null && !mStopDrawing) {
				if (mDirection != mTargetDirection) {

					// calculate the short routine
					float to = mTargetDirection;
					if (to - mDirection > 180) {
						to -= 360;
					} else if (to - mDirection < -180) {
						to += 360;
					}

					// limit the max speed to MAX_ROTATE_DEGREE
					float distance = to - mDirection;
					if (Math.abs(distance) > MAX_ROATE_DEGREE) {
						distance = distance > 0 ? MAX_ROATE_DEGREE
								: (-1.0f * MAX_ROATE_DEGREE);
					}

					// need to slow down if the distance is short
					mDirection = normalizeDegree(mDirection
							+ ((to - mDirection) * mInterpolator
									.getInterpolation(Math.abs(distance) > MAX_ROATE_DEGREE ? 0.4f
											: 0.3f)));
					mPointer.updateDirection(mDirection);
				}

				mHandler.postDelayed(mCompassViewUpdater, 20);
			}
		}
	};

	/**
	 * 设置电池显示
	 * 
	 * @Title: setElectri
	 * @Description: TODO void
	 */
	public void setElectri() {
		String isConect = "";
		if (null != getActivity()) {
			isConect = Utils.getpreference(getActivity(), MyApplication.BIND);
		}
		if (!isConect.equals(MyApplication.SUCESS)) {
			electriView.setText("未连接");
			electriView.setVisibility(View.VISIBLE);
			electriImageView.setVisibility(View.GONE);
		} else {
			electriView.setVisibility(View.INVISIBLE);
			electriImageView.setVisibility(View.VISIBLE);
			battery_num = Utils.getIntpreference(getActivity(),
					MyApplication.BATTERY, 80);
			if (battery_num == 0) {
				electriImageView.setImageResource(R.drawable.e_0);
			} else if (battery_num == 10) {
				electriImageView.setImageResource(R.drawable.e_10);
			} else if (battery_num == 20) {
				electriImageView.setImageResource(R.drawable.e_20);
			} else if (battery_num == 40) {
				electriImageView.setImageResource(R.drawable.e_40);
			} else if (battery_num == 60) {
				electriImageView.setImageResource(R.drawable.e_60);
			} else if (battery_num == 80) {
				electriImageView.setImageResource(R.drawable.e_80);
			} else if (battery_num == 100) {
				electriImageView.setImageResource(R.drawable.e_100);
			}
		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.compass_frag, null);
		initResources(view);
		initServices();
		boolean isRun = Utils.isServiceWorked(getActivity(),
				"com.inwhoop.codoon.service.BlueService");
		if (!isRun) {
			intent = new Intent(getActivity(), BlueService.class);
			getActivity().startService(intent);
		}
		return view;
	}

	private void initResources(View view) {
		isTime = Utils.getBooleanStartpreference(getActivity(),
				MyApplication.START_TIME);
		mDirection = 0.0f;
		mTargetDirection = 0.0f;
		mInterpolator = new AccelerateInterpolator();
		mStopDrawing = true;
		mPointer = (CompassView) view.findViewById(R.id.compass_pointer);
		speed_layout = (RelativeLayout) view.findViewById(R.id.speed_layout);
		speed_text = (TextView) view.findViewById(R.id.speed_text);
		speedView = (TextView) view.findViewById(R.id.speed_view);
		kmView = (TextView) view.findViewById(R.id.km_text);
		changeView = (TextView) view.findViewById(R.id.change_text);
		timeTextView = (TextView) view.findViewById(R.id.time);
		tabLayout = (LinearLayout) getActivity().findViewById(R.id.tab_layout);
		titleLayout = (RelativeLayout) getActivity().findViewById(
				R.id.title_bar);
		electriView = (TextView) view.findViewById(R.id.electri);
		electriImageView = (ImageView) view.findViewById(R.id.electri_img);
		speed_layout.setOnClickListener(this);

	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
		if (!hidden) {
			isTime = Utils.getBooleanStartpreference(getActivity(),
					MyApplication.START_TIME);
			isBind = Utils.getpreference(getActivity(), MyApplication.BIND);
			setElectri();
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		if (null == ((SlidingActivity) getActivity()).countIntent) {
			((SlidingActivity) getActivity()).countIntent = new Intent(
					getActivity(), CountService.class);
		}
		if (null == ((SlidingActivity) getActivity()).speedIntent) {
			((SlidingActivity) getActivity()).speedIntent = new Intent(
					getActivity(), SpeedService.class);
			getActivity().startService(
					((SlidingActivity) getActivity()).speedIntent);
			IntentFilter filter = new IntentFilter();// 创建IntentFilter对象
			filter.addAction(MyApplication.ACTION_COUNT);
			filter.addAction(MyApplication.ACTION_SPEED);
			getActivity().registerReceiver(mRecever, filter);
			isStopCount = Utils.getBooleanStoppreference(getActivity(),
					MyApplication.STOP_TIME);
		}

		// count=Utils.getIntpreferenceData(getActivity(), MyApplication.COUNT,
		// 0);
		// new Thread(new Runnable() {
		//
		// @Override
		// public void run() {
		// while (isbool) {
		// try {
		// speed = Utils.getIntpreference(getActivity(),
		// MyApplication.SPEED, 0);
		// System.out.println("speed= " + speed);
		// taPing = Utils.getIntpreference(getActivity(),
		// MyApplication.CARDEN, 0);
		// circleNum += Utils.getIntpreference(getActivity(),
		// MyApplication.CIRCLE, 0);
		// getMaxSpeed(speed);
		// getMaxTaPin(taPing);
		// isBind = Utils.getpreference(getActivity(),
		// MyApplication.BIND);
		// Thread.sleep(1000);
		// handler.sendEmptyMessage(2);
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
		//
		// }
		// }).start();
	}

	// private Thread thread = new Thread(new Runnable() {
	//
	// @Override
	// public void run() {
	// while (isbool) {
	// try {
	// speed = Utils.getIntpreference(getActivity(),
	// MyApplication.SPEED, 0);
	// taPing = Utils.getIntpreference(getActivity(),
	// MyApplication.CARDEN, 0);
	// circleNum += Utils.getIntpreference(getActivity(),
	// MyApplication.CIRCLE, 0);
	// getMaxSpeed(speed);
	// getMaxTaPin(taPing);
	// isBind = Utils.getpreference(getActivity(),
	// MyApplication.BIND);
	// Thread.sleep(1000);
	// handler.sendEmptyMessage(2);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// }
	// });

	private BroadcastReceiver mRecever = new BroadcastReceiver() {
		@Override
		public void onReceive(Context arg0, Intent arg1) {
			if (arg1.getAction().equals(MyApplication.ACTION_SPEED)) {
				speed = MyApplication.speed;
				taPing = MyApplication.tp;
				getMaxSpeed(MyApplication.speed);
				getMaxTaPin(MyApplication.tp);
				isBind = Utils.getpreference(getActivity(), MyApplication.BIND);
				handler.sendEmptyMessage(2);

			} else if (arg1.getAction().equals(MyApplication.ACTION_COUNT)) {
				mState = TIME_RESET;
				MyApplication.mState = TIME_RESET;
				isTime = false;
				if (null != getActivity()) {
					Utils.saveBooleanStartPreference(getActivity(),
							MyApplication.START_TIME, isTime);
				}
				// speed_count = 0;
				endLong = getLatestTimeMillis();
				// tp_count = 0;
				// System.out.println("onReceive=============");
				saveData();
				timeTextView.setText("00:00:00");
				// upShowTask.cancel(true);
				if (null != getActivity()) {
					boolean isplay = Utils.getBooleanStoppreference(
							getActivity(), MyApplication.PLAY);
					if (Utils.getIntpreferenceset(getActivity(),
							MyApplication.YUYIN, 0) > 0) {
						if (!isplay) {
							play(R.raw.qixingzanting);
							Utils.saveBooleanStopPreference(getActivity(),
									MyApplication.PLAY, true);
						}
					}
					Intent intent = new Intent();// 创建Intent对象
					intent.setAction(MyApplication.ACTION_STOP);
					getActivity().sendBroadcast(intent);// 发送广播
					if (null != ((SlidingActivity) getActivity()).intentPlay) {
						// System.out.println("intentPlay====stop ");
						getActivity().stopService(
								((SlidingActivity) getActivity()).intentPlay);
						((SlidingActivity) getActivity()).intentPlay = null;
					}
				}
			}
		}

	};

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				break;
			case 2:
				setAniText();
				// speed_text.setText(speed + "");
				// System.out.println("isTime=ccc== " + isTime);
				if (!isBind.equals(MyApplication.SUCESS)) {
					mState = TIME_RESET;
					isTime = false;
					if (null != getActivity()) {
						Utils.saveBooleanStartPreference(getActivity(),
								MyApplication.START_TIME, isTime);
					}
					speed_count = 0;
					tp_count = 0;
					timeTextView.setText("00:00:00");
					MyApplication.mState = TIME_RESET;
					// upShowTask.cancel(true);
				} else {
					if (speed > 0 && !isTime) {
						isTime = true;
						if (null != getActivity()) {
							Utils.saveBooleanStartPreference(getActivity(),
									MyApplication.START_TIME, true);
						}
						count = 0;
						speed_count = 0;
						tp_count = 0;
						startTime();
					}
					if (speed == 0) {
						if (taPing > 0 && !isTime) {
							isTime = true;
							if (null != getActivity()) {
								Utils.saveBooleanStartPreference(getActivity(),
										MyApplication.START_TIME, true);
							}
							count = 0;
							startTime();
						}
					}
					if (speed > 0) {
						avg_speed += speed;
						++speed_count;
						count = 0;
					}
					if (taPing > 0) {
						avg_pedal_rate += taPing;
						++tp_count;
						count = 0;
					}
					stopTime();
				}
				break;
			}

		}
	};

	/**
	 * 设置旋转后的文字
	 * 
	 * @Title: setAniText
	 * @Description: TODO void
	 */
	private void setAniText() {
		if (isTouch) {
			speedView.setText("踏频");
			changeView.setText("速度");
			kmView.setText("rmp");
			speed_text.setText(taPing + "");
			changeView.setTextColor(0xff1adae7);
			speed_text.setTextColor(0xff6bb52a);
		} else {
			speedView.setText("速度");
			changeView.setText("踏频");
			kmView.setText("km/h");
			speed_text.setText(speed + "");
			changeView.setTextColor(0xff6bb52a);
			speed_text.setTextColor(0xff1adae7);
		}
	}

	private void initServices() {
		// sensor manager
		mSensorManager = (SensorManager) getActivity().getSystemService(
				Context.SENSOR_SERVICE);
		mOrientationSensor = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ORIENTATION);

		// location manager
		mLocationManager = (LocationManager) getActivity().getSystemService(
				Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		criteria.setCostAllowed(true);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		// mLocationProvider = mLocationManager.getBestProvider(criteria, true);

	}

	private void getMaxSpeed(int speed) {
		if (maxSpeed < speed) {
			maxSpeed = speed;
		}
	}

	private void getMaxTaPin(int tapin) {
		if (maxTaPin < tapin) {
			maxTaPin = tapin;
		}
	}

	private void saveData() {
		if (null != getActivity()) {
			String deviceId = Utils.getpreference(getActivity(),
					MyApplication.DEV_ID);
			String version = Utils.getpreference(getActivity(),
					MyApplication.VERSION);
			float min = ((endLong - startLong - 3 * 60 * 1000) / (float) (1000 * 60));

			int perimeter = Utils.getIntpreference(getActivity(),
					MyApplication.PERIMETER, 112);
			EquipmentInfoBean info = new EquipmentInfoBean(deviceId, version,
					MyApplication.APP_VERSION);
			entity = new RidingEntity();
			list.clear();
			list.add(getData(min, perimeter));
			entity.setInfo(info);
			entity.setData(list);
			int datacount = Utils.getIntpreferenceData(getActivity(),
					"datacount", 0);
			Utils.savePreferenceData(getActivity(), "data" + datacount,
					JsonUtils.entityToJson(entity));
			datacount++;
			Utils.saveIntPreferenceData(getActivity(), "datacount", datacount);

			datacount = Utils.getIntpreferenceData(getActivity(), "datacount",
					0);

		}
	}

	private RidingDataBean getData(float min, float perimeter) {
		String startTime = Utils.getStrTime(startLong);
		String endTime = Utils.getStrTime(endLong - 1000 * 60 * 3);
		RidingDataBean riBean = new RidingDataBean();

		riBean.setStart_time(startTime);
		riBean.setEnd_time(endTime);
		riBean.setDistance(MyApplication.c_distance);
		riBean.setCalories(MyApplication.Kc);
		riBean.setAvg_speed(MyApplication.a_sp);
		riBean.setAvg_pedal_rate(MyApplication.a_tp);
		// if (speed_count > 0) {
		// riBean.setAvg_speed((float) (Math
		// .round((avg_speed / speed_count) * 100) / (float) 100));
		// }
		// if (tp_count > 0) {
		// riBean.setAvg_pedal_rate((float) (Math
		// .round((avg_pedal_rate / tp_count) * 100) / (float) 100));
		// }
		riBean.setMax_speed(MyApplication.max_sp);
		riBean.setMax_pedal_rate(MyApplication.max_tp);
		return riBean;
	}

	/**
	 * 功能描述：获取系统时间毫秒差 方法描述：
	 * 
	 * @return
	 */
	private long getLatestTimeMillis() {
		return System.currentTimeMillis();
	}

	private SensorEventListener mOrientationSensorEventListener = new SensorEventListener() {

		@Override
		public void onSensorChanged(SensorEvent event) {
			float direction = event.values[0] * -1.0f;
			mTargetDirection = normalizeDegree(direction);
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};

	private float normalizeDegree(float degree) {
		return (degree + 720) % 360;

	}

	@Override
	public void onResume() {
		super.onResume();
		if (mOrientationSensor != null) {
			mSensorManager.registerListener(mOrientationSensorEventListener,
					mOrientationSensor, SensorManager.SENSOR_DELAY_GAME);
		}
		mStopDrawing = false;
		mHandler.postDelayed(mCompassViewUpdater, 20);
//		setElectri();

	}

	@Override
	public void onPause() {
		super.onPause();
		mStopDrawing = true;
		isbool = false;
		if (mOrientationSensor != null) {
			mSensorManager.unregisterListener(mOrientationSensorEventListener);
		}
		// if (null != getActivity()) {
		// System.out.println("count=====gg====="+count);
		// Utils.saveIntPreferenceData(getActivity(), MyApplication.COUNT,
		// count);
		// }

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.speed_layout:
			isTouch = !isTouch;
			rotate();
			break;
		}

	}

	/**
	 * 动画
	 * 
	 * @Title: rotate
	 * @Description: TODO void
	 */
	private void rotate() {
		RotateAnimation rotateAnim = null;
		float cX = speed_layout.getWidth() / 2.0f;
		float cY = speed_layout.getHeight() / 2.0f;
		rotateAnim = new RotateAnimation(cX, cY,
				RotateAnimation.ROTATE_INCREASE);
		if (rotateAnim != null) {
			rotateAnim.setInterpolatedTimeListener(this);
			rotateAnim.setFillAfter(true);
			speed_layout.startAnimation(rotateAnim);
		}

	}

	@Override
	public void interpolatedTime(float interpolatedTime) {
		// 监听到翻转进度过半时，更新speed_text显示内容。
		if (interpolatedTime > 0.5f) {
			// if (isTouch) {
			// speedView.setText("踏频");
			// changeView.setText("速度");
			// kmView.setText("rmp");
			// speed_text.setText(taPing + "");
			// changeView.setTextColor(0xff1adae7);
			// speed_text.setTextColor(0xff6bb52a);
			// } else {
			// speedView.setText("速度");
			// changeView.setText("踏频");
			// kmView.setText("km/h");
			// speed_text.setText(speed + "");
			// changeView.setTextColor(0xff6bb52a);
			// speed_text.setTextColor(0xff1adae7);
			// }

		}

	}

	private void startTime() {
		if (isBind.equals(MyApplication.SUCESS)) {
			if (null != getActivity()) {
				Utils.saveBooleanStopPreference(getActivity(),
						MyApplication.PLAY, false);
				if (Utils.getIntpreferenceset(getActivity(),
						MyApplication.YUYIN, 0) > 0) {
					play(R.raw.qixingkaishi);
				}
				// Intent intent = new Intent(getActivity(), Upload.class);
				// getActivity().startService(intent);
				if (null == ((SlidingActivity) getActivity()).timerIntent) {
					((SlidingActivity) getActivity()).timerIntent = new Intent(
							getActivity(), TimerUploadService.class);
					getActivity().startService(
							((SlidingActivity) getActivity()).timerIntent);

				}
				// boolean isRun = Utils.isServiceWorked(getActivity(),
				// "com.inwhoop.codoon.service.PlayService");
				// // System.out.println("PlayService===="+isRun);
				// if (!isRun) {
				// // System.out.println("PlayService====");
				// Intent intentPlay = new Intent(getActivity(),
				// PlayService.class);
				// getActivity().startService(intentPlay);
				// }

			}
			// 获取起始点时间
			startTime = getLatestTimeMillis();
			startLong = getLatestTimeMillis();
			// 将我的状态设为启动状态
			mState = TIME_START;
			MyApplication.mState = TIME_START;
			s = 0;
			m = 0;
			h = 0;
			// timeTextView.setText(getTime());
			// 启动异步线程
			upShowTask = new UPTimeShowTask();
			upShowTask.execute();

			boolean isRun = Utils.isServiceWorked(getActivity(),
					"com.inwhoop.codoon.service.PlayService");
			// System.out.println("intentPlay===="+isRun);
			if (!isRun) {
				((SlidingActivity) getActivity()).intentPlay = new Intent(
						getActivity(), PlayService.class);
				getActivity().startService(
						((SlidingActivity) getActivity()).intentPlay);
			}
		}
	}

	private void stopTime() {
		if (speed == 0 && taPing == 0 && MyApplication.mState == TIME_START) {
			if (null != getActivity()) {
				boolean isStop = Utils.isServiceWorked(getActivity(),
						"com.inwhoop.codoon.service.CountService");
				if (null != ((SlidingActivity) getActivity()).countIntent) {
					if (!isStop & !isStopCount) {
						getActivity().startService(
								((SlidingActivity) getActivity()).countIntent);
						isStopCount = true;
						Utils.saveBooleanStopPreference(getActivity(),
								MyApplication.STOP_TIME, true);
					}
				}
			}
			// ++count;
			// if (count == (3 * 60)) {
			// mState = TIME_RESET;
			// MyApplication.mState = TIME_RESET;
			// isTime = false;
			// if (null != getActivity()) {
			// Utils.saveBooleanStartPreference(getActivity(),
			// MyApplication.START_TIME, isTime);
			// }
			// // speed_count = 0;
			// endLong = getLatestTimeMillis();
			// // tp_count = 0;
			// saveData();
			// timeTextView.setText("00:00:00");
			// // upShowTask.cancel(true);
			// if (null != getActivity()) {
			// if (Utils.getIntpreferenceset(getActivity(),
			// MyApplication.YUYIN, 0) > 0) {
			// play(R.raw.qixingzanting);
			// }
			// Intent intent = new Intent();// 创建Intent对象
			// intent.setAction(MyApplication.ACTION_STOP);
			// getActivity().sendBroadcast(intent);// 发送广播
			// }
			// }
			// stoptTime = getLatestTimeMillis();
		}
	}

	private class UPTimeShowTask extends AsyncTask<Void, Integer, Void> {
		private String lastestTime;

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			while (MyApplication.mState != TIME_RESET) {
				publishProgress(MyApplication.mState);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// 重置时候，通知onProgressUpdate修改界面
			publishProgress(MyApplication.mState);
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			switch (values[0]) {
			case TIME_START:
				// lastestTime = getLatestTime(getLatestTimeMillis() -
				// startTime);
				timeTextView.setText(getTime());

				break;
			case TIME_RESET:
				timeTextView.setText("00:00:00");
				break;
			}
		}
	}

	/**
	 * 功能描述：获取最新的时间 方法描述：
	 * 
	 * @param timeDiffer
	 *            启动时间和目前时间的时间差
	 * @return 已经启动多久了
	 */
	private String getLatestTime(long timeDiffer) {
		return ((new SimpleDateFormat(this.getResources().getString(
				R.string.time_data_format))).format(timeDiffer));
	}

	private String getTime() {
		// String time = "00:00:00";
		String second = "00";
		String min = "00";
		String hour = "00";
		if (s < 59) {
			s++;
		} else {
			s = 0;
			m++;
			if (m > 59) {
				m = 0;
				h++;
			}
		}
		if (h == 24) {
			h = 0;
		}
		if (s < 10) {
			second = "0" + s;
		} else {
			second = "" + s;
		}
		if (m < 10) {
			min = "0" + m;
		} else {
			min = "" + m;
		}

		if (h < 10) {
			hour = "0" + h;
		} else {
			hour = "" + h;
		}
		Utils.savePreference(getActivity(), MyApplication.HOUR, hour);
		Utils.savePreference(getActivity(), MyApplication.MIN, min);
		return hour + ":" + min + ":" + second;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		isbool = false;
		if (null != getActivity()) {
			if (null != intent)
				getActivity().stopService(intent);
			if (null != mRecever) {
				try {
					getActivity().unregisterReceiver(mRecever);
				} catch (Exception e) {
				}
			}
		}

	}

	private void play(int id) {
		try {
			mMediaPlayer = new MediaPlayer();
			/* 重置多媒体 */
			mMediaPlayer.reset();
			/* 读取mp3文件 */
			Uri uri = Uri.parse(MP3_PATH + id);

			mMediaPlayer.setDataSource(getActivity(), uri);
			/* 准备播放 */
			mMediaPlayer.prepare();
			/* 开始播放 */
			mMediaPlayer.start();
			mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					mMediaPlayer.stop();// 停止播放
					mMediaPlayer.release();// 释放资源
					mMediaPlayer = null;

				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
