package com.inwhoop.codoon.fragment;

import java.util.ArrayList;

import com.communication.ble.DisconveryManager;
import com.inwhoop.codoon.R;
import com.inwhoop.codoon.activity.SlidingActivity;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.utils.BlueToothUtils;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * 绑定码表
 * 
 * @Project: CodoonBikeApp
 * @Title: StartBindFragment.java
 * @Package com.inwhoop.codoon.fragment
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-5-10 下午12:53:56
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class StartBindFragment extends BaseFragment implements OnClickListener {
	private String TAG = "StartBindFragment";
	// private DisconveryManager mDisconveryManager;
	// private final int GET_NEW_DEVICE = 1;
	private boolean isOpen = true;
	private boolean isToast = false;
	private BluetoothAdapter blueAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.start_binding, null);
		init(view);
		setLeftBtBack(R.drawable.close, true);
		setTitleText(R.string.app_name);
		getLeftButton().setOnClickListener(this);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		searchBlue();

	}

	/**
	 * 搜索蓝牙
	 * 
	 * @Title: searchBlue
	 * @Description: TODO void
	 */
	private void searchBlue() {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
			Toast.makeText(getActivity(), "系统版本太低，不支持设备蓝牙!", Toast.LENGTH_SHORT)
					.show();
			return;
		}
		isToast = false;
		blueAdapter = BluetoothAdapter.getDefaultAdapter();
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isOpen) {
					Message msg = blueHandler.obtainMessage();
					try {
						boolean isenable = blueAdapter.isEnabled();
						Thread.sleep(1000);
						if (isenable) {
							isOpen = false;
							msg.what = MyApplication.READ_SUCCESS;
						} else {
							msg.what = MyApplication.READ_FAIL;
						}
					} catch (InterruptedException e) {
						msg.what = MyApplication.READ_FAIL;
					}
					blueHandler.sendMessage(msg);
				}
			}
		}).start();
	}

	private Handler blueHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MyApplication.READ_SUCCESS:
				new BlueToothUtils(mHandler).scanDevice(mContext);
				// scanDevice();
				break;

			case MyApplication.READ_FAIL:
				if (null != getActivity()) {
					if (!isToast) {
						Toast.makeText(getActivity(), "蓝牙未打开，正在打开蓝牙",
								Toast.LENGTH_SHORT).show();
						isToast = true;
						blueAdapter.enable();
					}
				}
				break;
			}

		};
	};

	// private void scanDevice() {
	// BluetoothAdapter.LeScanCallback scanCallback = new
	// BluetoothAdapter.LeScanCallback() {
	//
	// @Override
	// public void onLeScan(BluetoothDevice device, int rssi,
	// byte[] scanRecord) {
	// // TODO Auto-generated method stub
	// if (null != device && device.getName().equals("COD_MB")) {
	// Message message = mHandler.obtainMessage();
	// message.obj = device;
	// message.what = 1;
	// mHandler.sendMessage(message);
	// }
	//
	// }
	//
	// };
	//
	// if (null == mDisconveryManager) {
	// mDisconveryManager = new DisconveryManager(mContext, scanCallback);
	// } else {
	//
	// mDisconveryManager.startSearch();
	// }
	//
	// }

	// private Handler mHandler = new Handler(new Handler.Callback() {
	//
	// @Override
	// public boolean handleMessage(Message message) {
	// return true;
	// }
	//
	// });

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {

			if (BlueToothUtils.GET_NEW_DEVICE == msg.what) {
				if (null != getActivity()) {
					ConfirmBindFragment cBindFragment = new ConfirmBindFragment();
					Bundle bundle = new Bundle();
					bundle.putParcelable("device", (Parcelable) msg.obj);
					cBindFragment.setArguments(bundle);
					FragmentTransaction ft = getActivity()
							.getSupportFragmentManager().beginTransaction();
					ft.remove(StartBindFragment.this);
					ft.add(R.id.content_frame, cBindFragment);
					ft.commitAllowingStateLoss();
				}
				BlueToothUtils.mDisconveryManager.stopSearch();

			}
		};
	};

	@Override
	public void onDestroy() {
		if (null != BlueToothUtils.mDisconveryManager) {
			BlueToothUtils.mDisconveryManager.stopSearch();
		}
		super.onDestroy();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
	}

	@Override
	public void onClick(View v) {
		// MainFragment mainFragment = new MainFragment();
		// // Bundle bundle = new Bundle();
		// // bundle.putInt(MyApplication.CONECT_TAG, MyApplication.NO_CONECT);
		// // mainFragment.setArguments(bundle);
		// Bundle bundle=new Bundle();
		// bundle.putString("fag", "");
		// mainFragment.setArguments(bundle);
		// FragmentTransaction ft = getActivity().getSupportFragmentManager()
		// .beginTransaction();
		// ft.replace(R.id.content_frame, mainFragment, "mainFragment");
		// ft.addToBackStack("mainFragment");
		// ft.commitAllowingStateLoss();
		if (null != getActivity()) {
			// weightEditText.setText("");

			FragmentTransaction ft = getActivity().getSupportFragmentManager()
					.beginTransaction();
			ft.hide(StartBindFragment.this);
			if (null != (((SlidingActivity) getActivity()).menuFragment.mainFragment)) {
				// ft.hide((((SlidingActivity)getActivity()).menuFragment.mainFragment));
				// (((SlidingActivity)getActivity()).menuFragment.mainFragment).setArguments(bundle);
				ft.show((((SlidingActivity) getActivity()).menuFragment.mainFragment));
			} else {
				Bundle bundle = new Bundle();
				bundle.putString("fag", "");
				(((SlidingActivity) getActivity()).menuFragment.mainFragment) = new MainFragment();
				(((SlidingActivity) getActivity()).menuFragment.mainFragment)
						.setArguments(bundle);
				ft.add(R.id.content_frame,
						(((SlidingActivity) getActivity()).menuFragment.mainFragment));
			}
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.commitAllowingStateLoss();
			// Bundle bundle = new Bundle();
			// bundle.putInt(MyApplication.CONECT_TAG,
			// MyApplication.CONECT);
			// mainFragment.setArguments(bundle);
		}
		isOpen = false;

	}
}
