package com.inwhoop.codoon.fragment;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import org.json.JSONArray;

import com.communication.ble.BleSyncManager;
import com.communication.data.CODMBCommandData;
import com.communication.data.ISyncDataCallback;
import com.inwhoop.codoon.R;
import com.inwhoop.codoon.activity.SlidingActivity;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.service.BlueRealTimeService;
import com.inwhoop.codoon.utils.BlueToothUtils;
import com.inwhoop.codoon.utils.Utils;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * 确认绑定码表界面
 * 
 * @Project: CodoonBikeApp
 * @Title: ConfirmBindFragment.java
 * @Package com.inwhoop.codoon.fragment
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-5-10 下午1:44:28
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ConfirmBindFragment extends BaseFragment implements
		OnClickListener {

	// private BluetoothDevice mBlueDevice;
	// private BleSyncManager mSyncDeviceDataManager;
	private BluetoothDevice mBlueDevice;

	// public static BleSyncManager mSyncDeviceDataManager;
	private String TAG = "ConfirmBindFragment";
	private final int REQUEST_SCAN = 0;
	private final int CHANGE_COMMAND_STATE = 1;
	private final int COMMAND_RESULT = 2;
	private final int GET_REAL_TIME = 3;
	private Button confirmButton;
	private boolean isbool = true;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.confirm_bind, null);
		confirmButton = (Button) view.findViewById(R.id.confirm);
		init(view);
		setLeftBtBack(R.drawable.close, true);
		setTitleText(R.string.confirm_bind);
		confirmButton.setOnClickListener(this);
		getLeftButton().setOnClickListener(this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		Bundle bundle = getArguments();
		mBlueDevice = bundle.getParcelable("device");

	}

	// private void initSyncDeviceDataManager() {
	// ISyncDataCallback syncDataCallback = new ISyncDataCallback() {
	//
	// @Override
	// public void onNullData() {
	// // System.out.println("onNullData");
	// // Log.d(TAG, "onNullData()");
	// }
	//
	// @Override
	// public void onConnectSuccessed() {
	// // TODO Auto-generated method stub
	// // Log.d(TAG, "onConnectSuccessed()");
	// // // send some command after connectSuccessed, such as
	// // getVersion
	// mHandler.sendEmptyMessage(CHANGE_COMMAND_STATE);
	//
	// }
	//
	// @Override
	// public void onGetVersion(String version) {
	// // System.out.println("1onConnectSuccessed");
	// // Log.d(TAG, "onGetVersion():" + version);
	// showCommandResult("device version:" + version);
	// if (null != getActivity())
	// Utils.savePreference(getActivity(), MyApplication.VERSION,
	// version);
	// }
	//
	// @Override
	// public void onGetDeviceID(String deviceID) {
	// // System.out.println("2onConnectSuccessed");
	// // // TODO Auto-generated method stub
	// // Log.d(TAG, "onGetDeviceID():" + deviceID);
	// showCommandResult("deviceId:" + deviceID);
	// MyApplication.mSyncDeviceDataManager.SendDataToDevice(CODMBCommandData
	// .getPostDeviceTypeVersion());
	// if (null != getActivity())
	// Utils.savePreference(getActivity(), MyApplication.DEV_ID,
	// deviceID);
	//
	// }
	//
	// @Override
	// public void onUpdateTimeSuccessed() {
	// // TODO Auto-generated method stub
	// // System.out.println("3onConnectSuccessed");
	// // Log.d(TAG, "onUpdateTimeSuccessed()");
	// showCommandResult("onUpdateTimeSuccessed");
	// }
	//
	// @Override
	// public void onUpdateAlarmReminderSuccessed() {
	// // TODO Auto-generated method stub
	// // System.out.println("4onConnectSuccessed");
	// // Log.d(TAG, "onUpdateAlarmReminderSuccessed()");
	// showCommandResult("onUpdateAlarmReminderSuccessed");
	// }
	//
	// @Override
	// public void onBattery(int battery) {
	// // TODO Auto-generated method stub
	// // System.out.println("5onConnectSuccessed");
	// // Log.d(TAG, "onBattery():" + battery);
	// showCommandResult("battery:" + battery + "");
	// }
	//
	// @Override
	// public void onClearDataSuccessed() {
	// // System.out.println("6onConnectSuccessed");
	// // TODO Auto-generated method stub
	// // Log.d(TAG, "onClearDataSuccessed()");
	// showCommandResult("onClearDataSuccessed");
	// }
	//
	// @Override
	// public void onGetDeviceTime(String time) {
	// // TODO Auto-generated method stub
	// // System.out.println("7onConnectSuccessed");
	// // Log.d(TAG, "onGetDeviceTime():" + time);
	// showCommandResult("device time:" + time);
	// }
	//
	// @Override
	// public void onSyncDataProgress(int progress) {
	// // TODO Auto-generated method stub
	// // System.out.println("8onConnectSuccessed");
	// // Log.d(TAG, "onSyncDataProgress():" + progress);
	//
	// }
	//
	// @Override
	// public void onUpdateUserinfoSuccessed() {
	// // System.out.println("9onConnectSuccessed");
	// // TODO Auto-generated method stub
	// // Log.d(TAG, "onUpdateUserinfoSuccessed()");
	// showCommandResult("onUpdateUserinfoSuccessed");
	// }
	//
	// @Override
	// public void onGetUserInfo(int height, int weigh, int age,
	// int gender, int stepLength, int runLength, int sportType,
	// int goalValue) {
	// // TODO Auto-generated method stub
	// // System.out.println("10onConnectSuccessed");
	// // Log.d(TAG, "onGetUserInfo():goalValue=" + goalValue);
	// }
	//
	// @Override
	// public void onGetOtherDatas(ArrayList<Integer> datas) {
	// // TODO Auto-generated method stub
	// // System.out.println("11onConnectSuccessed");
	// // Log.d(TAG, "onGetOtherDatas():" + datas);
	// showCommandResult("onGetOtherDatas " + datas);
	// mHandler.sendEmptyMessageDelayed(GET_REAL_TIME, 1000);
	// }
	//
	// @Override
	// public void onTimeOut() {
	// // TODO Auto-generated method stub
	// // System.out.println("12onConnectSuccessed");
	// // Log.d(TAG, "onTimeOut()");
	// showCommandResult("onTimeOut");
	// }
	//
	// @Override
	// public void onSyncDataOver(JSONArray jsonObject,
	// ByteArrayOutputStream baos) {
	// // TODO Auto-generated method stub
	// // System.out.println("13onConnectSuccessed");
	// byte[] datas = baos.toByteArray();
	// // Log.d("syncdata", " ================baos len =" +
	// // datas.length);
	// StringBuffer sBuffer = new StringBuffer();
	// for (int i = 0; i < datas.length; i++) {
	// if ((i + 1) % 6 == 0) {
	// sBuffer.append("\n");
	// }
	// Log.d("writeData",
	// "datas i =" + Integer.toHexString(datas[i])
	// + "  byte i=" + Byte.toString(datas[i]));
	// sBuffer.append(Integer.toHexString(datas[i] & 0xFF) + "   ");
	// }
	// // showCommandResult(" dataframe json=\n" + sBuffer.toString());
	//
	// }
	//
	// @Override
	// public void onBindSucess() {
	// // TODO Auto-generated method stub
	// }
	//
	// @Override
	// public void onSyncDataOver(long[] data, ByteArrayOutputStream baos) {
	// // TODO Auto-generated method stub
	// }
	//
	// @Override
	// public void onTimeOut(BluetoothDevice device) {
	// // TODO Auto-generated method stub
	// }
	//
	// @Override
	// public void onGetDeviceRealtimeData(ArrayList<Integer> datas) {
	// // TODO Auto-generated method stub
	// int[] result = new int[3];
	// if (datas.size() > 9) {
	// result[0] = (int) ((datas.get(3) * 256 + datas.get(4)) / 10.0f);
	// result[1] = (int) ((datas.get(5) * 256 + datas.get(6)) / 10.0f);
	// result[2] = datas.get(7) * 256 + datas.get(8);
	// }
	// // showCommandResult("onGetDeviceRealtimeData speed:" +
	// // result[0]
	// // + "  踏平：" + result[1] + "  圈数：" + result[2]);
	// System.out.println("result[0]=-= "+result[0]);
	// if (null != MyApplication.context) {
	// // System.out.println("====================");
	// Utils.saveIntPreference(MyApplication.context, MyApplication.SPEED,
	// result[0]);
	// Utils.saveIntPreference(MyApplication.context,
	// MyApplication.CARDEN, result[1]);
	// Utils.saveIntPreference(MyApplication.context,
	// MyApplication.CIRCLE, result[2]);
	// Utils.savePreference(MyApplication.context, MyApplication.BIND,
	// MyApplication.SUCESS);
	// }
	//
	// }
	//
	// };
	//
	// if (null == MyApplication.mSyncDeviceDataManager) {
	// MyApplication.mSyncDeviceDataManager = new BleSyncManager(mContext,
	// syncDataCallback);
	// }
	//
	// }

	private void showCommandResult(String data) {
		Message msg = new Message();
		msg.what = COMMAND_RESULT;
		Bundle bundle = new Bundle();
		bundle.putString("message", data);
		mHandler.sendMessage(msg);
	}

	private Handler mHandler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message message) {
			switch (message.what) {
			case CHANGE_COMMAND_STATE:
				MyApplication.mSyncDeviceDataManager
						.SendDataToDevice(CODMBCommandData.getPostDeviceID());
				// getData();
				if (null != getActivity()) {
					((SlidingActivity)getActivity()).intentData = new Intent(getActivity(),
							BlueRealTimeService.class);
					getActivity().startService(((SlidingActivity)getActivity()).intentData);
				}
				BindSucessFragment bFragment = new BindSucessFragment();
				if (null != getActivity()) {
					Utils.savePreference(getActivity(), MyApplication.BIND,
							MyApplication.SUCESS);
					FragmentTransaction ft = getActivity()
							.getSupportFragmentManager().beginTransaction();
					ft.add(R.id.content_frame, bFragment);
					ft.addToBackStack("bFragment");
					ft.remove(ConfirmBindFragment.this);
					ft.commitAllowingStateLoss();
				}
				break;
			case COMMAND_RESULT:
				if (null != getActivity()) {
					// Toast.makeText(getActivity(),
					// message.getData().getString("message", ""),
					// Toast.LENGTH_SHORT).show();
				}
				break;
			default:
				break;
			}
			dismissProgressDialog();
			return true;
		}

	});

	private void getData() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isbool) {
					MyApplication.mSyncDeviceDataManager
							.SendDataToDevice(CODMBCommandData
									.getPostRealTimeData());
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}
		}).start();
	}

	@Override
	public void onDestroy() {
		isbool = false;
		// if (null != getActivity()) {
		// getActivity().stopService(intentData);
		// }
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.confirm:
			showProgressDialog("正在绑定...");
			if (null != mBlueDevice) {
				// initSyncDeviceDataManager();
				if (null != getActivity()) {
					new BlueToothUtils(mHandler).conectDevice(getActivity());
					MyApplication.mSyncDeviceDataManager.start(mBlueDevice);
				}
			} else {
				Log.e(TAG, "no bluetooth device been founded");
			}
			break;

		case R.id.left:
//			MainFragment mainFragment = new MainFragment();
//			Bundle bundle = new Bundle();
//			bundle.putString("fag", "");
//			mainFragment.setArguments(bundle);
//			FragmentTransaction ft = getActivity().getSupportFragmentManager()
//					.beginTransaction();
//			// Bundle bundle = new Bundle();
//			// bundle.putInt(MyApplication.CONECT_TAG, MyApplication.NO_CONECT);
//			// mainFragment.setArguments(bundle);
//			ft.replace(R.id.content_frame, mainFragment, "mainFragment");
//			ft.addToBackStack("mainFragment");
//			ft.commitAllowingStateLoss();
			if (null != getActivity()) {
//				weightEditText.setText("");
				
				FragmentTransaction ft = getActivity()
						.getSupportFragmentManager().beginTransaction();
				ft.remove(ConfirmBindFragment.this);
				

				if(null!=(((SlidingActivity)getActivity()).menuFragment.mainFragment)){
//					ft.hide((((SlidingActivity)getActivity()).menuFragment.mainFragment));
//					(((SlidingActivity)getActivity()).menuFragment.mainFragment).setArguments(bundle);
					ft.show((((SlidingActivity)getActivity()).menuFragment.mainFragment));
				}else{
					Bundle bundle = new Bundle();
					bundle.putString("fag", "");
					(((SlidingActivity)getActivity()).menuFragment.mainFragment) = new MainFragment();
					(((SlidingActivity)getActivity()).menuFragment.mainFragment).setArguments(bundle);
					ft.add(R.id.content_frame, (((SlidingActivity)getActivity()).menuFragment.mainFragment));
				}
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commitAllowingStateLoss();
				// Bundle bundle = new Bundle();
				// bundle.putInt(MyApplication.CONECT_TAG,
				// MyApplication.CONECT);
				// mainFragment.setArguments(bundle);
			}
			break;
		}

	}

}
