package com.inwhoop.codoon.fragment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.JSONArray;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.location.core.GeoPoint;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.LocationSource;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.Circle;
import com.amap.api.maps2d.model.CircleOptions;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.amap.api.maps2d.model.MyLocationStyle;
import com.amap.api.maps2d.model.PolylineOptions;
import com.communication.ble.BleSyncManager;
import com.communication.data.CODMBCommandData;
import com.communication.data.ISyncDataCallback;
import com.inwhoop.codoon.R;
import com.inwhoop.codoon.activity.MainActivity;
import com.inwhoop.codoon.activity.SlidingActivity;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.db.SQLOperateImpl;
import com.inwhoop.codoon.entity.BikeAllData;
import com.inwhoop.codoon.service.BlueConectService;
import com.inwhoop.codoon.service.BlueRealTimeService;
import com.inwhoop.codoon.service.MapService;
import com.inwhoop.codoon.utils.BlueToothUtils;
import com.inwhoop.codoon.utils.JsonUtils;
import com.inwhoop.codoon.utils.Utils;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.Toast;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;
import android.widget.TextView;

public class MainFragment extends BaseFragment implements OnClickListener,
		AMapLocationListener {
	private final int BIND_SUCESS = 77;
	private final int BIND_FAIL = 78;
	private RelativeLayout currButton = null;
	private RelativeLayout historyButton = null;
	private FragmentManager fManager = null;
	private FragmentTransaction fTransaction = null;
	private LinearLayout tab_layout = null;
	private SlidingDrawer sdDrawer;
	private ImageView mapupView;
	private LinearLayout mapdownLayout;
	private AMap aMap;
	private MapView mapView;
	// private OnLocationChangedListener mListener;
	private LocationManagerProxy mAMapLocationManager;
//	private TextView electriView;
//	private ImageView electriImageView;
	private ImageView cursor1, cursor2;
	// private int isConect;
//	private int battery_num;
	public NewCompassFragment cFragment;
	private GraphFragment gFragment;
	private TextView kmTextView;// 总里程
	private TextView qixingView;// 骑行
	private TextView kcView;// 总消耗
	private TextView cKmView;// 本次里程
	private TextView xHView;// 本次消耗
	private TextView aSpView;// 平均速度
	private TextView aTpView;// 平均踏频
	private TextView maxSpView;// 平均速度
	private TextView maxTpView;// 平均踏频
	private int speed;
	private int taPing;
	private int allSpeed;
	private int allTaPing;
	private int circleNum;
	private float maxSpeed = 0;
	private float maxTaPin = 0;
	private float allDistance = 0;
	private float allKc = 0;
	private float distance;
	private ImageView mylocationImageView, shareImageView;
	private ArrayList<LatLng> latlngList = new ArrayList<LatLng>();
	private ArrayList<LatLng> latlngList_path = new ArrayList<LatLng>();
	private LatLng marker1;

	private Marker marker = null;// 当前轨迹点图案
	private Marker markericon = null;// 当前轨迹点图案
	private FrameLayout layout = null;
	private Circle circle = null;
	private boolean isbool = true;
	private boolean ismap = false;
	private Intent intent;
	private Intent mapIntent;
	private SQLOperateImpl dbImpl;
	private int speed_count = 0;
	private int tp_count = 0;

	private BikeAllData bikeDatas;
	private String isAuto;// 判断是否需要自动绑定码表

	private boolean isOpen = true;
	private boolean isToast = false;
	private BluetoothAdapter blueAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.mainfragment, null);
		initView(view);
		init(view);
		setLeftBtBack(R.drawable.ic_top_menu, true);
		setTitleText(R.string.app_name);
		layout = (FrameLayout) view.findViewById(R.id.content_fragment);
		((SlidingActivity) getActivity()).menu.addIgnoredView(layout);
		dbImpl = new SQLOperateImpl(getActivity());
		isAuto = getArguments().getString("fag");
		init();
		String isBind = Utils.getpreference(getActivity(), MyApplication.BIND);
		if (isBind.equals(MyApplication.SUCESS)) {
			if (isAuto.equals("auto")) {
				autoCheckBlue();
			}
		}
		return view;
	}

	private void autoCheckBlue() {
		blueAdapter = BluetoothAdapter.getDefaultAdapter();
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (isOpen) {
					Message msg = handler.obtainMessage();
					try {
						boolean isenable = blueAdapter.isEnabled();
						Thread.sleep(1000);
						if (isenable) {
							isOpen = false;
							msg.what = BIND_SUCESS;
						} else {
							msg.what = BIND_FAIL;
						}
					} catch (InterruptedException e) {
						msg.what = MyApplication.READ_FAIL;
					}
					handler.sendMessage(msg);
				}
			}
		}).start();
	}

	private void getHistoryData() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				bikeDatas = JsonUtils.parseHistoryData(Utils.getpreference(
						getActivity(), MyApplication.USERID), Utils
						.getpreference(getActivity(), MyApplication.Token));
				Message message = handler.obtainMessage();
				if (null == bikeDatas) {
					message.what = MyApplication.READ_FAIL;
				} else {
					message.what = MyApplication.READ_SUCCESS;
				}
				handler.sendMessage(message);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case MyApplication.READ_SUCCESS:
				if (null != bikeDatas) {
					if (null != bikeDatas.statistic) {
						String total_distance = bikeDatas.statistic.total_distance
								+ "";
						String total_calories = bikeDatas.statistic.total_calories
								+ "";
						kmTextView.setText(total_distance + "\n总里程（公里）");
						kcView.setText(total_calories + "\n总消耗（千卡）");

						if (null != total_distance
								&& !total_distance.equals("")) {
							if (null != getActivity()) {
								Utils.savePreferencset(getActivity(),
										MyApplication.ALL_DISTANCE,
										total_distance);
							}
						}
						if (null != total_calories
								&& !total_calories.equals("")) {
							if (null != getActivity()) {
								Utils.savePreferencset(getActivity(),
										MyApplication.ALL_XIAOHAO,
										total_calories);
							}
						}
					}
				}
				break;

			case MyApplication.READ_FAIL:
				if (null != getActivity()) {
					Toast.makeText(getActivity(), "获取失败", Toast.LENGTH_SHORT)
							.show();
				}
				break;
			case BIND_SUCESS:
				if (null != getActivity()) {
					if (null == ((SlidingActivity) getActivity()).serIntent) {
						((SlidingActivity) getActivity()).serIntent = new Intent(
								getActivity(), BlueConectService.class);
						getActivity().startService(
								((SlidingActivity) getActivity()).serIntent);
					}

				}
				break;
			case BIND_FAIL:
				if (null != getActivity()) {
					if (!isToast) {
						isToast = true;
						blueAdapter.enable();
					}
				}
				break;
			case BlueToothUtils.CHANGE_COMMAND_STATE:
				MyApplication.mSyncDeviceDataManager
						.SendDataToDevice(CODMBCommandData.getPostDeviceID());
				if (null != getActivity()) {
					Utils.savePreference(getActivity(), MyApplication.BIND,
							MyApplication.SUCESS);
					((SlidingActivity)getActivity()).intentData = new Intent(getActivity(),
							BlueRealTimeService.class);
					getActivity().startService(((SlidingActivity)getActivity()).intentData);
//					setElectri();
				}
				break;
			}

		};
	};

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mapView.onCreate(savedInstanceState);
		fManager = getChildFragmentManager();
		fTransaction = fManager.beginTransaction();
		cFragment = new NewCompassFragment();
		gFragment = new GraphFragment();
		fTransaction.add(R.id.content_fragment, cFragment);
		fTransaction.add(R.id.content_fragment, gFragment);
		fTransaction.hide(gFragment);
		fTransaction.commitAllowingStateLoss();
//		setElectri();
		setSdDrawer();

		IntentFilter filter = new IntentFilter();// 创建IntentFilter对象
		filter.addAction(MyApplication.ACTION_BLURTHOOT);
		filter.addAction(MyApplication.ACTION_MAP);
		filter.addAction(MyApplication.ACTION_STOP);
		filter.addAction(MyApplication.ACTION_BLURTHOOTDEVICE);
		getActivity().registerReceiver(mRecever, filter);

		// intent = new Intent(getActivity(), PlayService.class);
		//
		// getActivity().startService(intent);

		// String isBind = Utils.getpreference(getActivity(),
		// MyApplication.BIND);
		// if (isBind.equals(MyApplication.SUCESS)) {
		// autoBind();
		// }
		// getHistoryData();
	}

	// private void autoBind() {
	// if (isAuto.equals("auto")) {
	// if (null != getActivity()) {
	// BlueToothUtils.scanDevice(getActivity(), handler);
	// }
	//
	// }
	// }

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mapIntent = new Intent(activity, MapService.class);
		activity.startService(mapIntent);
	}

	private BroadcastReceiver mRecever = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			if (arg1.getAction().equals(MyApplication.ACTION_BLURTHOOTDEVICE)) {
				new BlueToothUtils(handler).conectDevice(arg0);
				BluetoothDevice mBlueDevice = arg1.getExtras().getParcelable(
						"device");
				MyApplication.mSyncDeviceDataManager.start(mBlueDevice);

			} else if (arg1.getAction().equals(MyApplication.ACTION_BLURTHOOT)) {
				int data[] = arg1.getExtras().getIntArray("data");
				if (data.length > 2) {
					speed = data[0];
					taPing = data[1];
					circleNum += data[2];
					if (speed > 0) {
						++speed_count;
						allSpeed += speed;
					}
					if (taPing > 0) {
						++tp_count;
						allTaPing += taPing;
					}

					getMaxSpeed(speed);
					getMaxTaPin(taPing);
					int perimeter = Utils.getIntpreference(getActivity(),
							MyApplication.PERIMETER, 112);
					int min = Utils.getIntpreferenceset(getActivity(),
							MyApplication.DATA, 3);
					// float f = (circleNum * (perimeter / 100000));
					float f = ((float) (circleNum) * ((float) perimeter / (float) 100000));
					distance = distance
							+ (float) (Math.round(f * 100) / (float) 100);
					Utils.saveIntPreference(getActivity(),
							MyApplication.DISTANCE, (int) distance);
					float calories = Float.parseFloat(getKc());
					// float avg_speed = distance / ((float) min / 60);
					// float avg_pedal_rate = allTaPing / ((float) min / 60);
					float avg_speed = 0;
					float avg_pedal_rate = 0;
					DecimalFormat fnum = new DecimalFormat("##0.00");

					if (speed_count > 0) {
						// allDistance += (float) (Math.round(distance * 100) /
						// (float) 100);
						avg_speed = (float) allSpeed / (float) speed_count;
						avg_speed = Float.parseFloat(fnum.format(avg_speed));
					}
					if (tp_count > 0) {
						avg_pedal_rate = (float) allTaPing / (float) tp_count;
						avg_pedal_rate = Float.parseFloat(fnum
								.format(avg_pedal_rate));
					}

					allKc += calories;
					setTextInfo(distance, (float) allKc, (float) avg_speed,
							(float) avg_pedal_rate);
				}
			} else if (arg1.getAction().equals(MyApplication.ACTION_MAP)) {
				ismap = false;
				if (speed > 0)
					location();
			} else if (arg1.getAction().equals(MyApplication.ACTION_STOP)) {
				maxSpView.setText(0 + "km/h");
				maxTpView.setText(0 + "转/分");
				cKmView.setText(0 + "km");
				xHView.setText(0 + "kcal");
				aSpView.setText(0 + "km/h");
				aTpView.setText(0 + "rmp");
			}
		}

	};

	/**
	 * 计算卡路里
	 * 
	 * @Title: getCalories
	 * @Description: TODO
	 * @return float
	 */
	private float getCalories() {
		float calories = 0;
		if (speed < 161) {
			calories = speed * 4;
			calories = calories / 161;
		} else {
			calories = speed * 12;
			calories = calories / 161;
			calories = calories - 8;
		}
		return calories;

	}

	private String getKc() {
		DecimalFormat fnum = new DecimalFormat("##0.00");
		String kc = "0";
		float K = 0;
		float weight = 60;
		if (null != getActivity()) {
			weight = Utils.getFloatpreference(getActivity(),
					MyApplication.WEIGHT, 60);
		}
		K = 106 * weight * getCalories() / (float) 180000;
		// K = K / (float) 1000;
		kc = fnum.format(K);
		return kc;
	}

	private void initView(View view) {
		currButton = (RelativeLayout) view.findViewById(R.id.tab1);
		historyButton = (RelativeLayout) view.findViewById(R.id.tab2);
		mContext = getActivity();
		tab_layout = (LinearLayout) view.findViewById(R.id.tab_layout);
		sdDrawer = (SlidingDrawer) view.findViewById(R.id.slidingdrawer);

		cursor1 = (ImageView) view.findViewById(R.id.cursor1);
		cursor2 = (ImageView) view.findViewById(R.id.cursor2);
		mapView = (MapView) view.findViewById(R.id.map);
//		electriView = (TextView) view.findViewById(R.id.electri);
//		electriImageView = (ImageView) view.findViewById(R.id.electri_img);
		mapupView = (ImageView) view.findViewById(R.id.map_up);
		mapdownLayout = (LinearLayout) view.findViewById(R.id.map_down);

		kmTextView = (TextView) view.findViewById(R.id.km);
		qixingView = (TextView) view.findViewById(R.id.qixing);
		kcView = (TextView) view.findViewById(R.id.kc);
		cKmView = (TextView) view.findViewById(R.id.c_km);
		xHView = (TextView) view.findViewById(R.id.xiaohao);
		aSpView = (TextView) view.findViewById(R.id.a_speed);
		aTpView = (TextView) view.findViewById(R.id.a_tp);
		maxSpView = (TextView) view.findViewById(R.id.max_sp);
		maxTpView = (TextView) view.findViewById(R.id.max_tp);

		// Bundle bundle = getArguments();
		// isConect = bundle.getInt(MyApplication.CONECT_TAG);
		currButton.setOnClickListener(this);
		historyButton.setOnClickListener(this);
		mylocationImageView = (ImageView) view.findViewById(R.id.locationimg);
		mylocationImageView.setOnClickListener(this);
		shareImageView = (ImageView) view.findViewById(R.id.shareimg);
		shareImageView.setOnClickListener(this);
		kmTextView.setText(Utils.getpreferenceset(getActivity(),
				MyApplication.ALL_DISTANCE) + "\n总里程（公里）");
		kcView.setText(Utils.getpreferenceset(getActivity(),
				MyApplication.ALL_XIAOHAO) + "\n总消耗（千卡）");
	}

	@SuppressWarnings("deprecation")
	private void setSdDrawer() {
		sdDrawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {

			@Override
			public void onDrawerOpened() {
				ismap = true;
				mapupView.setVisibility(View.GONE);
				mapdownLayout.setVisibility(View.VISIBLE);
				getHistoryData();
				location();
				if (null != getActivity()) {
					boolean isGps = Utils.getBooleanpreference(getActivity(),
							MyApplication.GPS);
					if (isGps) {
						drawLine(latlngList);
					}
				}

			}
		});

		sdDrawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {

			@Override
			public void onDrawerClosed() {
				ismap = false;
				mapupView.setVisibility(View.VISIBLE);
				mapdownLayout.setVisibility(View.GONE);

			}
		});
	}

	private void getMaxSpeed(int speed) {
		if (maxSpeed < speed) {
			maxSpeed = speed;
		}
	}

	private void getMaxTaPin(int tapin) {
		if (maxTaPin < tapin) {
			maxTaPin = tapin;
		}
	}
	
//	@Override
//	public void onHiddenChanged(boolean hidden) {
//		super.onHiddenChanged(hidden);
//		if(!hidden){
//			setElectri();
//		}
//	}

//	/**
//	 * 设置电池显示
//	 * 
//	 * @Title: setElectri
//	 * @Description: TODO void
//	 */
//	private void setElectri() {
//		String isConect = "";
//		if (null != getActivity()) {
//			isConect = Utils.getpreference(getActivity(), MyApplication.BIND);
//		}
//		if (!isConect.equals(MyApplication.SUCESS)) {
//			electriView.setText("未连接");
//			electriView.setVisibility(View.VISIBLE);
//			electriImageView.setVisibility(View.GONE);
//		} else {
//			electriView.setVisibility(View.INVISIBLE);
//			electriImageView.setVisibility(View.VISIBLE);
//			battery_num = Utils.getIntpreference(getActivity(),
//					MyApplication.BATTERY, 80);
//			if (battery_num == 0) {
//				electriImageView.setImageResource(R.drawable.e_0);
//			} else if (battery_num == 10) {
//				electriImageView.setImageResource(R.drawable.e_10);
//			} else if (battery_num == 20) {
//				electriImageView.setImageResource(R.drawable.e_20);
//			} else if (battery_num == 40) {
//				electriImageView.setImageResource(R.drawable.e_40);
//			} else if (battery_num == 60) {
//				electriImageView.setImageResource(R.drawable.e_60);
//			} else if (battery_num == 80) {
//				electriImageView.setImageResource(R.drawable.e_80);
//			} else if (battery_num == 100) {
//				electriImageView.setImageResource(R.drawable.e_100);
//			}
//		}
//	}

	private void setTextInfo(float distance, float c, float a_sp, float a_tp) {
		DecimalFormat fnum = new DecimalFormat("##0.00");
		String tt = fnum.format(distance);
		String kk = fnum.format(allKc);
		String sp = fnum.format(a_sp);
		String tp = fnum.format(a_tp);
		String m_sp = fnum.format(maxSpeed);
		String m_tp = fnum.format(maxTaPin);
		MyApplication.Kc = Float.parseFloat(kk);
		MyApplication.c_distance = Float.parseFloat(tt);
		MyApplication.max_sp = maxSpeed;
		MyApplication.max_tp = maxTaPin;
		MyApplication.a_sp = a_sp;
		MyApplication.a_tp = a_tp;
		maxSpView.setText(m_sp + "km/h");
		maxTpView.setText(m_tp + "转/分");
		cKmView.setText(tt + "km");
		xHView.setText(kk + "kcal");
		aSpView.setText(sp + "km/h");
		aTpView.setText(tp + "rmp");
	}

	/**
	 * 初始化AMap对象
	 */
	private void init() {
		if (aMap == null) {
			aMap = mapView.getMap();
			setUpMap();
		}
	}

	/**
	 * 设置一些amap的属性
	 */
	private void setUpMap() {
		// 自定义系统定位小蓝点
		MyLocationStyle myLocationStyle = new MyLocationStyle();
		myLocationStyle.myLocationIcon(BitmapDescriptorFactory
				.fromResource(R.drawable.location_marker));// 设置小蓝点的图标
		myLocationStyle.strokeColor(Color.BLACK);// 设置圆形的边框颜色
		myLocationStyle.radiusFillColor(Color.argb(100, 0, 0, 180));// 设置圆形的填充颜色
		// myLocationStyle.anchor(int,int)//设置小蓝点的锚点
		myLocationStyle.strokeWidth(1.0f);// 设置圆形的边框粗细
		aMap.setMyLocationStyle(myLocationStyle);
		// aMap.setLocationSource(this);// 设置定位监听
		aMap.getUiSettings().setMyLocationButtonEnabled(false);// 设置默认定位按钮是否显示
		aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false

		// getRote();
	}

	@Override
	public void onClick(View v) {
		fTransaction = fManager.beginTransaction();
		switch (v.getId()) {
		case R.id.tab1:
//			setElectri();
			sdDrawer.setVisibility(View.VISIBLE);
			cursor1.setVisibility(View.VISIBLE);
			cursor2.setVisibility(View.GONE);
			// fTransaction.add(R.id.content_fragment, new CompassFragment());
			if (cFragment.isHidden()) {
				fTransaction.show(cFragment);
				fTransaction.hide(gFragment);
			}
			fTransaction.commitAllowingStateLoss();
			break;
		case R.id.tab2:
//			electriView.setVisibility(View.GONE);
//			electriImageView.setVisibility(View.GONE);
			sdDrawer.setVisibility(View.GONE);
			cursor1.setVisibility(View.GONE);
			cursor2.setVisibility(View.VISIBLE);
			// fTransaction.add(R.id.content_fragment, new GraphFragment());
			if (gFragment.isHidden()) {
				fTransaction.show(gFragment);
				fTransaction.hide(cFragment);
			}
			fTransaction.commitAllowingStateLoss();
			break;

		case R.id.locationimg:
			location();
			ismap = true;
			break;
		case R.id.shareimg:
			Utils.GetandSaveCurrentImage(getActivity());
			// #咕咚智能码表#告诉我，我已经骑行了XX公里，用时00h00、，燃烧 123大卡。
//			String time = Utils.getStrTime(System.currentTimeMillis());
			String useTime = Utils.getpreference(getActivity(),
					MyApplication.HOUR)+"h"
					+ Utils.getpreference(getActivity(), MyApplication.MIN)+"'";
			if("".equals(useTime)){
				useTime="00h00'";
			}
			DecimalFormat fnum = new DecimalFormat("##0.00");
			String Content = "#咕咚智能码表#告诉我，我已经骑行了" +fnum.format(distance)  + "公里，用时"
					+ useTime + ",燃烧" + fnum.format(allKc) + "大卡";
			Utils.showShare(getActivity(), Content, Utils.getSDCardPath()
					+ "/codoon/ScreenImage/Screen_1.png");
			break;

		}

	}

	private void location() {
		if (null == mAMapLocationManager) {
			mAMapLocationManager = LocationManagerProxy
					.getInstance(getActivity());
		}
		/*
		 * mAMapLocManager.setGpsEnable(false);
		 * 1.0.2版本新增方法，设置true表示混合定位中包含gps定位，false表示纯网络定位，默认是true Location
		 * API定位采用GPS和网络混合定位方式
		 * ，第一个参数是定位provider，第二个参数时间最短是2000毫秒，第三个参数距离间隔单位是米，第四个参数是定位监听者
		 */
		mAMapLocationManager.requestLocationUpdates(
				LocationProviderProxy.AMapNetwork, 2000, 10, this);

	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
		if(!hidden&&null!=cFragment){
			cFragment.setElectri();
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mapView.onResume();
		if(null!=cFragment){
			cFragment.setElectri();
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mapView.onPause();
		// deactivate();
	}

	/**
	 * 方法必须重写
	 */
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		mapView.onSaveInstanceState(outState);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (null != mapView) {
			mapView.onDestroy();
		}
		if (marker != null) {
			marker.destroy();
		}
		if (null != aMap) {
			aMap.clear();
		}
		if (null != mAMapLocationManager) {
			mAMapLocationManager.destory();
		}
		if (null != mRecever)
			getActivity().unregisterReceiver(mRecever);
		if (null != intent)
			getActivity().stopService(intent);
		if (null != mapIntent)
			getActivity().stopService(mapIntent);
		isbool = false;
	}

	private void drawLine(ArrayList<LatLng> list) {
		// TODO Auto-generated method stub
		aMap.clear();
		if (list.size() > 0) {
			LatLng replayGeoPoint = list.get(list.size() - 1);

			if (marker != null) {
				marker.destroy();
			}
			// 添加汽车位置
			MarkerOptions markerOptions = new MarkerOptions();
			markerOptions
					.position(replayGeoPoint)
					.title("起点")
					.snippet(" ")
					.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
							.decodeResource(getResources(),
									R.drawable.icon_marka))).anchor(0.5f, 0.5f);
			marker = aMap.addMarker(markerOptions);
			// 增加起点开始
			aMap.addMarker(new MarkerOptions()
					.position(list.get(0))
					.title("起点")
					.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
							.decodeResource(getResources(),
									R.drawable.nav_route_result_start_point))));
			// 增加起点结束
			if (list.size() > 1) {
				PolylineOptions polylineOptions = (new PolylineOptions())
						.addAll(list).color(Color.rgb(9, 129, 240)).width(6.0f);
				aMap.addPolyline(polylineOptions);

			}
			// for (int i = 1; i < list.size() - 1; i++) {
			// // aMap.addMarker(new MarkerOptions()
			// // .position(list.get(i))
			// // .title("中间")
			// // .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
			// // .decodeResource(getResources(),
			// // R.drawable.icon_marka))));
			// aMap.addMarker(new MarkerOptions().position(list.get(i))
			// .title("中间"));
			// }
			aMap.addMarker(new MarkerOptions()
					.position(list.get(list.size() - 1))
					.title("终点")
					.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
							.decodeResource(getResources(),
									R.drawable.nav_route_result_end_point))));
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// marker1 = new LatLng(location.getLatitude() * 1E6,
		// location.getLongitude() * 1E6);
		// latlngList.add(marker1);
		// drawLine(latlngList);
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	/**
	 * 定位成功后回调函数
	 */
	@Override
	public void onLocationChanged(final AMapLocation aLocation) {
		if (ismap) {
			MyLocal(aLocation);
			ismap = false;
		} else {
			gpsLine(aLocation);
		}
		// if (null != aLocation) {
		// LatLng latLng = new LatLng(aLocation.getLatitude(),
		// aLocation.getLongitude());
		// aMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
		// // handler.postDelayed(new Runnable() {
		// //
		// // @Override
		// // public void run() {
		// // dbImpl.add(aLocation.getLatitude() * 1E6 + "",
		// // aLocation.getLongitude() * 1E6 + "",
		// // Utils.getStrTime(System.currentTimeMillis()));
		// //
		// // }
		// // }, 200);
		// marker1 = new LatLng(aLocation.getLatitude() * 1E6,
		// aLocation.getLongitude() * 1E6);
		// latlngList.add(marker1);
		// // if (null != getActivity()) {
		// // if (Utils
		// // .getBooleanpreference(getActivity(), MyApplication.GPS)) {
		// // drawLine(latlngList);
		// // }
		// // }
		// if (null != markericon) {
		// markericon.destroy();
		// }
		// if (null != circle) {
		// circle.remove();
		// }
		// marker = aMap.addMarker(new MarkerOptions().position(latLng)
		// .anchor(0.5f, 0.5f)
		// // 锚点设置为中心
		// .icon(BitmapDescriptorFactory
		// .fromResource(R.drawable.location_marker)));
		// // 自定义定位成功后绘制圆形
		// circle = aMap.addCircle(new CircleOptions().center(latLng)
		// .radius(50).fillColor(Color.argb(100, 0, 0, 180))
		// .strokeColor(Color.BLACK).strokeWidth(1.0f));
		//
		// }
	}

	private void MyLocal(AMapLocation aLocation) {
		if (null != aLocation) {
			LatLng latLng = new LatLng(aLocation.getLatitude(),
					aLocation.getLongitude());
			aMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
			if (null != markericon) {
				markericon.destroy();
			}
			if (null != circle) {
				circle.remove();
			}
			marker = aMap.addMarker(new MarkerOptions().position(latLng)
					.anchor(0.5f, 0.5f)
					// 锚点设置为中心
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.location_marker)));
			// 自定义定位成功后绘制圆形
			circle = aMap.addCircle(new CircleOptions().center(latLng)
					.radius(50).fillColor(Color.argb(100, 0, 0, 180))
					.strokeColor(Color.BLACK).strokeWidth(1.0f));
		}
	}

	private void gpsLine(AMapLocation aLocation) {
		marker1 = new LatLng(aLocation.getLatitude(), aLocation.getLongitude());
		latlngList.add(marker1);

	}

}
