package com.inwhoop.codoon.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.json.JSONObject;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.activity.SlidingActivity;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.entity.BikeSettingBean;
import com.inwhoop.codoon.entity.BikeSettingEntity;
import com.inwhoop.codoon.entity.EquipmentInfoBean;
import com.inwhoop.codoon.entity.ScaleInfo;
import com.inwhoop.codoon.utils.JsonUtils;
import com.inwhoop.codoon.utils.Utils;
import com.inwhoop.codoon.view.LineView;

import android.content.res.Resources;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

/**
 * 码表设置
 * 
 * @Project: CodoonBikeApp
 * @Title: SetFragment.java
 * @Package com.inwhoop.codoon.fragment
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-5-9 上午11:58:23
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class SetFragment extends BaseFragment implements OnClickListener {

	private LineView line, sizeline;

	private SeekBar sizeBar, widthBar;

	private List<ScaleInfo> list = new ArrayList<ScaleInfo>();

	private TextView sizeTextView, widthTextView, girthTextView;

	private EditText weightEditText = null;

	private Button submitButton = null;

	private int sizespace = 0;

	private int widspace = 0;

	private String[] sizeinfo = null;

	private String[] size14 = null;
	private String[] size16 = null;
	private String[] size20 = null;
	private String[] size24 = null;
	private String[] size26 = null;
	private String[] size27 = null;
	private String[] size28 = null;

	private String[] size14per = null;
	private String[] size16per = null;
	private String[] size20per = null;
	private String[] size24per = null;
	private String[] size26per = null;
	private String[] size27per = null;
	private String[] size28per = null;

	private int sizepos = 0;

	private int widpos = 0;

	private List<String[]> widlist = new ArrayList<String[]>();

	private List<String[]> perlist = new ArrayList<String[]>();

	private boolean flag = false;

	private String girth = "";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.set_fragment, null);
		init(view);
		return view;
	}

	@Override
	public void init(View view) {
		super.init(view);
		setTitleText(R.string.clock);
		setLeftBtBack(R.drawable.ic_top_menu, true);
		mContext = getActivity();
		initData();
		sizeTextView = (TextView) view.findViewById(R.id.sizetext);
		girthTextView = (TextView) view.findViewById(R.id.girth);
		sizeTextView.setText(sizeinfo[0]);
		widthTextView = (TextView) view.findViewById(R.id.widthtext);
		line = (LineView) view.findViewById(R.id.line);
		sizeline = (LineView) view.findViewById(R.id.sizeline);
		sizeline.setMaxnum(6);
		getlist();
		line.setMaxnum(1);
		sizeBar = (SeekBar) view.findViewById(R.id.sizebar);
		sizeBar.setMax(10000);
		sizespace = 10000 / 6;
		if ("".equals(Utils.getpreferenceset(getActivity(),
				MyApplication.WHEEL_SIZE))) {
			sizeBar.setProgress(0);
		} else {
			sizeBar.setProgress(Integer.parseInt(Utils.getpreferenceset(
					getActivity(), MyApplication.WHEEL_SIZE)));
			sizepos = Integer.parseInt(Utils.getpreferenceset(getActivity(),
					MyApplication.WHEEL_SIZE)) / sizespace;
		}
		widthBar = (SeekBar) view.findViewById(R.id.widthseekbar);
		widthBar.setMax(10000);
		((SlidingActivity) getActivity()).menu.addIgnoredView(sizeBar);
		((SlidingActivity) getActivity()).menu.addIgnoredView(widthBar);

		if ("".equals(Utils.getpreferenceset(getActivity(),
				MyApplication.WHEEL_SIZE_TEXT))) {
			sizeTextView.setText(sizeinfo[0]);
		} else {
			sizeTextView.setText(Utils.getpreferenceset(getActivity(),
					MyApplication.WHEEL_SIZE_TEXT));
		}
		// widthTextView.setText(widlist.get(0)[0]);
		widthTextView.setText(Utils.getpreferenceset(getActivity(),
				MyApplication.WHEEL_WIDTH_TEXT));
		widthBar.setProgress(widthBar.getMax());
		if (0 == Utils.getIntpreferenceset(getActivity(),
				MyApplication.WHEEL_WIDTH_NUMBER, 0)) {
			line.setMaxnum(1);
			widspace = 10000 / list.get(0).number;
		} else {
			line.setMaxnum(Utils.getIntpreferenceset(getActivity(),
					MyApplication.WHEEL_WIDTH_NUMBER, 0));
			widspace = 10000 / Utils.getIntpreferenceset(getActivity(),
					MyApplication.WHEEL_WIDTH_NUMBER, 0);
		}
		if ("".equals(Utils.getpreferenceset(getActivity(),
				MyApplication.WHEEL_WIDTH))) {
			widthBar.setProgress(widthBar.getMax());
		} else {
			widthBar.setProgress(Integer.parseInt(Utils.getpreferenceset(
					getActivity(), MyApplication.WHEEL_WIDTH)));
			widpos = Integer.parseInt(Utils.getpreferenceset(
					getActivity(), MyApplication.WHEEL_WIDTH))/widspace;
		}
		if("".equals(Utils.getpreferenceset(
					getActivity(), MyApplication.WHEEL_GRITH))){
			girthTextView.setText(perlist.get(0)[0]);
		}else{
			girthTextView.setText(Utils.getpreferenceset(
					getActivity(), MyApplication.WHEEL_GRITH));
		}
		weightEditText = (EditText) view.findViewById(R.id.weightedit);
		float weight=Utils.getFloatpreference(getActivity(),
				MyApplication.WEIGHT, -1);
		if(weight>-1){
			weightEditText.setText(weight+"");
		}
		submitButton = (Button) view.findViewById(R.id.submit);
		submitButton.setOnClickListener(this);
		sizeBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				Utils.savePreferencset(getActivity(),
						MyApplication.WHEEL_SIZE_TEXT, sizeinfo[sizepos]);
				Utils.savePreferencset(getActivity(), MyApplication.WHEEL_SIZE,
						"" + sizeBar.getProgress());
				Utils.savePreferencset(getActivity(),
						MyApplication.WHEEL_WIDTH_TEXT, widlist.get(sizepos)[0]);
				Utils.savePreferencset(getActivity(),
						MyApplication.WHEEL_WIDTH, "" + widthBar.getProgress());
				Utils.saveIntPreferenceset(getActivity(),
						MyApplication.WHEEL_WIDTH_NUMBER, line.getNumber());
				Utils.savePreferencset(getActivity(),
						MyApplication.WHEEL_GRITH, "" + perlist.get(sizepos)[0]);
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {

			}

			@Override
			public void onProgressChanged(SeekBar arg0, int progress,
					boolean arg2) {
				sizepos = progress / sizespace;
				sizeBar.setProgress(sizepos * sizespace);
				line.setMaxnum(list.get(sizepos).number);
				line.invalidate();
				sizeTextView.setText(sizeinfo[sizepos]);
				widspace = 10000 / list.get(sizepos).number;
				widthTextView.setText(widlist.get(sizepos)[0]);
				widthBar.setProgress(0);
				if (sizepos < 3) {
					widthBar.setProgress(widthBar.getMax());
				}
				girthTextView.setText(perlist.get(sizepos)[0]);
			}
		});
		widthBar.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_MOVE:
					int progress = widthBar.getProgress();
					widpos = progress / widspace;
					if (sizepos > 2) {
						widthTextView.setText(widlist.get(sizepos)[widpos]);
						girthTextView.setText(perlist.get(sizepos)[widpos]);
					} else {
						girthTextView.setText(perlist.get(sizepos)[0]);
					}
					break;

				case MotionEvent.ACTION_UP:
					addhandler.sendEmptyMessageDelayed(0, 100);
					break;

				default:
					break;
				}
				if (sizepos > 2) {
					return false;
				}
				return true;
			}
		});
		widthBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {

			}

			@Override
			public void onProgressChanged(SeekBar arg0, int progress,
					boolean flag) {
			}
		});
	}

	private Handler addhandler = new Handler() {

		public void handleMessage(Message msg) {
			int progress = widthBar.getProgress();
			int m = progress % widspace;
			if (m > widspace / 2) {
				if(widpos!=line.getNumber()){
					widpos = widpos + 1;
				}
				widthBar.setProgress((widpos + 1) * widspace);
				widthTextView.setText(widlist.get(sizepos)[widpos]);
				girthTextView.setText(perlist.get(sizepos)[widpos]);
			} else {
				widthBar.setProgress(widpos * widspace);
			}
			if (widpos < widlist.get(sizepos).length) {
				Utils.savePreferencset(getActivity(), MyApplication.WHEEL_GRITH, ""
						+ perlist.get(sizepos)[widpos]);
				Utils.savePreferencset(getActivity(),
						MyApplication.WHEEL_WIDTH_TEXT,
						widlist.get(sizepos)[widpos]);
				Utils.savePreferencset(getActivity(),
						MyApplication.WHEEL_WIDTH, "" + widthBar.getProgress());
			}
		};
	};

	/**
	 * 初始化数据
	 * 
	 * @Title: initData
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void initData() {
		Resources resources = getActivity().getResources();
		sizeinfo = resources.getStringArray(R.array.sizeinfo);
		size14 = resources.getStringArray(R.array.size_14);
		size16 = resources.getStringArray(R.array.size_16);
		size20 = resources.getStringArray(R.array.size_20);
		size24 = resources.getStringArray(R.array.size_24);
		size26 = resources.getStringArray(R.array.size_26);
		size27 = resources.getStringArray(R.array.size_27);
		size28 = resources.getStringArray(R.array.size_28);
		widlist.add(size14);
		widlist.add(size16);
		widlist.add(size20);
		widlist.add(size24);
		widlist.add(size26);
		widlist.add(size27);
		widlist.add(size28);
		size14per = resources.getStringArray(R.array.size_14_perimeter);
		size16per = resources.getStringArray(R.array.size_16_perimeter);
		size20per = resources.getStringArray(R.array.size_20_perimeter);
		size24per = resources.getStringArray(R.array.size_24_perimeter);
		size26per = resources.getStringArray(R.array.size_26_perimeter);
		size27per = resources.getStringArray(R.array.size_27_perimeter);
		size28per = resources.getStringArray(R.array.size_28_perimeter);
		perlist.add(size14per);
		perlist.add(size16per);
		perlist.add(size20per);
		perlist.add(size24per);
		perlist.add(size26per);
		perlist.add(size27per);
		perlist.add(size28per);
	}

	private List<ScaleInfo> getlist() {
		ScaleInfo info = null;
		for (int i = 0; i < 7; i++) {
			info = new ScaleInfo();
			switch (i) {
			case 0:
				info.position = i;
				info.number = 1;
				break;

			case 1:
				info.position = i;
				info.number = 1;
				break;

			case 2:
				info.position = i;
				info.number = 1;
				break;

			case 3:
				info.position = i;
				info.number = 1;
				break;

			case 4:
				info.position = i;
				info.number = 9;
				break;
			case 5:
				info.position = i;
				info.number = 1;
				break;
			case 6:
				info.position = i;
				info.number = 3;
				break;

			default:
				break;
			}
			list.add(info);
		}
		return list;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:
			if (null != getActivity()) {
				String s = weightEditText.getText().toString().trim();
				if (null != s && !s.equals("")) {
					Utils.saveFloatPreference(getActivity(),
							MyApplication.WEIGHT, Float.parseFloat(s));
				}
			}
			submit();
			break;

		default:
			break;
		}
	}
	
	private void submit() {
//		if("".equals(weightEditText.getText().toString().trim())){
//			showToast("请填写体重信息~~");
//			return;
//		}
		showProgressDialog("正在上传信息...");
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					girth = girthTextView.getText().toString().trim();
					Utils.saveIntPreference(getActivity(),
							MyApplication.PERIMETER, Integer.valueOf(girth));
					BikeSettingEntity entity = new BikeSettingEntity();
					entity.setInfo(Utils.getEquipmentinfo(getActivity()));
					BikeSettingBean bean = new BikeSettingBean();
					bean.setPerimeter(Integer.parseInt(girth));
					entity.setData(bean);
					String result = JsonUtils.uploadGirth(entity, Utils
							.getpreference(getActivity(), MyApplication.Token));
					msg.obj = result;
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;

				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				// showToast("信息保存失败");
				// break;

			case MyApplication.READ_SUCCESS:
				try {
					// JSONObject obj = new JSONObject(msg.obj.toString());
					// if ("OK".equals(obj.getString("status"))) {
					// showToast("信息保存成功");
					MyApplication.flag = true;
					if (null != getActivity()) {
//						weightEditText.setText("");
						
						FragmentTransaction ft = getActivity()
								.getSupportFragmentManager().beginTransaction();
						if (null != (((SlidingActivity) getActivity()).menuFragment.setFragment)) {
							// ft.hide((((SlidingActivity)getActivity()).menuFragment.mainFragment));
							// (((SlidingActivity)getActivity()).menuFragment.mainFragment).setArguments(bundle);
							ft.remove((((SlidingActivity) getActivity()).menuFragment.setFragment));
							(((SlidingActivity) getActivity()).menuFragment.setFragment) = null;
						}
						if (null != (((SlidingActivity) getActivity()).menuFragment.startBindFragment)) {
							// ft.hide((((SlidingActivity)getActivity()).menuFragment.mainFragment));
							// (((SlidingActivity)getActivity()).menuFragment.mainFragment).setArguments(bundle);
							ft.remove((((SlidingActivity) getActivity()).menuFragment.startBindFragment));
							(((SlidingActivity) getActivity()).menuFragment.startBindFragment) = null;
						}
						if(null!=(((SlidingActivity)getActivity()).menuFragment.mainFragment)){
//							ft.hide((((SlidingActivity)getActivity()).menuFragment.mainFragment));
//							(((SlidingActivity)getActivity()).menuFragment.mainFragment).setArguments(bundle);
							ft.show((((SlidingActivity)getActivity()).menuFragment.mainFragment));
						}else{
							Bundle bundle = new Bundle();
							bundle.putString("fag", "");
							(((SlidingActivity)getActivity()).menuFragment.mainFragment) = new MainFragment();
							(((SlidingActivity)getActivity()).menuFragment.mainFragment).setArguments(bundle);
							ft.add(R.id.content_frame, (((SlidingActivity)getActivity()).menuFragment.mainFragment));
						}
						ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
						ft.commitAllowingStateLoss();
						// Bundle bundle = new Bundle();
						// bundle.putInt(MyApplication.CONECT_TAG,
						// MyApplication.CONECT);
						// mainFragment.setArguments(bundle);
					}
					// } else {
					// showToast("信息保存失败");
					// }
				} catch (Exception e) {
					System.out.println("===========eException==========="+e.toString());
				}
				break;

			default:
				break;
			}
		};
	};

}
