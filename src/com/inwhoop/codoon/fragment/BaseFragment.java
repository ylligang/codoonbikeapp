package com.inwhoop.codoon.fragment;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.activity.SlidingActivity;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.utils.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * fragment基类
 * 
 * @Project: CodoonBikeApp
 * @Title: BaseFragment.java
 * @Package com.inwhoop.codoon.fragment
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-5-9 上午9:26:44
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class BaseFragment extends Fragment {
	private Button leftButton;
	private Button rightButton;
	private TextView titleView;

	public ProgressDialog progressDialog;// 加载进度框
	public Context mContext = null;

	public void init(View view) {
		MyApplication.fragments.add(this);
		mContext = getActivity();
		MyApplication.context = mContext;
		leftButton = (Button) view.findViewById(R.id.left);
		titleView = (TextView) view.findViewById(R.id.title);
		rightButton = (Button) view.findViewById(R.id.right);
	}

	public void setLeftBtText(int textId) {
		leftButton.setVisibility(View.VISIBLE);
		leftButton.setText(textId);
	}

	public void setLeftBtBack(int imgId, final boolean isback) {
		leftButton.setVisibility(View.VISIBLE);
		leftButton.setBackgroundResource(imgId);
		leftButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (null == getActivity()) {
					return;
				}
				if (isback) {
					if (((SlidingActivity) getActivity()).menu
							.isSecondaryMenuShowing()) {
						((SlidingActivity) getActivity()).menu.showContent();
					} else {
						((SlidingActivity) getActivity()).menu.showMenu();
						String isBind = Utils.getpreference(getActivity(),
								MyApplication.BIND);
						if (isBind.equals(MyApplication.SUCESS)) {
							if (null != ((SlidingActivity) getActivity()).menuFragment.bindText) {
								((SlidingActivity) getActivity()).menuFragment.bindText
										.setText(R.string.binded);
							}
							
						}
					}
				} else {
					getActivity().finish();
				}
			}
		});
	}

	public void setTitleText(int textId) {
		titleView.setVisibility(View.VISIBLE);
		titleView.setText(textId);
	}

	public void setRightBtText(int textId) {
		rightButton.setVisibility(View.VISIBLE);
		rightButton.setText(textId);
	}

	public void setRightBtBack(int imgId) {
		rightButton.setVisibility(View.VISIBLE);
		rightButton.setBackgroundResource(imgId);
	}

	public Button getLeftButton() {
		return leftButton;
	}

	public Button getRightButton() {
		return rightButton;
	}

	/**
	 * 显示进度框
	 * 
	 * @Title: showProgressDialog
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showProgressDialog(String msg) {
		if (null != getActivity()) {
			if (null == progressDialog) {
				progressDialog = new ProgressDialog(getActivity());
			}
			progressDialog.setMessage(msg);
			progressDialog.setCancelable(true);
			progressDialog.show();
		}
	}

	/**
	 * 关闭进度框
	 * 
	 * @Title: dismissProgressDialog
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	protected void dismissProgressDialog() {
		if (null != progressDialog && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	/**
	 * 显示toast
	 * 
	 * @Title: showToast
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showToast(String msg) {
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}

}
