package com.inwhoop.codoon.utils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.text.TextUtils;

import com.inwhoop.codoon.app.MyApplication;

/**
 * 发送Http请求
 * 
 * @Project: CodoonBikeApp
 * @Title: SyncHttp.java
 * @Package com.wz.util
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-2-11 下午5:14:24
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class SyncHttp {

	/**
	 * 通过GET方式发送请求
	 * 
	 * @param url
	 *            URL地址
	 * @param params
	 *            参数
	 * @return
	 * @throws Exception
	 */
	public String httpGet(String url, String params, String mAccessToken)
			throws Exception {
		String response = ""; // 返回信息
		// 拼接请求URL
		if (null != params && !params.equals("")) {
			url += "?" + params;
		}
		// System.out.println("httpGet=" + url);
		int timeoutConnection = 15000;
		int timeoutSocket = 15000;
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters,
				timeoutConnection);
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

		// 构造HttpClient的实例
		HttpClient httpClient = new DefaultHttpClient(httpParameters);
		// 创建GET方法的实例
		HttpGet httpGet = new HttpGet(url);
		if (TextUtils.isEmpty(mAccessToken)) {
			httpGet.addHeader(MyApplication.AUTHORIZATION_KEY,
					getAuthorization());
		} else {
			httpGet.addHeader(MyApplication.AUTHORIZATION_KEY, "Bearer "
					+ mAccessToken);
		}
		// try {
		HttpResponse httpResponse = httpClient.execute(httpGet);
		int statusCode = httpResponse.getStatusLine().getStatusCode();
		// System.out.println("statusCode========= " + statusCode);
		if (statusCode == HttpStatus.SC_OK) // SC_OK = 200
		{
			// 获得返回结果
			response = EntityUtils.toString(httpResponse.getEntity());
		} else {
			response = "返回码：" + statusCode;

		}
		// System.out.println("response==== "+response);
		// } catch (Exception e) {
		// throw new Exception(e);
		// }

		return response;
	}

	/**
	 * 通过POST方式发送请求
	 * 
	 * @param url
	 *            URL地址
	 * @param params
	 *            参数
	 * @return
	 * @throws Exception
	 */
	public String httpPost(String url, List<Parameter> params, String head)
			throws Exception {
		String response = null;
		int timeoutConnection = 15000;
		int timeoutSocket = 15000;
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters,
				timeoutConnection);
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		// 构造HttpClient的实例
		HttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpPost httpPost = new HttpPost(url);
		// httpPost.addHeader("Authorization", head);
		// httpPost.addHeader("Content-Type", "application/json");
		if (null != params && params.size() >= 0) {
			// 设置httpPost请求参数
			httpPost.setEntity(new UrlEncodedFormEntity(
					buildNameValuePair(params), HTTP.UTF_8));
		}
		// 使用execute方法发送HTTP Post请求，并返回HttpResponse对象
		HttpResponse httpResponse = httpClient.execute(httpPost);
		int statusCode = httpResponse.getStatusLine().getStatusCode();
		if (statusCode == HttpStatus.SC_OK) {
			// 获得返回结果
			response = EntityUtils.toString(httpResponse.getEntity());
		} else {
			response = "返回码：" + statusCode;
		}
//		System.out.print("str= "
//				+ EntityUtils.toString(httpResponse.getEntity()));
		return response;
	}

	/**
	 * 上传数据
	 * 
	 * @Title: post
	 * @Description: TODO
	 * @param http
	 * @param data
	 * @return String
	 */
	public String post(String http, String data, String mAccessToken) {
		ByteArrayEntity arrayEntity = new ByteArrayEntity(data.getBytes());
		// arrayEntity.setContentType("binary/octet-stream");
		HttpPost httpPost = new HttpPost(http);
		httpPost.setEntity(arrayEntity);
		// httpPost.addHeader("Content-Type",
		// "application/x-www-form-urlencoded");
		httpPost.setHeader("Content-type", "application/json");
		if (TextUtils.isEmpty(mAccessToken)) {
			httpPost.addHeader(MyApplication.AUTHORIZATION_KEY,
					getAuthorization());
		} else {
			httpPost.addHeader(MyApplication.AUTHORIZATION_KEY, "Bearer "
					+ mAccessToken);
		}
//		httpPost.addHeader(MyApplication.AUTHORIZATION_KEY, "Bearer "
//				+ mAccessToken);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = null;
		try {
			response = client.execute(httpPost);
			int result = response.getStatusLine().getStatusCode();
			InputStream is = response.getEntity().getContent();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] bytedata = new byte[1024];
			int count = -1;
			while ((count = is.read(bytedata, 0, bytedata.length)) != -1) {
				outStream.write(bytedata, 0, count);
			}
			data = null;
			String s = new String(outStream.toByteArray(), "utf-8");
			return s;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 把Parameter类型集合转换成NameValuePair类型集合
	 * 
	 * @param params
	 *            参数集合
	 * @return
	 */
	private List<BasicNameValuePair> buildNameValuePair(List<Parameter> params) {
		List<BasicNameValuePair> result = new ArrayList<BasicNameValuePair>();
		for (Parameter param : params) {
			BasicNameValuePair pair = new BasicNameValuePair(param.getName(),
					param.getValue());
			result.add(pair);
		}
		return result;
	}

	public static String getAuthorization() {
		byte[] bytes = (MyApplication.HTTP_CLIENT_KEY + ":" + MyApplication.HTTP_CLIENT_SECRET)
				.getBytes();
		return "Basic " + Base64.encode(bytes, 0, bytes.length);
	}
}
