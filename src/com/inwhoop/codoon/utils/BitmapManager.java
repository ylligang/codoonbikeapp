package com.inwhoop.codoon.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StatFs;
import android.view.View;
import android.widget.ImageView;

/**
 * 图片管理类
 * 
 * @author ylligang118@126.com
 * @version V1.0
 * @Project: GameProduct
 * @Title: BitmapManager.java
 * @Package com.inwhoop.gameproduct.utils
 * @Description: TODO
 * @date 2014-4-3 下午3:49:15
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public enum BitmapManager {
	INSTANCE;
	/**
	 * 二级缓存
	 */
	private Map<String, SoftReference<Bitmap>> cache;
	/**
	 * 线程池
	 */
	private ExecutorService pool;
	/**
	 * 缓存图片控件对象
	 */
	private Map<ImageView, String> imageMap = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());
	/**
	 * sd卡缓存空间的临界点
	 */
	private static final int FREE_SD_SPACE_NEEDED_TO_CACHE = 1;
	private static int MB = 1024 * 1024;

	/**
	 * 缓存目录
	 */
	public static String DIR = Environment.getExternalStorageDirectory()
			.getPath()
			+ "/Android/data/com.inwhoop.codoon"
			+ "/gamecache/image";

	private final static String TAG = "BitmapManager";

	BitmapManager() {
		cache = new HashMap<String, SoftReference<Bitmap>>();
		pool = Executors.newFixedThreadPool(10);
	}

	/**
	 * 加载图片，优先加载本地缓存图片
	 * 
	 * @param url
	 *            图片url
	 * @param imageView
	 *            图片控件
	 * @param defaultImageId
	 *            默认图片资源id
	 * @param isCache
	 *            是否缓存sd卡
	 * 
	 * @Title: loadBitmap
	 * @Description: TODO
	 */
	public void loadBitmap(String url, ImageView imageView, int defaultImageId,
			boolean isCache) {
		Bitmap bitmap = null;
		if (imageView == null) {
			return;
		}
		if (null == url) {
			imageView.setImageResource(defaultImageId);
			return;
		}
		imageMap.put(imageView, url);
		bitmap = getBitmapFromCache(url); // 获取缓存
		if (null != bitmap) {
			imageView.setImageBitmap(bitmap);
			imageView.setVisibility(View.VISIBLE);
		} else {
			if (isCache) { // 如果开启缓存本地情况 进行取本地数据
				bitmap = getLocalBitmap(url);
			}
			if (null != bitmap) {
				imageView.setImageBitmap(bitmap);
				imageView.setVisibility(View.VISIBLE);
				url = picName(url);
				cache.put(url, new SoftReference<Bitmap>(bitmap)); // 加入内存
			} else {
				imageView.setImageResource(defaultImageId);
				startThreadDownImg(url, imageView, defaultImageId, isCache); // 获取网络图片
			}
		}
	}

	/**
	 * 加载图片，优先加载本地缓存图片
	 * 
	 * @param url
	 *            图片url
	 * @param imageView
	 *            图片控件
	 * @param defaultImageId
	 *            默认图片资源id
	 * @param isCache
	 *            是否缓存sd卡
	 * @param length
	 *            将适应的长度
	 * @param isWidth
	 *            是否是宽，否的话传入的长度就是高度来自适应
	 * 
	 * @Title: loadBitmap
	 * @Description: TODO 并适应传入的长度缩放：注意：ImageView的父布局目前只能 TODO
	 *               是linerlayout，relativelayout，FrameLayout
	 */
	public void loadBitmap(String url, final ImageView imageView,
			int defaultImageId, boolean isCache, final int length,
			boolean isWidth) {
		Bitmap bitmap = null;
		if (imageView == null) {
			return;
		}
		if (null == url) {
			imageView.setImageResource(defaultImageId);
			return;
		}
		imageMap.put(imageView, url);
		bitmap = getBitmapFromCache(url); // 获取缓存
		if (null != bitmap) {
			imageView.setImageBitmap(bitmap);
			setPareams(bitmap.getWidth(), bitmap.getHeight(), length,
					imageView, isWidth);
			imageView.setVisibility(View.VISIBLE);
		} else {
			if (isCache) { // 如果开启缓存本地情况 进行取本地数据
				bitmap = getLocalBitmap(url);
			}
			if (null != bitmap) {
				imageView.setImageBitmap(bitmap);
				setPareams(bitmap.getWidth(), bitmap.getHeight(), length,
						imageView, isWidth);
				imageView.setVisibility(View.VISIBLE);
				url = picName(url);
				cache.put(url, new SoftReference<Bitmap>(bitmap)); // 加入内存
			} else {
				imageView.setImageResource(defaultImageId);
				startThreadDownImgAndParenm(url, imageView, defaultImageId,
						isCache, length, isWidth); // 获取网络图片
			}
		}
	}

	/**
	 * 以bitmap的宽高比例设置ImageView宽高 ：目前不知道父布局是什么类型，所以暂时写了catch
	 * 
	 * @param bitmapWidth
	 *            传进来的宽度
	 * @param bitmapHeight
	 *            传进来的高度
	 * @param length
	 *            需要显示的最终长度
	 * @param imageView
	 *            传进来的ImageView
	 * @param isWidth
	 *            传进来的长度是否是宽
	 */
	public static void setPareams(int bitmapWidth, int bitmapHeight,
			int length, ImageView imageView, boolean isWidth) {

		float w;
		float h;

		if (isWidth) { // 传的宽。宽不变，得出缩放后的高
			w = length;
			h = (float) bitmapHeight / bitmapWidth * length;
		} else { // 传的高，得出缩放后的宽
			w = (float) bitmapWidth / bitmapHeight * length;
			h = length;
		}
		// LogUtil.i("bmp_w+bmp_h:" + bitmapWidth + "+" + bitmapHeight +
		// " boolen:" + isWidth + "  ==w@h" + w + "@" + h);
		try {
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					(int) w, (int) h);
			imageView.setLayoutParams(params);
		} catch (ClassCastException e) {
			try {
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
						(int) w, (int) h);
				imageView.setLayoutParams(params);
			} catch (ClassCastException e2) {
				FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
						(int) w, (int) h);
				imageView.setLayoutParams(params);

			}
		}
	}

	/**
	 * 获取程序内存Cache的缓存图片
	 * 
	 * @param url
	 *            缓存图片的url
	 * 
	 * @return Bitmap 缓存的图片
	 * 
	 * @Title: getBitmapFromCache
	 * @Description: TODO
	 */
	private Bitmap getBitmapFromCache(String url) {
		url = picName(url);
		if (cache.containsKey(url)) {
			return cache.get(url).get();
		}
		return null;
	}

	/**
	 * 根据url获取本地缓存的图片
	 * 
	 * @param url
	 *            缓存图片的url
	 * 
	 * @return Bitmap 缓存本地的图片
	 * 
	 * @Title: getLocalBitmap
	 * @Description: TODO
	 */
	private Bitmap getLocalBitmap(String url) {
		Bitmap map = null;
		url = picName(url);

		if (null == url || url.equals("null")) {
			return null;
		}
		// String localUrl = URLEncoder.encode(url);
		if (exist(url)) {
			map = BitmapFactory.decodeFile(DIR + "/" + url);
		}
		return map;
	}

	/**
	 * 开启线程获取网络图片
	 * 
	 * @param url
	 *            下载url
	 * @param imageView
	 *            显示图片的控件
	 * @param defaultImageId
	 *            默认图片的资源id
	 * @param isCache
	 *            是否缓存sd卡
	 * 
	 * @Title: startThread
	 * @Description: TODO
	 */
	private void startThreadDownImg(final String url,
			final ImageView imageView, final int defaultImageId,
			final boolean isCache) {
		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				String tag = imageMap.get(imageView);
				if (tag != null && tag.equals(url)) {
					if (null != msg.obj) {
						imageView.setImageBitmap((Bitmap) msg.obj);
						imageView.setVisibility(View.VISIBLE);
					} else {
						imageView.setImageResource(defaultImageId);
					}
				}
			}
		};
		pool.submit(new Runnable() {
			@Override
			public void run() {
				Bitmap bmp = downloadBitmap(url, isCache);
				Message message = Message.obtain();
				message.obj = bmp;
				handler.sendMessage(message);
			}
		});
	}

	private void startThreadDownImgAndParenm(final String url,
			final ImageView imageView, final int defaultImageId,
			final boolean isCache, final int length, final boolean isWidth) {
		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				String tag = imageMap.get(imageView);
				if (tag != null && tag.equals(url)) {
					if (null != msg.obj) {
						Bitmap bp = (Bitmap) msg.obj;
						imageView.setImageBitmap(bp);
						setPareams(bp.getWidth(), bp.getHeight(), length,
								imageView, isWidth);
						imageView.setVisibility(View.VISIBLE);
					} else {
						imageView.setImageResource(defaultImageId);
					}
				}
			}
		};
		pool.submit(new Runnable() {
			@Override
			public void run() {
				Bitmap bmp = downloadBitmap(url, isCache);
				Message message = Message.obtain();
				message.obj = bmp;
				handler.sendMessage(message);
			}
		});
	}

	/**
	 * 下载网络图片
	 * 
	 * @param url
	 *            图片url
	 * @param isCache
	 *            是否缓存到sd卡
	 * 
	 * @return Bitmap 下载的图片
	 * 
	 * @Title: downloadBitmap
	 * @Description: TODO
	 */
	private Bitmap downloadBitmap(String url, boolean isCache) {
		String downUrl = url;
		try {
			url = picName(url);
			if (url == null || url.equals("null"))
				return null;
			Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(
					downUrl).getContent());
			cache.put(url, new SoftReference<Bitmap>(bitmap));
			if (isCache) {
				saveImgToSd(bitmap, URLEncoder.encode(url));
			}
			return bitmap;
		} catch (IOException e) {
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * 保存图片到SD卡
	 * 
	 * @param bm
	 *            需要保存的图片
	 * @param url
	 *            图片url
	 * 
	 * @Title: saveImgToSd
	 * @Description: TODO
	 */
	private static void saveImgToSd(Bitmap bm, String url) {
		if (null == url || url.equals("null"))
			return;
		if (null == bm) {
			return;
		}
		// 判断sdcard上的空间
		if (FREE_SD_SPACE_NEEDED_TO_CACHE > freeSpaceOnSd()) {
			return;
		}
		if (!Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState()))
			return;
		String filename = url;
		// 目录不存在就创建
		File dirPath = new File(DIR);
		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}

		File file = new File(DIR + "/" + filename);
		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			OutputStream outStream = new FileOutputStream(file);
			String picType = url.substring(url.lastIndexOf("_") + 1)
					.toLowerCase();
			if (picType.equals("png")) {
				bm.compress(Bitmap.CompressFormat.PNG, 80, outStream);
			} else {
				bm.compress(Bitmap.CompressFormat.JPEG, 80, outStream);
			}
			outStream.flush();
			outStream.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}

	/**
	 * 计算sdcard上的剩余空间
	 * 
	 * @return int 剩余空间大小
	 * 
	 * @Title: freeSpaceOnSd
	 * @Description: TODO
	 */
	private static int freeSpaceOnSd() {
		StatFs stat = new StatFs(Environment.getExternalStorageDirectory()
				.getPath());
		double sdFreeMB = ((double) stat.getAvailableBlocks() * (double) stat
				.getBlockSize()) / MB;

		return (int) sdFreeMB;
	}

	/**
	 * 判断本地是否有该url对应图片
	 * 
	 * @param url
	 *            图片url
	 * 
	 * @return <li>true:存在</li><li>false:不存在</li>
	 * 
	 * @Title: Exist
	 * @Description: TODO
	 */
	private boolean exist(String url) {
		File file = new File(DIR + "/" + url);
		return file.exists();
	}

	/**
	 * 格式化图片名称
	 * 
	 * @param url
	 *            图片url
	 * 
	 * @return String 格式化后的string
	 * 
	 * @Title: picName
	 * @Description: TODO
	 */
	private String picName(String url) {
		url = URLEncoder.encode(url).hashCode() + "";
		return url;
	}

}
