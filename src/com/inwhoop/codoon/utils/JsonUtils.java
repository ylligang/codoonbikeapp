package com.inwhoop.codoon.utils;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.os.Build;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.entity.BikeAllData;
import com.inwhoop.codoon.entity.BikeData;
import com.inwhoop.codoon.entity.BikeDataBean;
import com.inwhoop.codoon.entity.BikeSettingBean;
import com.inwhoop.codoon.entity.BikeSettingEntity;
import com.inwhoop.codoon.entity.EquipmentInfoBean;
import com.inwhoop.codoon.entity.MoreData;
import com.inwhoop.codoon.entity.RidingDataBean;
import com.inwhoop.codoon.entity.RidingEntity;
import com.inwhoop.codoon.entity.ThirdLogin;
import com.inwhoop.codoon.entity.User;
import com.inwhoop.codoon.entity.UserToken;

@TargetApi(19)
public class JsonUtils {

	public static User parseUser(String json) {
		User user = null;
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		user = gson.fromJson(json, User.class);
		return user;
	}

	public static String entityToJson(Object object) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		return gson.toJson(object);
	}

	/**
	 * 登录
	 * 
	 * @Title: parseLogin
	 * @Description: TODO
	 * @param email
	 * @param password
	 * @return UserToken
	 */
	public static UserToken parseLogin(String email, String password) {
		SyncHttp syncHttp = new SyncHttp();
		UserToken userToken = null;
		String result = "";
		try {
			String params = "email=" + URLEncoder.encode(email, "utf-8")
					+ "&password=" + URLEncoder.encode(password, "utf-8")
					+ "&grant_type=password" + "&client_id="
					+ MyApplication.HTTP_CLIENT_KEY + "&scope=user";
			result = syncHttp.httpGet(MyApplication.HTTP_TOKEN_URL, params, "");
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			userToken = gson.fromJson(result, UserToken.class);
		} catch (Exception e) {
			System.out.println("Exception== " + e.toString());
			e.printStackTrace();
		}
		return userToken;
	}

	public static UserToken parseThridLogin(ThirdLogin thirdLogin,
			String mAccessToken) {
		String result = "";
		UserToken userToken = null;
		SyncHttp syncHttp = new SyncHttp();
		// System.out.println("json== "+JsonUtils.entityToJson(thirdLogin));
		// result = syncHttp.post(MyApplication.HTTP_OTHER_LOGIN,
		// JsonUtils.entityToJson(thirdLogin),mAccessToken);
		try {
			String params = "token="
					+ URLEncoder.encode(thirdLogin.getToken(), "utf-8")
					+ "&secret="
					+ URLEncoder.encode(thirdLogin.getSecret(), "utf-8")
					+ "&external_user_id="
					+ URLEncoder.encode(thirdLogin.getExternal_user_id(),
							"utf-8") + "&source="
					+ URLEncoder.encode(thirdLogin.getSource(), "utf-8")
					+ "&expire_in=" + thirdLogin.getExpire_in() + "&catalog="
					+ URLEncoder.encode(thirdLogin.getCatalog(), "utf-8")
					+ "&client_id=" + MyApplication.HTTP_CLIENT_KEY
					+ "&device_token=";

			result = syncHttp.httpGet(MyApplication.HTTP_OTHER_LOGIN, params,
					null);
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			userToken = gson.fromJson(result, UserToken.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userToken;
	}

	/**
	 * 自行车设置上传
	 * 
	 * @Title: uploadGirth
	 * @Description: TODO
	 * @param @param entity
	 * @param @return
	 * @return String
	 */
	public static String uploadGirth(BikeSettingEntity entity,
			String mAccessToken) {
		String result = "";
		SyncHttp syncHttp = new SyncHttp();
		result = syncHttp.post(MyApplication.HTTP_RIDEBLE_BIKESETTINGUPLOAD,
				JsonUtils.entityToJson(entity), mAccessToken);
		return result;
	}

	/**
	 * 数据同步
	 * 
	 * @Title: uploadData
	 * @Description: TODO
	 * @param @param entity
	 * @param @return
	 * @return String
	 */
	public static String uploadData(String json, String mAccessToken) {
		String result = "";
		SyncHttp syncHttp = new SyncHttp();
		result = syncHttp.post(MyApplication.HTTP_RIDEBLE_DATAUPLOAD, json,
				mAccessToken);
		return result;
	}

	/**
	 * 获取更多骑行数据
	 * 
	 * @Title: getBikeData
	 * @Description: TODO
	 * @param start_time
	 * @param count
	 * @param mAccessToken
	 * @return List<BikeData>
	 * @throws Exception
	 */
	public static List<BikeData> getBikeData(String start_time, int count,
			String mAccessToken) throws Exception {
		List<BikeData> bikeDatas = new ArrayList<BikeData>();
		MoreData moreData = new MoreData();
		BikeData bikeData = new BikeData();
		BikeDataBean bikeDataBean = new BikeDataBean();
		bikeDataBean.setStart_time(start_time);
		bikeDataBean.setCount(count);
		moreData.setInfo(bikeDataBean);
		SyncHttp syncHttp = new SyncHttp();
		String result = syncHttp.post(MyApplication.HTTP_RIDEBLE_MOREPERIOD,
				JsonUtils.entityToJson(moreData), mAccessToken);
//		 System.out.println("==========resultresult==========="+result);
		try {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONObject jsonObject = new JSONObject(result);
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jObject = jsonArray.getJSONObject(i);
				bikeData = gson.fromJson(jObject.toString(), BikeData.class);
				bikeDatas.add(bikeData);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bikeDatas;
	}

	public static BikeAllData parseHistoryData(String userId,
			String mAccessToken) {
//		96714493-007d-48a1-bfeb-33abf236e996
//		c1ef814f3d51fc6f8812ab1604ead685
		BikeAllData bikeData = null;
		SyncHttp syncHttp = new SyncHttp();
		String result = "";
		try {
			result = syncHttp.httpGet(MyApplication.HTTP_RIDEBLE_HISTORY,
					"user_id=" + userId, mAccessToken);
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONObject jsonObject = new JSONObject(result)
					.optJSONObject("data");
//	2f234341-272c-4599-9f1f-e8159de52669
//	5b22fb44322c1b208888763dbd3a480a
			// JSONArray jsonArray = new JSONArray(jsonObject.get("periods"));
			// for (int i = 0; i < jsonArray.length(); i++) {
			// JSONObject jObject = jsonArray.getJSONObject(i);
			bikeData = gson.fromJson(jsonObject.toString(), BikeAllData.class);
			// }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bikeData;
	}

	public static String getData() {
		String info = "";
		SyncHttp sHttp = new SyncHttp();
		try {
			String result = sHttp.httpGet(
					"http://yxg.mxcod.com/apicheckxinban.php", "", null);
			JSONObject jsonObject = new JSONObject(result);
			info = jsonObject.getString("type");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return info;

	}

}
