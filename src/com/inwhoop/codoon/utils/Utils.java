package com.inwhoop.codoon.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.weibo.TencentWeibo;

import com.inwhoop.codoon.R;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.entity.EquipmentInfoBean;
import com.inwhoop.codoon.onekeyshare.OnekeyShare;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class Utils {

	/**
	 * 判断email格式是否正确
	 * 
	 * @Title: isEmail
	 * @Description: TODO
	 * @param @param email
	 * @param @return
	 * @return boolean
	 */
	public static boolean isEmail(String email) {
		String str = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(email);

		return m.matches();
	}

	/**
	 * 判断字符串是否为空
	 * 
	 * @Title: isNull
	 * @Description: TODO
	 * @param @param str
	 * @param @return
	 * @return boolean
	 */
	public static boolean isNull(String str) {
		boolean flag = false;
		if (null == str || str.trim().equals("")
				|| str.trim().equalsIgnoreCase("null")) {
			flag = true;
		}
		return flag;
	}

	/**
	 * 保存String数据到sp
	 * 
	 * @Title: savePreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @param value 值
	 * @return void
	 */
	public static void savePreference(Context context, String key, String value) {
		// PreferenceManager.getDefaultSharedPreferences(context).edit()
		// .putString(key, value).commit();
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP,
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}

	/**
	 * 保存int数据到sp
	 * 
	 * @Title: savePreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @param value 值
	 * @return void
	 */
	public static void saveIntPreference(Context context, String key, int value) {
		// PreferenceManager.getDefaultSharedPreferences(context).edit()
		// .putString(key, value).commit();
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP,
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public static void saveFloatPreference(Context context, String key,
			float value) {
		// PreferenceManager.getDefaultSharedPreferences(context).edit()
		// .putString(key, value).commit();
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP,
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putFloat(key, value);
		editor.commit();
	}

	public static float getFloatpreference(Context context, String key,
			float defaultValue) {
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP,
				Context.MODE_PRIVATE);
		return sp.getFloat(key, defaultValue);
		// return PreferenceManager.getDefaultSharedPreferences(context)
		// .getString(key, "");
	}

	/**
	 * 保存String数据到sp
	 * 
	 * @Title: savePreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @param value 值
	 * @return void
	 */
	public static void savePreferenceData(Context context, String key,
			String value) {
		// PreferenceManager.getDefaultSharedPreferences(context).edit()
		// .putString(key, value).commit();
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP1,
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}

	/**
	 * 保存int数据到sp
	 * 
	 * @Title: savePreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @param value 值
	 * @return void
	 */
	public static void saveIntPreferenceData(Context context, String key,
			int value) {
		// PreferenceManager.getDefaultSharedPreferences(context).edit()
		// .putString(key, value).commit();
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP1,
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	/**
	 * 从sp获取数据
	 * 
	 * @Title: getpreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @return
	 * @return String 返回的值
	 */
	public static String getpreferenceData(Context context, String key) {
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP1,
				Context.MODE_PRIVATE);
		return sp.getString(key, "");
		// return PreferenceManager.getDefaultSharedPreferences(context)
		// .getString(key, "");
	}

	/**
	 * 从sp获取数据
	 * 
	 * @Title: getpreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @return
	 * @return String 返回的值
	 */
	public static int getIntpreferenceData(Context context, String key,
			int defaultValue) {
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP1,
				Context.MODE_PRIVATE);
		return sp.getInt(key, defaultValue);
		// return PreferenceManager.getDefaultSharedPreferences(context)
		// .getString(key, "");
	}

	/**
	 * 从sp获取数据
	 * 
	 * @Title: getpreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @return
	 * @return String 返回的值
	 */
	public static String getpreference(Context context, String key) {
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP,
				Context.MODE_PRIVATE);
		return sp.getString(key, "");
		// return PreferenceManager.getDefaultSharedPreferences(context)
		// .getString(key, "");
	}

	/**
	 * 从sp获取数据
	 * 
	 * @Title: getpreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @return
	 * @return String 返回的值
	 */
	public static int getIntpreference(Context context, String key,
			int defaultValue) {
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP,
				Context.MODE_PRIVATE);
		return sp.getInt(key, defaultValue);
		// return PreferenceManager.getDefaultSharedPreferences(context)
		// .getString(key, "");
	}

	public static void saveBooleanPreference(Context context, String key,
			boolean isbool) {
		// PreferenceManager.getDefaultSharedPreferences(context).edit()
		// .putString(key, value).commit();
		SharedPreferences sp = context.getSharedPreferences(
				MyApplication.SET_SP, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putBoolean(key, isbool);
		editor.commit();
	}

	public static boolean getBooleanpreference(Context context, String key) {
		SharedPreferences sp = context.getSharedPreferences(
				MyApplication.SET_SP, Context.MODE_PRIVATE);
		boolean isbool = false;
		if (null != sp) {
			isbool = sp.getBoolean(key, true);
		}
		return isbool;
	}

	public static void saveBooleanStartPreference(Context context, String key,
			boolean isbool) {
		// PreferenceManager.getDefaultSharedPreferences(context).edit()
		// .putString(key, value).commit();
		SharedPreferences sp = context.getSharedPreferences(
				MyApplication.START_TIME, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putBoolean(key, isbool);
		editor.commit();
	}

	public static boolean getBooleanStartpreference(Context context, String key) {
		SharedPreferences sp = context.getSharedPreferences(
				MyApplication.START_TIME, Context.MODE_PRIVATE);
		boolean isbool = false;
		if (null != sp) {
			isbool = sp.getBoolean(key, false);
		}
		return isbool;
	}

	public static void saveBooleanStopPreference(Context context, String key,
			boolean isbool) {
		// PreferenceManager.getDefaultSharedPreferences(context).edit()
		// .putString(key, value).commit();
		SharedPreferences sp = context.getSharedPreferences(
				MyApplication.STOP_TIME, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putBoolean(key, isbool);
		editor.commit();
	}

	public static boolean getBooleanStoppreference(Context context, String key) {
		SharedPreferences sp = context.getSharedPreferences(
				MyApplication.STOP_TIME, Context.MODE_PRIVATE);
		boolean isbool = false;
		if (null != sp) {
			isbool = sp.getBoolean(key, false);
		}
		return isbool;
	}

	/**
	 * 保存对象到sp
	 * 
	 * @Title: saveObject
	 * @Description: TODO
	 * @param context
	 * @param object
	 *            void
	 */
	public static void saveObject(Context context, String sp_name, Object object) {
		SharedPreferences preferences = context.getSharedPreferences(sp_name,
				Context.MODE_PRIVATE);
		// 创建字节输出流
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			// 创建对象输出流，并封装字节流
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			// 将对象写入字节流
			oos.writeObject(object);
			// 将字节流编码成base64的字符窜
			String obString = new String(
					org.apache.commons.codec.binary.Base64.encodeBase64(baos
							.toByteArray()));
			Editor editor = preferences.edit();
			editor.putString(MyApplication.USER_INFO, obString);
			editor.commit();
		} catch (IOException e) {
			System.out.println("e== " + e.toString());
		}
	}

	/**
	 * 解析对象
	 * 
	 * @Title: readObject
	 * @Description: TODO
	 * @param context
	 * @return Object
	 */
	public static Object readObject(Context context, String sp_name) {
		Object object = null;
		SharedPreferences preferences = context.getSharedPreferences(sp_name,
				Context.MODE_PRIVATE);
		String productBase64 = preferences.getString(MyApplication.USER_INFO,
				"");
		// 读取字节
		byte[] base64 = org.apache.commons.codec.binary.Base64
				.decodeBase64(productBase64.getBytes());
		// 封装到字节流
		ByteArrayInputStream bais = new ByteArrayInputStream(base64);
		try {
			// 再次封装
			ObjectInputStream bis = new ObjectInputStream(bais);
			try {
				// 读取对象
				object = bis.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (StreamCorruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return object;
	}

	/**
	 * 拼接语音播报
	 * 
	 * @Title: getMediaPath
	 * @Description: TODO
	 * @param data
	 * @param rawId
	 * @return List<Integer>
	 */
	public static ArrayList<Integer> getMediaPath(String data, int s_rawId,
			int e_rawId) {
		ArrayList<Integer> mediaPaths = new ArrayList<Integer>();
		if (s_rawId != -1) {
			mediaPaths.add(s_rawId);
		}
		if (data.length() == 1) {// 一位数
			mediaPaths.add(R.raw.num0 + Integer.valueOf(data));
		} else if (data.length() == 2) {// 两位数
			int num1 = Integer.valueOf(data.substring(0, 1));
			int num2 = Integer.valueOf(data.substring(1, 2));
			if (num1 > 0) {
				mediaPaths.add(R.raw.num0 + num1);
				mediaPaths.add(R.raw.shi);
			}
			if (num2 > 0) {
				mediaPaths.add(R.raw.num0 + num2);
			}
		} else if (data.length() == 3) {// 三位数
			int num1 = Integer.valueOf(data.substring(0, 1));
			int num2 = Integer.valueOf(data.substring(1, 2));
			int num3 = Integer.valueOf(data.substring(2, 3));
			if (num1 > 0) {
				mediaPaths.add(R.raw.num0 + num1);
				mediaPaths.add(R.raw.bai);
			}
			if (num2 > 0) {
				mediaPaths.add(R.raw.num0 + num2);
				mediaPaths.add(R.raw.shi);
			}
			if (num3 > 0) {
				mediaPaths.add(R.raw.num0 + num3);
			}
		} else if (data.length() == 4) {// 四位数
			int num1 = Integer.valueOf(data.substring(0, 1));
			int num2 = Integer.valueOf(data.substring(1, 2));
			int num3 = Integer.valueOf(data.substring(2, 3));
			int num4 = Integer.valueOf(data.substring(3, 4));
			if (num1 > 0) {
				mediaPaths.add(R.raw.num0 + num1);
				mediaPaths.add(R.raw.qian);
			}
			if (num2 > 0) {
				mediaPaths.add(R.raw.num0 + num2);
				mediaPaths.add(R.raw.bai);
			}
			if (num3 > 0) {
				mediaPaths.add(R.raw.num0 + num3);
				mediaPaths.add(R.raw.shi);
			}
			if (num4 > 0) {
				mediaPaths.add(R.raw.num0 + num4);
			}
		} else if (data.length() == 5) {// 五位数
			int num1 = Integer.valueOf(data.substring(0, 1));
			int num2 = Integer.valueOf(data.substring(1, 2));
			int num3 = Integer.valueOf(data.substring(2, 3));
			int num4 = Integer.valueOf(data.substring(3, 4));
			int num5 = Integer.valueOf(data.substring(4, 5));
			if (num1 > 0) {
				mediaPaths.add(R.raw.num0 + num1);
				mediaPaths.add(R.raw.wan);
			}
			if (num2 > 0) {
				mediaPaths.add(R.raw.num0 + num2);
				mediaPaths.add(R.raw.qian);
			}
			if (num3 > 0) {
				mediaPaths.add(R.raw.num0 + num3);
				mediaPaths.add(R.raw.bai);
			}
			if (num3 > 0) {
				mediaPaths.add(R.raw.num0 + num4);
				mediaPaths.add(R.raw.shi);
			}
			if (num5 > 0) {
				mediaPaths.add(R.raw.num0 + num5);
			}
		}
		mediaPaths.add(e_rawId);
		return mediaPaths;
	}

	public static EquipmentInfoBean getEquipmentinfo(Context context) {
		EquipmentInfoBean info = new EquipmentInfoBean();
		info.setProduct_id(Utils.getpreference(context, MyApplication.DEV_ID));
		info.setHard_version(Utils
				.getpreference(context, MyApplication.VERSION));
		info.setSoft_version("1.0");
		return info;
	}

	// 将时间戳转为字符串
	public static String getStrTime(long cc_time) {
		// String re_StrTime = null;
		//
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// // 例如：cc_time=1291778220
		// // long lcc_time = Long.valueOf(cc_time);
		// re_StrTime = sdf.format(new Date(cc_time * 1000L));
		//
		// return re_StrTime;

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
		Date curDate = new Date(cc_time);
		return formatter.format(curDate);

	}

	/**
	 * 
	 * @param begin
	 *            开始时间
	 * @param end
	 *            当前时间
	 * @param dateFormate
	 *            时间格式
	 * @return 两个时间点之间的差，单位小时
	 */
	public static String compareTime(String begin, String end) {
		long hours = 0;
		long min = 0;
		SimpleDateFormat dateFormate = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		try {
			Date date1 = dateFormate.parse(begin);
			Date date2 = dateFormate.parse(end);
			long diff = date2.getTime() - date1.getTime();
			min = diff / (1000 * 60); // 分钟
			hours = diff / (1000 * 60 * 60);// 小时
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return hours + "h" + min + "'";
	}

	// 使用快捷分享完成分享（请务必仔细阅读位于SDK解压目录下Docs文件夹中OnekeyShare类的JavaDoc）
	public static void showShare(Context context, String Content, String imgpath) {
		final OnekeyShare oks = new OnekeyShare();
		// 分享时 Notification 的图标和文字
		oks.setNotification(R.drawable.ic_launcher,
				context.getString(R.string.app_name));
		// address 是接收人地址，仅在信息和邮件使用
		oks.setAddress("");
		// title 标题，印象笔记、邮箱、信息、微信、人人网和 QQ 空间使用
		oks.setTitle(Content);
		// titleUrl 是标题的网络链接，仅在人人网和 QQ 空间使用
		// oks.setTitleUrl(url);
		// text 是分享文本，所有平台都需要这个字段
		oks.setText(Content);

		// imagePath 是图片的本地路径，Linked- In 以外的平台都支持此参数
		oks.setImagePath(imgpath);
		// imageUrl 是图片的网络路径，新浪微博、人人网、QQ 空间、
		// 微信、易信、Linked-In 支持此字段
		// oks.setImageUrl(imgpath);
		// // 微信、易信中使用，表示视屏地址或网页地址
		// oks.setUrl(url);
		// comment 是我对这条分享的评论，仅在人人网和 QQ 空间使用
		oks.setComment(context.getString(R.string.app_name));
		// site 是分享此内容的网站名称，仅在 QQ 空间使用
		oks.setSite(context.getString(R.string.app_name));
		// siteUrl 是分享此内容的网站地址，仅在 QQ 空间使用
		// oks.setSiteUrl("http://www.inwhoop.com");
		// oks.setVenueName("Share SDK");
		// oks.setVenueDescription("This is a beautiful place!");
		// oks.setLatitude(23.056081f);
		// oks.setLongitude(113.385708f);
		// 是否直接分享（true 则直接分享）
		oks.setSilent(false);

		// 去除注释，可令编辑页面显示为Dialog模式
		// oks.setDialogMode();

		// 去除注释，则快捷分享的操作结果将通过OneKeyShareCallback回调
		// oks.setCallback(new OneKeyShareCallback());
		// oks.setShareContentCustomizeCallback(new
		// ShareContentCustomizeDemo());

		// 去除注释，演示在九宫格设置自定义的图标
		// Bitmap logo = BitmapFactory.decodeResource(menu.getResources(),
		// R.drawable.ic_launcher);
		// String label = menu.getResources().getString(R.string.app_name);
		// OnClickListener listener = new OnClickListener() {
		// public void onClick(View v) {
		// String text = "Customer Logo -- Share SDK " +
		// ShareSDK.getSDKVersionName();
		// Toast.makeText(menu.getContext(), text, Toast.LENGTH_SHORT).show();
		// oks.finish();
		// }
		// };
		// oks.setCustomerLogo(logo, label, listener);

		oks.show(context);
//		Platform sina = ShareSDK.getPlatform(context, SinaWeibo.NAME);
//		Platform tencent = ShareSDK.getPlatform(context, TencentWeibo.NAME);
//		if (sina.isValid()) {
//			sina.removeAccount();
//		}
//		if (tencent.isValid()) {
//			tencent.removeAccount();
//		}
	}

	public static void clear(Context context, String name) {
		SharedPreferences sp = context.getSharedPreferences(name,
				Context.MODE_PRIVATE);
		sp.edit().clear().commit();

	}

	/**
	 * 获取sd路径
	 * 
	 * @Title: getSDCardPath
	 * @Description: TODO
	 * @return String
	 */
	public static String getSDCardPath() {
		File sdcardDir = null;
		boolean sdcardExist = Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
		if (sdcardExist) {
			sdcardDir = Environment.getExternalStorageDirectory();
		}
		return sdcardDir.toString();
	}

	public static String getYear(String date) {
		String year = "";
		if (null != date && date.contains("-")) {
			int end = date.lastIndexOf("-");
			year = date.substring(0, end + 3);
		}
		return year;
	}

	public static String getHour(String startTime, String endTime) {
		String mon1 = "";
		String mon2 = "";
		if (null != startTime && startTime.contains("-")
				&& startTime.contains(":")) {
			int start1 = startTime.lastIndexOf("-");
			int end1 = startTime.lastIndexOf(":");
			mon1 = startTime.substring(start1 + 3, end1).trim();
		}

		if (null != endTime && endTime.contains("-") && endTime.contains(":")) {
			int start = endTime.lastIndexOf("-");
			int end = endTime.lastIndexOf(":");

			mon2 = endTime.substring(start + 3, end).trim();
		}
		return mon1 + "-" + mon2;
	}

	public static String getTime(String startTime, String endTime) {
		long min = 0;
		long hour = 0;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		java.util.Date now;
		try {
			now = df.parse(startTime);
			java.util.Date date = df.parse(endTime);
			long l = date.getTime() - now.getTime();
			long day = l / (24 * 60 * 60 * 1000);
			hour = (l / (60 * 60 * 1000) - day * 24);
			min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
			// System.out.println("endTime-startTime== "+startTime);
			// System.out.println("endTime== "+endTime);
			// System.out.println("endTime-min== "+min);
			// long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min *
			// 60);
			// System.out.println("" + day + "天" + hour + "小时" + min + "分" + s
			// + "秒");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String time = "";
		if (min < 10) {
			time = "0" + hour + "h0" + min + "'";
		} else if (min < 60) {
			time = "0" + hour + "h" + min + "'";
		}

		return time;

	}

	/**
	 * 获取和保存当前屏幕的截图
	 */
	public static void GetandSaveCurrentImage(Activity act) {
		// 1.构建Bitmap
		try {
			WindowManager windowManager = act.getWindowManager();
			Display display = windowManager.getDefaultDisplay();
			int w = display.getWidth();
			int h = display.getHeight();

			Bitmap Bmp = Bitmap.createBitmap(w, h, Config.ARGB_8888);

			// 2.获取屏幕
			View decorview = act.getWindow().getDecorView();
			decorview.setDrawingCacheEnabled(true);
			Bmp = decorview.getDrawingCache();

			String SavePath = getSDCardPath() + "/codoon/ScreenImage";

			// 3.保存Bitmap

			File path = new File(SavePath);
			// 文件
			String filepath = SavePath + "/Screen_1.png";
			File file = new File(filepath);
			if (!path.exists()) {
				path.mkdirs();
			}
			if (!file.exists()) {
				file.createNewFile();
			}

			FileOutputStream fos = null;
			fos = new FileOutputStream(file);
			if (null != fos) {
				Bmp.compress(Bitmap.CompressFormat.PNG, 90, fos);
				fos.flush();
				fos.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			// TODO: handle exception
		}
	}

	/**
	 * 保存String数据到sp
	 * 
	 * @Title: savePreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @param value 值
	 * @return void
	 */
	public static void savePreferencset(Context context, String key,
			String value) {
		// PreferenceManager.getDefaultSharedPreferences(context).edit()
		// .putString(key, value).commit();
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP2,
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}

	/**
	 * 保存int数据到sp
	 * 
	 * @Title: savePreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @param value 值
	 * @return void
	 */
	public static void saveIntPreferenceset(Context context, String key,
			int value) {
		// PreferenceManager.getDefaultSharedPreferences(context).edit()
		// .putString(key, value).commit();
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP2,
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	/**
	 * 从sp获取数据
	 * 
	 * @Title: getpreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @return
	 * @return String 返回的值
	 */
	public static String getpreferenceset(Context context, String key) {
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP2,
				Context.MODE_PRIVATE);
		return sp.getString(key, "");
		// return PreferenceManager.getDefaultSharedPreferences(context)
		// .getString(key, "");
	}

	/**
	 * 从sp获取数据
	 * 
	 * @Title: getpreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @return
	 * @return String 返回的值
	 */
	public static int getIntpreferenceset(Context context, String key,
			int defaultValue) {
		SharedPreferences sp = context.getSharedPreferences(MyApplication.SP2,
				Context.MODE_PRIVATE);
		return sp.getInt(key, defaultValue);
		// return PreferenceManager.getDefaultSharedPreferences(context)
		// .getString(key, "");
	}

	/**
	 * 判断服务是否运行
	 * 
	 * @Title: isWorked
	 * @Description: TODO
	 * @param context
	 * @param name
	 * @return boolean
	 */
	public static boolean isServiceWorked(Context context, String name) {
		ActivityManager myManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		ArrayList<RunningServiceInfo> runningService = (ArrayList<RunningServiceInfo>) myManager
				.getRunningServices(30);
		for (int i = 0; i < runningService.size(); i++) {
			if (runningService.get(i).service.getClassName().toString()
					.equals(name)) {
				return true;
			}
		}
		return false;
	}

	// /**
	// * sdk版本检测
	// *
	// * @Title: getSDKVersionNumber
	// * @Description: TODO
	// * @return int
	// */
	// public static int getSDKVersionNumber() {
	//
	// int sdkVersion;
	// try {
	// sdkVersion = Integer.valueOf(android.os.Build.VERSION.SDK);
	//
	// } catch (NumberFormatException e) {
	//
	// sdkVersion = 0;
	// }
	// return sdkVersion;
	// }
}
