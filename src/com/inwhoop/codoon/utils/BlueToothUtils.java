package com.inwhoop.codoon.utils;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Set;

import org.json.JSONArray;

import com.communication.ble.BleSyncManager;
import com.communication.ble.DisconveryManager;
import com.communication.data.CODMBCommandData;
import com.communication.data.ISyncDataCallback;
import com.inwhoop.codoon.app.MyApplication;
import com.inwhoop.codoon.service.BlueRealTimeService;

import android.app.AlertDialog.Builder;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class BlueToothUtils {
	// private static BleSyncManager mSyncDeviceDataManager;
	public static DisconveryManager mDisconveryManager;
	// private static BluetoothDevice mBlueDevice;
	public final static int GET_NEW_DEVICE = 88;
	public final static int CHANGE_COMMAND_STATE = 1;
	private final int COMMAND_RESULT = 2;
	private final int GET_REAL_TIME = 3;
	private Handler handler = null;

	// public static boolean getDevice() {
	// BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
	// boolean isenable = adapter.isEnabled();
	// if (isenable) {
	// Set<BluetoothDevice> devices = adapter.getBondedDevices();
	// for (int i = 0; i < devices.size(); i++) {
	// BluetoothDevice device = (BluetoothDevice) devices.iterator()
	// .next();
	// System.out.println("device.getName()= " + device.getName());
	// if (null != device && device.getName().equals("COD_MB")) {
	// return true;
	// }
	// }
	// }
	// return false;
	// }

	public BlueToothUtils(Handler handler) {
		this.handler = handler;
	}

	public void setNull() {
		handler = null;
	}

	/**
	 * 搜索蓝牙
	 * 
	 * @Title: scanDevice
	 * @Description: TODO
	 * @param mContext
	 * @param mDisconveryManager
	 * @param handler
	 *            void
	 */
	public void scanDevice(Context mContext) {
	
		BluetoothAdapter.LeScanCallback scanCallback = new BluetoothAdapter.LeScanCallback() {

			@Override
			public void onLeScan(BluetoothDevice device, int arg1, byte[] arg2) {
				if (null != device && device.getName().equals("COD_MB")) {
					Message message = handler.obtainMessage();
					message.obj = device;
					message.what = GET_NEW_DEVICE;
					handler.sendMessage(message);
				}

			}
		};

		if (null == mDisconveryManager) {
			mDisconveryManager = new DisconveryManager(mContext, scanCallback);
		} else {
			mDisconveryManager.startSearch();
		}
	}

	public void conectDevice(final Context context) {
		ISyncDataCallback syncDataCallback = new ISyncDataCallback() {

			@Override
			public void onNullData() {
				// Log.d(TAG, "onNullData()");
			}

			@Override
			public void onConnectSuccessed() {
				// TODO Auto-generated method stub
				// Log.d(TAG, "onConnectSuccessed()");
				// // send some command after connectSuccessed, such as
				// getVersion
				handler.sendEmptyMessage(CHANGE_COMMAND_STATE);
				// if (null != context) {
				// Intent intentData = new Intent(context,
				// BlueRealTimeService.class);
				// context.startService(intentData);
				// }

			}

			@Override
			public void onGetVersion(String version) {
				// System.out.println("1onConnectSuccessed");
				// Log.d(TAG, "onGetVersion():" + version);
				if (null != context)
					Utils.savePreference(context, MyApplication.VERSION,
							version);
			}

			@Override
			public void onGetDeviceID(String deviceID) {
				// // TODO Auto-generated method stub
				// Log.d(TAG, "onGetDeviceID():" + deviceID);
				MyApplication.mSyncDeviceDataManager
						.SendDataToDevice(CODMBCommandData
								.getPostDeviceTypeVersion());
				if (null != context)
					Utils.savePreference(context, MyApplication.DEV_ID,
							deviceID);

			}

			@Override
			public void onUpdateTimeSuccessed() {
				// TODO Auto-generated method stub
				// System.out.println("3onConnectSuccessed");
				// Log.d(TAG, "onUpdateTimeSuccessed()");
			}

			@Override
			public void onUpdateAlarmReminderSuccessed() {
				// TODO Auto-generated method stub
				// System.out.println("4onConnectSuccessed");
				// Log.d(TAG, "onUpdateAlarmReminderSuccessed()");
			}

			@Override
			public void onBattery(int battery) {
				// TODO Auto-generated method stub
				// System.out.println("5onConnectSuccessed");
				// Log.d(TAG, "onBattery():" + battery);
			}

			@Override
			public void onClearDataSuccessed() {
				// System.out.println("6onConnectSuccessed");
				// TODO Auto-generated method stub
				// Log.d(TAG, "onClearDataSuccessed()");
			}

			@Override
			public void onGetDeviceTime(String time) {
				// TODO Auto-generated method stub
				// System.out.println("7onConnectSuccessed");
				// Log.d(TAG, "onGetDeviceTime():" + time);
			}

			@Override
			public void onSyncDataProgress(int progress) {
				// TODO Auto-generated method stub
				// System.out.println("8onConnectSuccessed");
				// Log.d(TAG, "onSyncDataProgress():" + progress);

			}

			@Override
			public void onUpdateUserinfoSuccessed() {
				// System.out.println("9onConnectSuccessed");
				// TODO Auto-generated method stub
				// Log.d(TAG, "onUpdateUserinfoSuccessed()");
			}

			@Override
			public void onGetUserInfo(int height, int weigh, int age,
					int gender, int stepLength, int runLength, int sportType,
					int goalValue) {
				// TODO Auto-generated method stub
				// System.out.println("10onConnectSuccessed");
				// Log.d(TAG, "onGetUserInfo():goalValue=" + goalValue);
			}

			@Override
			public void onGetOtherDatas(ArrayList<Integer> datas) {
				// TODO Auto-generated method stub
				// System.out.println("11onConnectSuccessed");
				// Log.d(TAG, "onGetOtherDatas():" + datas);
				// handler.sendEmptyMessageDelayed(GET_REAL_TIME, 1000);
			}

			@Override
			public void onTimeOut() {
				// TODO Auto-generated method stub
				// System.out.println("12onConnectSuccessed");
				// Log.d(TAG, "onTimeOut()");
			}

			@Override
			public void onSyncDataOver(JSONArray jsonObject,
					ByteArrayOutputStream baos) {
				// TODO Auto-generated method stub
				// System.out.println("13onConnectSuccessed");
				byte[] datas = baos.toByteArray();
				// Log.d("syncdata", " ================baos len =" +
				// datas.length);
				StringBuffer sBuffer = new StringBuffer();
				for (int i = 0; i < datas.length; i++) {
					if ((i + 1) % 6 == 0) {
						sBuffer.append("\n");
					}
					Log.d("writeData",
							"datas i =" + Integer.toHexString(datas[i])
									+ "  byte i=" + Byte.toString(datas[i]));
					sBuffer.append(Integer.toHexString(datas[i] & 0xFF) + "   ");
				}
				// showCommandResult(" dataframe json=\n" + sBuffer.toString());

			}

			@Override
			public void onBindSucess() {
				// TODO Auto-generated method stub
			}

			@Override
			public void onSyncDataOver(long[] data, ByteArrayOutputStream baos) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTimeOut(BluetoothDevice device) {
			}

			@Override
			public void onGetDeviceRealtimeData(ArrayList<Integer> datas) {
				// TODO Auto-generated method stub
				int[] result = new int[3];
				if (datas.size() > 9) {
					result[0] = (int) ((datas.get(3) * 256 + datas.get(4)) / 10.0f);
					result[1] = (int) ((datas.get(5) * 256 + datas.get(6)) / 10.0f);
					result[2] = datas.get(7) * 256 + datas.get(8);
				}
				// showCommandResult("onGetDeviceRealtimeData speed:" +
				// result[0]
				// + "  踏平：" + result[1] + "  圈数：" + result[2]);
				// System.out.println("result[0]= "+result[0]);
				// System.out.println("result[1]= "+result[1]);
				if (null != context) {
					Utils.saveIntPreference(context, MyApplication.SPEED,
							result[0]);
					Utils.saveIntPreference(context, MyApplication.CARDEN,
							result[1]);
					Utils.saveIntPreference(context, MyApplication.CIRCLE,
							result[2]);
					Utils.savePreference(context, MyApplication.BIND,
							MyApplication.SUCESS);
				}
			}

		};

		if (null == MyApplication.mSyncDeviceDataManager) {
			// System.out.println("mBlueDevice====mSyncDeviceDataManager");
			MyApplication.mSyncDeviceDataManager = new BleSyncManager(context,
					syncDataCallback);
		}

	}
}
