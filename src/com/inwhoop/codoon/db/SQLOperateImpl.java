package com.inwhoop.codoon.db;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.codoon.entity.MapData;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SQLOperateImpl {
	private DBOpneHelper dbOpenHelper;

	public SQLOperateImpl(Context context) {
		dbOpenHelper = new DBOpneHelper(context);

	}

	/**
	 * 增，用insert向数据库中插入数据
	 */
	public void add(String lag, String lon, String sTime) {
		// System.out.println("add=======================");
		SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(DBOpneHelper.LAG, lag);
		values.put(DBOpneHelper.LON, lon);
		values.put(DBOpneHelper.S_TIME, sTime);
		db.insert(DBOpneHelper.TABLE, null, values);
	}

	public List<MapData> query(String time) {
		SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
		List<MapData> mapDatas = new ArrayList<MapData>();
		Cursor cursor = null;
		try {
			cursor = db.rawQuery("SELECT * FROM " + DBOpneHelper.TABLE
					+ " WHERE " + DBOpneHelper.S_TIME + "=\"" + time + "\"",
					null);
			if (cursor != null) {

				if (!cursor.moveToFirst()) {
					return mapDatas;
				}

				int lag = cursor.getColumnIndex(DBOpneHelper.LAG);
				int lon = cursor.getColumnIndex(DBOpneHelper.LON);
				do {
					MapData mData = new MapData();

					mData.lag = cursor.getString(lag);
					mData.log = cursor.getString(lon);

					mapDatas.add(mData);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return mapDatas;
	}
}
