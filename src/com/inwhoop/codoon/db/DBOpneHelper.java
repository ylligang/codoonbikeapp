package com.inwhoop.codoon.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpneHelper extends SQLiteOpenHelper {
	private static final int VERSION = 1;// 版本
	private static final String DB_NAME = "map.db";// 数据库名
	public static final String TABLE = "map";// 表名
	public static final String _ID = "_id";// 表中的列名
	public static final String LAG = "lag";// 表中的列名
	public static final String LON = "lon";// 表中的列名
	public static final String S_TIME = "s_time";// 表中的列名
	public static final String E_TIME = "e_time";// 表中的列名
	// 创建数据库语句
	private static final String CREATE_TABLE = "create table " + TABLE + " ( "
			+ _ID + " Integer primary key autoincrement," + LAG + " text, "
			+ LON + " text, " + S_TIME + " text, " + E_TIME + " text )";

	public DBOpneHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
